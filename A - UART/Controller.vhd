----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:07:26 01/15/2014 
-- Design Name: 
-- Module Name:    Controller - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Controller is
	Port(
			clk : in STD_LOGIC;											
			reset_n : in  STD_LOGIC;									-- button
			start : in STD_LOGIC;										-- button
			send : in  STD_LOGIC;										-- button
			x_en : out STD_LOGIC;
			x_reset : out STD_LOGIC;
			u_reset : out STD_LOGIC;
			sel_value : out STD_LOGIC;
			sel_led : out STD_LOGIC;
			enable_digit : out STD_LOGIC_VECTOR(3 downto 0);
			dots : out STD_LOGIC_VECTOR(3 downto 0)
			);
end Controller;

architecture Behavioral of Controller is

type state is (s_idle, s_loadx, s_viewx, s_send);
signal s_current, s_next: state := s_idle;
signal reset: std_logic;

begin

reset <= not(reset_n);			-- 1-attivo

STATO_PROSSIMO: process(clk, reset)
	begin
		
		if reset='1' then
			s_current <= s_idle;
		elsif rising_edge(clk) then
			s_current <= s_next;
		end if;
		
	end process;

FSM_MAIN: process (start, send, s_current)
	
	begin
	
		x_en <= '0';
		x_reset <= '0';
		u_reset <= '0';
		sel_value <= '0';
		sel_led <= '0';
		enable_digit <= "0000";
		dots <= "0000";
		
		case s_current is
			
			when s_idle =>		enable_digit <= "0011";
									
									x_en <= '1';
									x_reset <= '1';
									
									u_reset <= '1';
									
									if start='1' then
										s_next <= s_loadx;
									else
										s_next <= s_idle;
									end if;
									
			
			when s_loadx =>	if start='0' then
										x_en <= '1';
										enable_digit <= "0011";
									else
										x_en <= '0';
									end if;
									
									if send='1' then
										s_next <= s_send;
									else
										s_next <= s_loadx;
									end if;
									
									
			when s_send => 	x_en <= '0';
									enable_digit <= "0011";
									
									sel_value <= '1';
									sel_led <= '1';
									
									if start='0' and reset='1' then
										s_next <= s_idle;
									elsif start='1' and reset='0' then
										s_next <= s_loadx;
									else
										s_next <= s_send;
									end if;
									
			
			when others =>		s_next <= s_idle;
			
			
			end case;
			
	end process;
	
	
end Behavioral;

