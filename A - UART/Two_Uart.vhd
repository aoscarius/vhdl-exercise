library IEEE; 
use IEEE.STD_LOGIC_1164.ALL;

entity Two_Uart is 	
	Port( 		
		clk: in STD_LOGIC; 		
		reset: in STD_LOGIC; 		
		d_in: instd_logic_vector (7 downto 0); 		
		bit_tx: out std_logic; 		
		bit_rx: in STD_LOGIC; 		
		d_out: out std_logic_vector (7 downto 0) 		
		); 
end Two_Uart;

architecture Behavioral of Two_Uart is
component RS232RefCompTX    
	Port (  	 			
		TXD: out std_logic := '1'; 		 	
		RXD: instd_logic;					   		 	
		CLK: instd_logic;							 			
		DBIN: in std_logic_vector (7 downto 0); 			
		DBOUT: out std_logic_vector (7 downto 0); 			
		RDA: inout std_logic;							 			
		TBE: inout std_logic := '0';				 			
		RD: in std_logic;							 			
		WR: in std_logic;							 			
		PE: out std_logic;							 			
		FE: out std_logic;							 			
		OE: out std_logic;											 			
		RST: in std_logic := '0'
		);				 
end component;
	 
for all: RS232RefCompTX use entity WORK.RS232RefComp(Behavioral);

component RS232RefCompRX    
	Port (  	 			
		TXD: out std_logic := '1'; 		 	
		RXD: in std_logic;					   		 	
		CLK: in std_logic;							 			
		DBIN: in std_logic_vector (7 downto 0); 			
		DBOUT: out std_logic_vector (7 downto 0); 			
		RDA: inout std_logic:='1';							 			
		TBE: inout std_logic := '1';				 			
		RD: in std_logic;							 			
		WR: in std_logic;							 			
		PE: out std_logic;							 			
		FE: out std_logic;							 			
		OE: out std_logic;											 			
		RST: in std_logic := '0'
		);				 
end component;	 

for all: RS232RefCompRX use entity WORK.RS232RefComp(Behavioral);

signal DBOUT1,DBIN2 :std_logic_vector (7 downto 0); 
signal rdaSig1,tbeSig1,PE1,FE1,OE1,rdaSig2,tbeSig2,PE2,FE2,OE2,WR,WR2,RD,RD2,TXD2: std_logic;

begin

TX: RS232RefCompTX  	
	Port map( 				
		TXD => bit_tx,  				
		RXD => '1',  				
		CLK => clk,  				
		DBIN => d_in,  				
		DBOUT => DBOUT1,  				
		RDA => rdaSig1, 				
		TBE => tbeSig1, 				
		RD => '1', 				
		WR => '1', 				
		PE => PE1, 				
		FE => FE1, 				
		OE => OE1, 				
		RST => reset 				
		); 
			
RX: RS232RefCompRX  	
	Port map( 				
		TXD => TXD2,  				
		RXD => bit_rx,  				
		CLK => clk,  				
		DBIN => DBIN2,  				
		DBOUT => d_out,  				
		RDA => rdaSig2, 				
		TBE => tbeSig2, 				
		RD => '0', 				
		WR => '0', 				
		PE => PE2, 				
		FE => FE2, 				
		OE => OE2, 				
		RST => reset 				
		);
end Behavioral;