----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:26:10 01/15/2014 
-- Design Name: 
-- Module Name:    Two_Uart_Comunication - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library IEEE; 
use IEEE.STD_LOGIC_1164.ALL;
--Uncomment the following library declaration if using 
--arithmetic functions with Signed or Unsigned values 
--use IEEE.NUMERIC_STD.ALL;
--Uncomment the following library declaration if instantiating 
--any Xilinx primitives in this code. 
--library UNISIM; 
--use UNISIM.VComponents.all;

entity Two_Uart_Comunication is 	
	Port(	   			
		clk : in  STD_LOGIC; 			
		reset : in  STD_LOGIC;												
		start : in STD_LOGIC;			
		send : in  STD_LOGIC;
		--switches	 			
		in_byte : in STD_LOGIC_VECTOR(7 downto 0);			 			
		pin_in : in STD_LOGIC;										-- *** 			
		--led
		led: out STD_LOGIC_VECTOR(7 downto 0);					 			
		pin_out : out STD_LOGIC;									-- *** 			
		--cifra del display
		anodes : out  STD_LOGIC_VECTOR (3 downto 0);			 			
		-- 7 segments + punto 
		cathodes : out  STD_LOGIC_VECTOR (7 downto 0)					
		); 
end Two_Uart_Comunication;

architecture Structural of Two_Uart_Comunication is

component Display_Manager is 	
	Generic( 				
		clk_freq_in : integer := 50000000; 				
		clk_freq_out : integer := 5000000 				
		); 
	
	Port(  			
		clk : in STD_LOGIC; 			
		reset_n : in STD_LOGIC;									          
		value : in STD_LOGIC_VECTOR (15 downto 0);			 			
		enable_digit : in STD_LOGIC_VECTOR (3 downto 0);	 			
		dots : in STD_LOGIC_VECTOR (3 downto 0);				 			
		anodes : out STD_LOGIC_VECTOR (3 downto 0);			 			
		cathodes : out STD_LOGIC_VECTOR (7 downto 0)		 			
		);	 
end component;

component Registro_Clockato is 	
	Generic( width : natural := 8); 	
	Port (  			
		d : in STD_LOGIC_VECTOR (width-1 downto 0);	 			
		clk : in STD_LOGIC;									  			
		reset : in STD_LOGIC;
								 															  
		en : in STD_LOGIC;									          
		q : out STD_LOGIC_VECTOR (width-1 downto 0) 			
		); 
end component;

component Two_Uart is 	
	Port( 			
		clk : in STD_LOGIC; 			
		reset : in STD_LOGIC; 			
		d_in: in std_logic_vector (7 downto 0); 			
		bit_tx : out std_logic; 			
		bit_rx : in STD_LOGIC; 			
		d_out: out std_logic_vector (7 downto 0) 			
		); 
end component;

component Mux2_vect is 	
	Generic ( width : integer := 4);    
	Port(  			
		a : in STD_LOGIC_VECTOR(width-1 downto 0);          
		b : in STD_LOGIC_VECTOR(width-1 downto 0);          
		sel : in STD_LOGIC;          
		o : out STD_LOGIC_VECTOR(width-1 downto 0) 			
		); 
end component;

component Controller is 	
	Port( 			
		clk : in STD_LOGIC;											 			
		reset_n : in STD_LOGIC;									 			
		start : in STD_LOGIC;										 			
		send : in STD_LOGIC;										 			
		x_en : out STD_LOGIC; 			
		x_reset : out STD_LOGIC; 			
		u_reset : out STD_LOGIC; 			
		sel_value : out STD_LOGIC; 			
		sel_led : out STD_LOGIC; 			
		enable_digit : out STD_LOGIC_VECTOR(3 downto 0); 			
		dots : out STD_LOGIC_VECTOR(3 downto 0) 			
		); 
end component;

signal reset_n : std_logic; 
signal xen_tmp, xreset_tmp : std_logic; 
signal ureset_tmp : std_logic := '0'; 
signal digit_tmp, dots_tmp : std_logic_vector(3 downto 0); 
signal x_tmp : std_logic_vector(7 downto 0); 
signal value_tmp : std_logic_vector(15 downto 0); 
signal mux_tmp : std_logic_vector(7 downto 0); 
signal sel_tmp : std_logic; 
signal seled_tmp : std_logic; 
signal dout_tmp : std_logic_vector(7 downto 0);

--signal link_in, link_out: std_logic;

begin

reset_n <= not(reset);
value_tmp <= "0000"&"0000"&x_tmp;
--link_in <= link_out;

DISPLAY: Display_Manager  	
	Generic map( 				
		clk_freq_in => 50000000, 				
		clk_freq_out => 400 				
		) 	
	Port map(  			
		clk => clk, 			
		reset_n => reset_n,									          
		value => value_tmp,			 			
		enable_digit => digit_tmp,	 			
		dots => dots_tmp,				 			
		anodes => anodes,			 			
		cathodes => cathodes		 			
		);
	 		
REGX: Registro_Clockato 	
	Generic map( width => 8) 	
	Port map(  			
		d => in_byte,	          
		clk => clk, 									           
		reset => xreset_tmp,
								 																
		en => xen_tmp,									          
		q => x_tmp 			
	);

UART: Two_Uart  	
	Port map( 			
		clk => clk,  			
		reset => ureset_tmp, 			
		d_in => mux_tmp, 			
		bit_tx => pin_out, 			
		bit_rx => pin_in, 			
		d_out => dout_tmp 			
		); 		

MUX: Mux2_vect 	
	Generic map( width => 8)    
	Port map(  			
		a => "00000000",          
		b => x_tmp,          
		sel => sel_tmp,          
		o => mux_tmp 			
		); 		

CU: Controller  	
	Port map( 			
		clk => clk, 											 			
		reset_n => reset_n,									 			
		start => start,									 			
		send => send,										 			
		x_en => xen_tmp, 			
		x_reset => xreset_tmp, 			
		u_reset => ureset_tmp, 			
		sel_value => sel_tmp, 			
		sel_led => seled_tmp, 			
		enable_digit => digit_tmp, 			
		dots => dots_tmp 			
		);

MUXLED: Mux2_vect  	
	Generic map( width => 8)    
	Port map(  			
		a => "00000000",          
		b => dout_tmp,          
		sel => seled_tmp,          
		o => led 			
	);
end Structural; 