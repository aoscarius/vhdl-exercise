-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: IR-RC5 Module
-- Create Date:  02/01/2015
-- Module Name:  sWatchdog.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Watchdog statitco non programmabile, che ogni qual volta
--					  termina il conteggio, abilita una linea di timeout e si arresta
--					  fino al prossimo reset
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity sWatchdog is
	generic ( M  : natural := 256 );
	port ( clk, reset : in std_logic;
		    timeout: out std_logic ); 
end sWatchdog;

architecture behavioral of sWatchdog is
	signal reg_counter : natural range 0 to (M-1) := (M-1);
	signal reg_timeout : std_logic := '0';
begin
	process(clk, reset, reg_counter)
	begin
		-- Se reset alto, reinizializzo il contatore
		if (reset='1') then
			reg_counter <= 0;
			reg_timeout <= '0';
		-- Altrimenti se non � stato 
		elsif (reg_timeout = '0') then
			if (clk'event and clk='1') then
				reg_counter <= reg_counter - 1;
			end if;
			if (reg_counter = 0) then
				reg_timeout <= '1';
			end if;
		end if;
	end process;

	timeout <= reg_timeout;
	
end behavioral;