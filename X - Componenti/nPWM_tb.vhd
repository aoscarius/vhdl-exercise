-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  06/01/2015
-- Module Name:  nPWM_tb.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Testbench per il modulo nPWM, dove � possibile osservare, come
--               il modulo pu� essere usato per generare non solo piu fasi ma anche
--               un segnale a frequenza doppia o quadrupla di quella generata
--
-- Dependencies: nPWM.vhd
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY nPWM_tb IS
END nPWM_tb;
 
ARCHITECTURE behavior OF nPWM_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT nPWM
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         load : IN  std_logic;
         duty : IN  std_logic_vector(7 downto 0);
         pwm_out : OUT  std_logic_vector(3 downto 0);
         pwm_n_out : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal load : std_logic := '0';
   signal duty : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal pwm_out : std_logic_vector(3 downto 0);
   signal pwm_n_out : std_logic_vector(3 downto 0);
   signal pwm : std_logic;

   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
	-- Segnale a frequenza quadrupla, generato dalle fasi del segnale prodotto
	-- Dimostrazione di come giocando opportunamente su fase e duty cycle di un segnale di clock
	-- � possibile creane uno nuovo a frequenza anche doppia.
	pwm <= pwm_out(0) xor pwm_out(1) xor pwm_out(2) xor pwm_out(3);
	
	-- Instantiate the Unit Under Test (UUT)
	uut: nPWM PORT MAP (
          clk => clk,
          reset => reset,
          load => load,
          duty => duty,
          pwm_out => pwm_out,
          pwm_n_out => pwm_n_out
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

		duty <= "00100000";
		load <='1';
      wait for clk_period*10;
		load <='0';
      -- insert stimulus here 

      wait;
   end process;

END;
