-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ROM Mapped RCA Adder
-- Create Date:  24/11/2014
-- Module Name:  RCAROMAdder.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione di un Ripple Carry Adder di dimensione generica,
--               interamente mappato in memoria attraverso l'uso del Full Adder
--               implementato a partire da un decoder 3:8 mappato in ROM.
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity RCAROMAdder is
	generic ( N: natural := 4 );
	port ( a, b: in std_logic_vector((N-1) downto 0);
		   sum: out std_logic_vector((N-1) downto 0);
		   cout: out std_logic
	);
end RCAROMAdder;

architecture Behavioral of RCAROMAdder is
	component FADecoder
	port ( a, b, cin: in std_logic;
		   sum, cout: out std_logic
	);
	end component;

	signal c_sig : std_logic_vector(N downto 0);
begin
	c_sig(0) <= '0';
	
	rpc: for i in  0 to (N-1) generate
		far: FADecoder port map (a(i), b(i), c_sig(i), sum(i), c_sig(i+1));
	end generate;
	
	cout <= c_sig(N);
end Behavioral;

