-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  15/11/2014 
-- Module Name:  BUSMux_4_1.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Multiplexer 4:1 per BUS a N bit
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity BUSMux_4_1 is
	generic ( N: natural := 4 );
	port (din_0 : in std_logic_vector((N-1) downto 0);   
		  din_1 : in std_logic_vector((N-1) downto 0); 
		  din_2 : in std_logic_vector((N-1) downto 0);
		  din_3 : in std_logic_vector((N-1) downto 0);
		  sel : in std_logic_vector(1 downto 0); 
		  dout : out std_logic_vector((N-1) downto 0)
	);
end BUSMux_4_1;

architecture Behavioral of BUSMux_4_1 is
begin
	process(sel, din_0, din_1, din_2, din_3)
	begin
		case sel is
			when "00" => dout <= din_0;
			when "01" => dout <= din_1;
			when "10" => dout <= din_2;
			when "11" => dout <= din_3; 
			when others => dout <= (others => 'X');
		end case; 
	end process;
end Behavioral;