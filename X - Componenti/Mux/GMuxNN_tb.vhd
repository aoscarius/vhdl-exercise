-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  25/11/2014
-- Module Name:  GMuxNN_tb.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Testbench per il modulo GMuxNN.
--
-- Dependencies: none
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
use work.typedef.all;

ENTITY GMuxNN_tb IS
END GMuxNN_tb;

ARCHITECTURE behavior OF GMuxNN_tb IS 
	COMPONENT GMuxNN
		generic(NSel: natural := 2;
				  NBit: natural := 8
		);
		port ( din : in std_logic_matrix(((2**NSel)-1) downto 0, (NBit-1) downto 0);
				sel : in std_logic_vector((NSel-1) downto 0);
				dout: out std_logic_vector((NBit-1) downto 0)
		);
	END COMPONENT;

	SIGNAL din:  std_logic_matrix(3 downto 0, 7 downto 0);
	SIGNAL sel:  std_logic_vector(1 downto 0);
	SIGNAL dout: std_logic_vector(7 downto 0); 
BEGIN
	uut: GMuxNN PORT MAP(
		  din => din,
		  sel => sel,
		  dout => dout
	);

	stim_proc : PROCESS
	BEGIN
		wait for 10 ns;
		din(3,7) <= '1'; din(3,6) <= '1'; din(3,5) <= '1'; din(3,4) <= '1'; din(3,3) <= '1'; din(3,2) <= '1'; din(3,1) <= '1'; din(3,0) <= '1'; -- 11111111
		din(2,7) <= '0'; din(2,6) <= '0'; din(2,5) <= '0'; din(2,4) <= '0'; din(2,3) <= '0'; din(2,2) <= '0'; din(2,1) <= '0'; din(2,0) <= '0'; -- 00000000
		din(1,7) <= '1'; din(1,6) <= '1'; din(1,5) <= '0'; din(1,4) <= '0'; din(1,3) <= '1'; din(1,2) <= '1'; din(1,1) <= '0'; din(1,0) <= '0'; -- 11001100
		din(0,7) <= '0'; din(0,6) <= '0'; din(0,5) <= '1'; din(0,4) <= '1'; din(0,3) <= '0'; din(0,2) <= '0'; din(0,1) <= '1'; din(0,0) <= '1'; -- 00110011
			wait for 5 ns;
		sel <= "00";
		wait for 20 ns;
		sel <= "01";
		wait for 20 ns;
		sel <= "10";
		wait for 20 ns;
		sel <= "11";
		wait for 20 ns;
	END PROCESS;

END;
