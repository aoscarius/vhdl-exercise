-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar




-- 
-- Project Name: ASE Component Library
-- Create Date:  22/11/2014
-- Module Name:  GMux.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Questo modulo implementa un multiplexer di tipo 2^N:N. Questa
--               particolare implementazione viene riconosciuta dal compilatore
--               XST come un mux nonostante la non classica struttira di un 
--               mux a causa dell'uso del parametro generico NSel. Tuttavia, 
--               questo risulta essere un particolare caso in quanto, se � vero
--               che questa struttura viene convertita in un mux, la sua versione
--               complementare, non viene riconosciuta come demux.
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity GMux is
	generic(NSel: natural := 2);
	port ( din : in std_logic_vector(((2**NSel)-1) downto 0);
			 sel : in std_logic_vector((NSel-1) downto 0);
			 dout: out std_logic
	);
end GMux;

architecture Behavioral of GMux is
begin
	-- Questa particolare assegnazione viene identificata come mux
	-- ma non vale al contrario per i demux
	dout <= din(conv_integer(sel));
end Behavioral;

