-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  25/11/2014
-- Module Name:  GMuxN.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Altre forme di mux generici, in particolare questo presenta in 
--               ingresso ed in uscita bus fissi da 8 bit.
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Libreria di supporto
use work.typedef.all;

entity GMuxN is
	generic(NSel: natural := 2);
	port ( din : in std_logic_vector_matrix(((2**NSel)-1) downto 0);
			 sel : in std_logic_vector((NSel-1) downto 0);
			 dout: out std_logic_vector(7 downto 0)
	);
end GMuxN;

architecture Behavioral of GMuxN is
begin
	dout <= din(conv_integer(sel));
end Behavioral;