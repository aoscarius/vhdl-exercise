-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  25/11/2014
-- Module Name:  GMuxNN.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Altre forme di mux generici, in particolare questo presenta in 
--               ingresso ed in uscita bus generici di dimensione NBit
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Libreria di supporto
use work.typedef.all;

entity GMuxNN is
	generic(NSel: natural := 2;
			NBit: natural := 8
	);
	port ( din : in std_logic_matrix(((2**NSel)-1) downto 0, (NBit-1) downto 0);
		   sel : in std_logic_vector((NSel-1) downto 0);
		   dout: out std_logic_vector((NBit-1) downto 0)
	);
end GMuxNN;

architecture Behavioral of GMuxNN is
begin
	gen: for i in (NBit-1) downto 0 generate
		dout(i) <= din(conv_integer(sel),i);
	end generate;
end Behavioral;