-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  25/11/2014
-- Module Name:  typedef.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Libreria di supporto per i mux generici
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;

package typedef is
	type std_logic_vector_matrix is array (natural range <>) of std_logic_vector(7 downto 0);
	type std_logic_matrix is array (natural range <>, natural range <>) of std_logic;
end typedef;