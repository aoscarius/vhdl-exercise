-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  15/11/2014 
-- Module Name:  HitCounter.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Contatore generico modulo N, con full range hit
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity HitCounter is
	generic (N : natural := 8);
	port ( clk, reset, count_inc: in std_logic;
		   count_hit: out std_logic  
	); 
end HitCounter;

architecture Behavioral of HitCounter is
	signal reg_count : natural range 0 to N := 0;
begin
	process(clk, reset, reg_count)
	begin
		if (reset='1') then
			reg_count <= 0;
		elsif (clk'event and clk='1') then
			if (count_inc='1') then
				reg_count <= reg_count + 1;
			end if;
		end if;	
		if (reg_count<N) then
			count_hit <= '0';
		else
			count_hit <= '1';
		end if;
	end process;
end Behavioral;