-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: IR-RC5 Module
-- Create Date:  02/01/2015
-- Module Name:  NCounterMod.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Contatore modulo M specializzato, che mantiene alta la linea 
--               rule_hit, finch� il contatore si mantiene ad un valore inferiore
--               a quello preimpostato.
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity CounterMod is
	generic ( M  : natural := 256;
				 hit_until : natural := 128
	);
	port ( clk, reset : in std_logic;
		    rule_hit: out std_logic  
	); 
end CounterMod;

architecture behavioral of CounterMod is
	signal tmp_cnt : natural range 0 to (M-1):= 0;
begin
	process(clk, reset, tmp_cnt)
	begin
		-- Se reset alto, resetto il contatore
		if (reset='1') then
			tmp_cnt <= 0;
			rule_hit <= '0';
		-- Altrimenti lo incremento ad ogni ciclo di clock
		else
			if (clk'event and clk='1') then
				tmp_cnt <= tmp_cnt + 1;
			end if;
			
			-- Se il contatore � ancora sotto la soglia prestabilita rule_it resta alto
			if (tmp_cnt<=(hit_until-1)) then
				rule_hit <= '1';
			else
				rule_hit <= '0';
			end if;	
			
		end if;
	end process;
end behavioral;