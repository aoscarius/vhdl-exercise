-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  06/01/2015
-- Module Name:  nPWM.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Modulo per la generazione di un segnale PWM, a frequenza prefissata e 
--				 duty cycle variabile in tempo reale con possibilitÓ di generare un numero 
--				 finito di fasi in uscita
--
-- Dependencies: none
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity nPWM is
  generic(
      sys_clk  : integer := 50_000_000; --Clock di sistema in hz
      pwm_freq : integer := 1_000_000;     --Frequenza del PWM in hz
      bits_res : integer := 8;
	   phases : integer := 4);           --Numero di bits di risoluzione per il setting del duty cycle
  port(
      clk      : in  std_logic;                               --Clock di sistema
      reset    : in  std_logic;                               --Reset del modulo
      load     : in  std_logic;                               --Caricamento duty cycle
      duty     : in  std_logic_vector((bits_res-1) downto 0); --Duty Cycle
      pwm_out  : out std_logic_vector((phases-1) downto 0);	  --Outputs PWMs
	   pwm_n_out : out std_logic_vector((phases-1) downto 0));          				
end nPWM;

architecture behavioral of nPWM is
	constant period  : integer := sys_clk/pwm_freq;                          --Numero di clocks in un periodo
   type counters is array (0 to phases-1) of integer range 0 to period - 1; --Tipo di dato per l'array di contatori di clocks
    signal reg_count : counters := (others => 0);                           --Array di contatori di clocks in un periodo
	signal reg_duty : integer range 0 to period - 1 := 0;                   --Contatore di clocks
begin
	process(clk, reset)
	begin
		-- Reset asincrono
		if(reset = '1') then
			 reg_count <= (others => 0); 
		    pwm_out <= (others => '0');                                       
		    pwm_n_out <= (others => '0');     
	   -- Ad ogni ciclo di clock
		elsif(clk'event and clk = '1') then                      
			-- Se load attivo carico il nuovo duty 
			if(load = '1') then
				reg_duty <= conv_integer(duty)*period/(2**bits_res);
			end if;
			-- Per ogni fase presente genero un contatore
			for i in 0 to phases-1 loop 
				-- Se il contatore della i-esima ha raggiunto il numero di clocks per periodo
				-- allora resetto il contatore
				if (reg_count(0) = ((period-1)-(i*period/phases))) then
					reg_count(i) <= 0;
				else
					reg_count(i) <= reg_count(i) + 1;
				end if;
			 end loop;
			 -- Per ogni fase presente effettuo il test sul rispettivo contatore
			 for i in 0 to phases-1 loop 
				-- Se il contatore della i-esima fase ha superato il numero di clock per duty cycle
				-- allora porto l'uscita al suo valore logico basso
				if(reg_count(i) < (reg_duty-1)) then
					pwm_out(i) <= '1';
					pwm_n_out(i) <= '0';
				else
					pwm_out(i) <= '0';
					pwm_n_out(i) <= '1';
				end if;
			end loop;
		end if;
	end process;
end behavioral;