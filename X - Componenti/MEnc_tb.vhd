-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: IR-RC5 Module
-- Create Date:  02/01/2015
-- Module Name:  MEnc_tb.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Testbench per il modulo per la codifica Manchester di un dato ad N bit
--
-- Dependencies: MEnc.vhd
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY MEnc_tb IS
END MEnc_tb;
 
ARCHITECTURE behavior OF MEnc_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT MEnc
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         data_bus : IN  std_logic_vector(13 downto 0);
         out_data : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
	signal reset : std_logic := '0';
	signal data_bus : std_logic_vector(13 downto 0) := (others => '0');
 	
	constant start_bits : std_logic_vector (1 downto 0) := "11";
	signal toggle : std_logic := '0';
	signal address : std_logic_vector (4 downto 0) := "00000";
	signal command : std_logic_vector (5 downto 0) := "000000";

 	--Outputs
   signal out_data : std_logic;

   -- Clock period definitions
   constant clk_period : time := 1778 us;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: MEnc PORT MAP (
          clk => clk,
          reset => reset,
          data_bus => data_bus,
          out_data => out_data
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
		-- Prima trasmissione di test
		reset <= '1';
		
		toggle <= '0';
		address <= "00011";
		command <= "011100";

		data_bus <= start_bits & toggle & address & command;

		wait for clk_period*1;
		reset <= '0';
		wait for clk_period*14;

		-- Seconda trasmissione di test
		reset <= '1';
		
		toggle <= '0';
		address <= "00110";
		command <= "000111";

		data_bus <= start_bits & toggle & address & command;

		wait for clk_period*1;
		reset <= '0';
		wait for clk_period*14;
   end process;
END;
