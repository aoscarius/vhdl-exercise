-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  22/11/2014 
-- Module Name:  Bin2BCD.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione hardware dell'algoritmo Double Dabble, usato
--               per convertire numeri binari di tipo unsigned in numeri 
--               codificati in BCD. L'algoritmo � adattato a byte, rappresentabili 
--               su 3 cifre.
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_1164.all ;
use ieee.std_logic_unsigned.all ;

entity Bin2BCD is
    port ( binary_in : in  std_logic_vector (7 downto 0);
           bcd_out   : out std_logic_vector (11 downto 0)
	);
end Bin2BCD;

architecture behaviour of Bin2BCD is
begin
	process (binary_in)
		variable bin_src : std_logic_vector (4 downto 0) ;
		variable bcd_dst : std_logic_vector (11 downto 0) ;
	begin
		bcd_dst             := (others => '0') ;
		bcd_dst(2 downto 0) := binary_in(7 downto 5) ;
		bin_src             := binary_in(4 downto 0) ;

		for i in bin_src'range loop
			if bcd_dst(3 downto 0) > "0100" then
				bcd_dst(3 downto 0) := bcd_dst(3 downto 0) + "0011" ;
			end if ;
			if bcd_dst(7 downto 4) > "0100" then
				bcd_dst(7 downto 4) := bcd_dst(7 downto 4) + "0011" ;
			end if ;
			-- No roll over for hundred digit, since in 0 .. 2

			bcd_dst := bcd_dst(10 downto 0) & bin_src(bin_src'left) ; -- shift bcd_dst + 1 new entry
			bin_src := bin_src(bin_src'left - 1 downto bin_src'right) & '0' ; -- shift src + pad with 0
		end loop ;

		bcd_out(11 downto 8) <= bcd_dst(11 downto 8);
		bcd_out(7  downto 4) <= bcd_dst(7  downto 4);
		bcd_out(3 downto 0) <= bcd_dst(3  downto 0);
	end process;
end behaviour;