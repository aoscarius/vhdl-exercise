-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: IR-RC5 Module
-- Create Date:  06/01/2015
-- Module Name:  PWM_tb.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Testbench per il modulo per la generazione di un segnale PWM con
--               duty cycle variabile
--
-- Dependencies: PWM.vhd
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
 
entity PWM_tb is
end PWM_tb;
 
architecture behavior of PWM_tb is 
 
    -- component declaration for the unit under test (uut)
    component PWM
    port(
         clk : in  std_logic;
         reset : in  std_logic;
         load : in  std_logic;
         duty : in  std_logic_vector(7 downto 0);
         pwm_out : out  std_logic);
    end component;

   --inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal load : std_logic := '0';
   signal duty : std_logic_vector(7 downto 0) := (others => '0');

 	--outputs
   signal pwm_out : std_logic;

   -- clock period definitions
   constant clk_period : time := 20 ns;
 
begin
 
	-- instantiate the unit under test (uut)
   uut: PWM port map (
          clk => clk,
          reset => reset,
          load => load,
          duty => duty,
          pwm_out => pwm_out
        );

   -- clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
   -- stimulus process
   stim_proc: process
   begin		
		duty <= "00001111";
		load <= '1';
		wait for clk_period*2;
		load <= '0';
		
		wait for 100 us;
		
		duty <= "01100111";
		load <= '1';
		wait for clk_period*2;
		load <= '0';

		wait for 100 us;

		duty <= "11110000";
		load <= '1';
		wait for clk_period*2;
		load <= '0';

     wait;
   end process;

end;
