-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: IR-RC5 Module
-- Create Date:  06/01/2015
-- Module Name:  sPWM_tb.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Testbench per il modulo per la generazione di un segnale PWM con
--               duty cycle statico prefissato
--
-- Dependencies: sPWM.vhd
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
 
entity sPWM_tb is
end sPWM_tb;
 
architecture behavior of sPWM_tb is 
 
    -- component declaration for the unit under test (uut)
    component sPWM
    port(
         clk : in  std_logic;
         reset : in  std_logic;
         pwm_out : out  std_logic);
    end component;

   --inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';

 	--outputs
   signal pwm_out : std_logic;

   -- clock period definitions
   constant clk_period : time := 20 ns;
 
begin
 
	-- instantiate the unit under test (uut)
   uut: sPWM port map (
          clk => clk,
          reset => reset,
          pwm_out => pwm_out
        );

   -- clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
   -- stimulus process
   stim_proc: process
   begin		
      wait;
   end process;

end;
