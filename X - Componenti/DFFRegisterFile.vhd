-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  15/11/2014 
-- Module Name:  DFFRegisterFile.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Register File di dimensione generica, realizzati con Flip-Flop di tipo D
--               con reset asincrono e abilitazione in scrittura sincrona
--
-- Dependencies: none
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all ;
use ieee.numeric_std.all;

-- DFF with asynchronous reset
entity DFFRegisterFile is
	generic ( N : natural := 8;
			  NAddr : natural := 8
	);
	port ( clk, reset: in std_logic;
		   wr_en : in std_logic; 
		   addr : in std_logic_vector((NAddr-1) downto 0);
		   d : in std_logic_vector((N-1) downto 0);
		   q : out std_logic_vector((N-1) downto 0)
	);
end DFFRegisterFile ;

architecture Behavioral of DFFRegisterFile is
	type t_regfile is array (((2**NAddr)-1) downto 0) of std_logic_vector((N-1) downto 0);
	signal reg_file : t_regfile;
begin
	process(clk, reset)
	begin
		if (reset='1') then
			q <= (others => (others =>'0'));
		elsif (clk'event and clk='1') then
			if (wr_en='1') then
				reg_file(to_integer(unsigned(addr))) <= d;
			end if;
		end if;
	end process;
	q <= reg_file(to_integer(unsigned(addr)));
end Behavioral;
