-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  15/11/2014 
-- Module Name:  DFFRegister.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Registro ad N bit, realizzato mediante l'uso di Flip-Flop di tipo D
--               con reset asincrono
--
-- Dependencies: none
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

-- DFF with asynchronous reset
entity DFFRegister is
	generic ( N : natural := 8 );
	port ( clk, reset, en: in std_logic;
		   d : in std_logic_vector(N-1 downto 0);
		   q : out std_logic_vector(N-1 downto 0)
	);
end DFFRegister;

architecture Behavioral of DFFRegister is
begin
	process(clk, reset)
	begin
		if (reset='1') then
			q <= (others =>'0');
		elsif (clk'event and clk='1') then
			if (en='1') then
				q <= d;
			end if;
		end if;
	end process;
end Behavioral;
