-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Pattern Check
-- Create Date:  15/12/2014 
-- Module Name:  PatternCheck.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Macchina a stati finiti. Riconoscitore di sequenza 1011 
--               senza sovrapposizioni.
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity PatternCheck is
    Port (	clk, reset : in  std_logic;
			sig_in : in std_logic; -- Segnale da analizzare
			pattern_hit : out std_logic	 -- Alto se si riscontra la sequenza
	);
end PatternCheck;

architecture moore of PatternCheck is

	type state_type is (s_1, s_10, s_101, s_1011); 
	signal s_current, s_next: state_type := s_1;

	-- Attributo per dichiarare una specifica personalizzata alla codifica degli stati
	attribute syn_encoding				    : string;
	attribute syn_encoding of state_type : type is "11 01 10 00";


begin

	next_state: process(clk, reset)
	begin
		if reset='1' then
			s_current <= s_1;
		elsif (clk'event and clk='1') then
			s_current <= s_next;
		end if;
	end process;
				 
	fsm_main: process (sig_in, s_current)
	begin

		case s_current is
		
			when s_1=>	
				pattern_hit <= '0';

				if sig_in='0' then 
					s_next <= s_10;
				else 
					s_next <= s_1;					
				end if;
				  
			when s_10=>	
				pattern_hit <= '0';

				if sig_in='1' then 
					s_next <= s_101;
				else 
					s_next <= s_1;
				end if;
				  
			when s_101=>	
				pattern_hit <= '0';

				if sig_in='1' then 
					s_next <= s_1011;
				else 
					s_next <= s_10;
				end if;
				  
			when s_1011 => 
				pattern_hit <= '1';
				
				if sig_in='1' then 
					s_next <= s_10;
				else 
					s_next <= s_1;
				end if;
			
		end case;
			
	end process;
			
end moore;

architecture mealy of PatternCheck is

	type state_type is (s_1, s_10, s_101, s_1011); 
	signal s_current, s_next: state_type := s_1;

	-- Attributo per dichiarare una specifica personalizzata alla codifica degli stati
	attribute syn_encoding				    : string;
	attribute syn_encoding of state_type : type is "11 01 10 00";

begin

	next_state: process(clk, reset)
	begin
		if reset='1' then
			s_current <= s_1;
		elsif (clk'event and clk='1') then
			s_current <= s_next;
		end if;
	end process;
				 
	fsm_main: process (sig_in, s_current)
	begin

		case s_current is
		
			when s_1=>	
				if sig_in='0' then 
					pattern_hit <= '0';
					s_next <= s_10;
				else 
					pattern_hit <= '0';
					s_next <= s_1;					
				end if;
				  
			when s_10=>	
				if sig_in='1' then 
					pattern_hit <= '0';
					s_next <= s_101;
				else 
					pattern_hit <= '0';
					s_next <= s_1;
				end if;
				  
			when s_101=>	
				if sig_in='1' then 
					pattern_hit <= '0';
					s_next <= s_1011;
				else 
					pattern_hit <= '0';
					s_next <= s_10;
				end if;
				  
			when s_1011 => 
				if sig_in='1' then 
					pattern_hit <= '1';
					s_next <= s_10;
				else 
					pattern_hit <= '0';
					s_next <= s_1;
				end if;
			
		end case;
			
	end process;
			
end mealy;