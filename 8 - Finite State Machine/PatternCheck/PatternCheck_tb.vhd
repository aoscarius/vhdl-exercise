-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Pattern Check
-- Create Date:  15/12/2014 
-- Module Name:  PatternCheck.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Testbench per la macchina a stati finiti.
--
-- Dependencies: none
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY PatternCheck_tb IS
END PatternCheck_tb;
 
ARCHITECTURE behavior OF PatternCheck_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT PatternCheck
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         sig_in : IN  std_logic;
         pattern_hit : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal sig_in : std_logic := '0';

 	--Outputs
   signal pattern_hit : std_logic;

   -- Clock period definitions
   constant clk_period : time := 20 ns;
   
   for all: PatternCheck use entity work.PatternCheck(moore);
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: PatternCheck PORT MAP (
          clk => clk,
          reset => reset,
          sig_in => sig_in,
          pattern_hit => pattern_hit
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	
		
		reset <= '1','0' after clk_period;
		
		wait for clk_period;
		sig_in <= '1';
		wait for clk_period;
		sig_in <= '0';
		wait for clk_period;
		sig_in <= '1';
		wait for clk_period;
		sig_in <= '1';
		wait for clk_period;
		sig_in <= '1';
		wait for clk_period;
		sig_in <= '0';
		wait for clk_period;
		sig_in <= '1';
		wait for clk_period;
		sig_in <= '1';
		wait for clk_period;
		sig_in <= '0';
		wait for clk_period;
		sig_in <= '1';
		wait for clk_period;
		sig_in <= '1';
		wait for clk_period;
		sig_in <= '0';

   end process;

END;
