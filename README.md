# README

This repository is a collection of exercises produced by me during the Computer System Architecture course at my unveristy for the development board of the STM STM32F4-Discovery.

You can find various types of projects and machine developed in pure VHDL.

## Licenses

All this material by [A[O]scar(ius)](https://gitlab.com/u/aoscarius) is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/)

![Creative Commons License](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
