-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar


-- 
-- Project Name: ASE Component Library
-- Create Date:  30/12/2014 
-- Module Name:  Mux_2_1.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione di un multiplexer a 2 ingressi ed 1 uscita
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Mux_2_1 is
	port ( d0, d1: in std_logic;
			 sel : in std_logic;
			 dout: out std_logic
	);
end Mux_2_1;

architecture bhv_when OF Mux_2_1 is
begin
    dout <= d0 WHEN sel='0' ELSE
            d1 WHEN sel='1' ELSE
            'X';
end bhv_when;


























