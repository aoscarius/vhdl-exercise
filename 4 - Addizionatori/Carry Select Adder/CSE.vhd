-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar


-- 
-- Project Name: Carry Select Adder
-- Create Date:  20/12/2014 
-- Module Name:  CSE.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione generic del sommatore Carry Select. Il modulo 
--					  richiede che il numero di bit del sottoblocco sia un sottomultiplo
--					  del numero di bit del sommatore per funzionare correttamente
--
-- Dependencies: RCA.vhd, FA.vhd, BUSMux_2_1.vhd, Mux_2_1.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity CSE is
	generic ( bit_depth: natural := 12; -- Numero di bit del sommatore
				 M : natural := 4          -- Numero di bit del sottoblocco
	);
	port (a: in STD_LOGIC_VECTOR((bit_depth-1) downto 0);
			b: in STD_LOGIC_VECTOR((bit_depth-1) downto 0);
		   cin: in STD_LOGIC;
			s: out STD_LOGIC_VECTOR((bit_depth-1) downto 0);
			cout: out STD_LOGIC
	);
end CSE;

architecture Behavioral of CSE is

	component RCA is
	generic ( N: natural := M );
		port ( x_in, y_in: in  std_logic_vector ((N-1) downto 0);
		   c_in : in std_logic;
--		   nadd_sub: in std_logic;
         sum : out  std_logic_vector ((N-1) downto 0);
--		   overflow : out std_logic; 
		   c_out : out std_logic);
	end component;

	component BUSMux_2_1 is
		generic ( N : natural := M);
		port ( d0, d1: in std_logic_vector((N-1) downto 0);
				 sel : in std_logic;
				 dout: out std_logic_vector((N-1) downto 0)
		);
	end component;

	component Mux_2_1 is
		port ( d0, d1: in std_logic;
				 sel : in std_logic;
				 dout: out std_logic
		);
	end component;

	signal R0P_sum : std_logic_vector((bit_depth-1) downto M);
	signal R1P_sum : std_logic_vector((bit_depth-1) downto M);
	
	signal M0P_cout : std_logic_vector((bit_depth/M)-1 downto 0);
	signal M1P_cout : std_logic_vector((bit_depth/M)-1 downto 0);

	signal MP_sel : std_logic_vector((bit_depth/M)-1 downto 0);

begin
	
	PGen: for i in 0 to ((bit_depth/M)-1) generate
	begin
	
		P0: if (i=0) generate
		begin
			-- Collegamenti per il primo blocco
			RCA_P0: RCA port map (x_in=>a((M-1) downto 0), y_in=>b((M-1) downto 0), c_in=>cin, sum=>s((M-1) downto 0), c_out=>MP_sel(0));
		end generate;
		
		PN: if (i>0) generate
		begin
			-- Collegamenti del blocco i-esimo
			RCA0_P: RCA port map (x_in=>a(((M*(i+1))-1) downto (M*i)), y_in=>b(((M*(i+1))-1) downto (M*i)), c_in=>'0', sum=>R0P_sum(((M*(i+1))-1) downto (M*i)), c_out=>M0P_cout(i));
			RCA1_P: RCA port map (x_in=>a(((M*(i+1))-1) downto (M*i)), y_in=>b(((M*(i+1))-1) downto (M*i)), c_in=>'1', sum=>R1P_sum(((M*(i+1))-1) downto (M*i)), c_out=>M1P_cout(i));
			
			MUX_P_SUM: BUSMux_2_1 port map(d0=>R0P_sum(((M*(i+1))-1) downto (M*i)), d1=>R1P_sum(((M*(i+1))-1) downto (M*i)), sel=>MP_sel(i-1), dout=>s(((M*(i+1))-1) downto (M*i)));
			MUX_P_CARRY: Mux_2_1 port map(d0=>M0P_cout(i) , d1=>M1P_cout(i) , sel=>MP_sel(i-1) , dout=>MP_sel(i));
		end generate;
		
	end generate;
	
	cout <= MP_sel((bit_depth/M)-1);
	
end Behavioral;