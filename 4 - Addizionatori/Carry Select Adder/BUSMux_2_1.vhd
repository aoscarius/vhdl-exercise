-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar


-- 
-- Project Name: ASE Component Library
-- Create Date:  30/12/2014 
-- Module Name:  BUSMux_2_1.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione di un multiplexer a 2 ingressi ed 1 uscita per 
--				     BUS di dimensione N.
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity BUSMux_2_1 is
	generic ( N : natural := 4 );
	port ( d0, d1: in std_logic_vector((N-1) downto 0);
			 sel : in std_logic;
			 dout: out std_logic_vector((N-1) downto 0)
	);
end BUSMux_2_1;

architecture bhv_when OF BUSMux_2_1 is
begin
    dout <= d0 WHEN sel='0' ELSE
            d1 WHEN sel='1' ELSE
            (others => 'X');
end bhv_when;


























