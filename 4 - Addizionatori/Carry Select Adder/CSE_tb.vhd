-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Generic Adder
-- Create Date:  27/11/2014
-- Module Name:  CSE_tb.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Testbench del sommatore Carry Select
--
-- Dependencies: CSE.vhd, RCA.vhd, FA.vhd, BUSMux_2_1.vhd, Mux_2_1.vhd
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;
 
ENTITY CSE_tb IS
END CSE_tb;
 
ARCHITECTURE behavior OF CSE_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT CSE
    PORT(
         a : IN  std_logic_vector(11 downto 0);
         b : IN  std_logic_vector(11 downto 0);
         cin : IN  std_logic;
         s : OUT  std_logic_vector(11 downto 0);
         cout : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(11 downto 0) := (others => '0');
   signal b : std_logic_vector(11 downto 0) := (others => '0');
   signal cin : std_logic := '0';

 	--Outputs
   signal s : std_logic_vector(11 downto 0);
   signal cout : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 

BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: CSE PORT MAP (
          a => a,
          b => b,
          cin => cin,
          s => s,
          cout => cout
        );

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 10 ns;	

      a <= conv_std_logic_vector(10, a'length);
      b <= conv_std_logic_vector(10, b'length);
      cin <= '0';

       wait for 10 ns;	

      a <= conv_std_logic_vector(121, a'length);
      b <= conv_std_logic_vector(180, b'length);
      cin <= '0';

      wait for 10 ns;	

      a <= conv_std_logic_vector(240, a'length);
      b <= conv_std_logic_vector(15, b'length);
      cin <= '0';

      wait for 10 ns;	

      a <= conv_std_logic_vector(12, a'length);
      b <= conv_std_logic_vector(1180, b'length);
      cin <= '1';

     wait;
   end process;

END;
