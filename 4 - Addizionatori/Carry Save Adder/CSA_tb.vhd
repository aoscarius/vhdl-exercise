-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar

 
--
-- Project Name: Carry Save Adder
-- Create Date:  10/01/2015 
-- Module Name:  CSA.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Testbench del Carry Save Adder
--
-- Dependencies: CSA.vhd, FA.vhd 
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY CSA_tb IS
END CSA_tb;
 
ARCHITECTURE behavior OF CSA_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT CSA
    PORT(
         x : IN  std_logic_vector(7 downto 0);
         y : IN  std_logic_vector(7 downto 0);
         z : IN  std_logic_vector(7 downto 0);
         result : OUT  std_logic_vector(9 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal x : std_logic_vector(7 downto 0) := (others => '0');
   signal y : std_logic_vector(7 downto 0) := (others => '0');
   signal z : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal result : std_logic_vector(9 downto 0);
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: CSA PORT MAP (
          x => x,
          y => y,
          z => z,
          result => result
        );

   -- Stimulus process
   stim_proc: process
   begin		

		x <= "00000001";
		y <= "00000011";
		z <= "00000111";
		
      wait for 100 ns;
 
		x <= "10011100";
		y <= "00100101";
		z <= "01010101";
		
      wait;
	end process;

END;
