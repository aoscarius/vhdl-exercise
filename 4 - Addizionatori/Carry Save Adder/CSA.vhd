-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar

 
--
-- Project Name: Carry Save Adder
-- Create Date:  10/01/2015 
-- Module Name:  CSA.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione di un Carry Save Adder per la somma di 3
--					  operandi con 8 bit ciascuno. Il primo blocco di Full Adder
-- 				  calcola le somme parziali e senza propagazione di riporto,
-- 				  mentre gli FA a valle sommano i valori provenienti dal primo
-- 				  livello di componenti.
--
-- Dependencies: FA.vhd 
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity CSA is
Generic (N : integer := 8); 
    Port ( x : in  STD_LOGIC_VECTOR ((N-1) downto 0);
           y : in  STD_LOGIC_VECTOR ((N-1) downto 0);
           z : in  STD_LOGIC_VECTOR ((N-1) downto 0);
           result : out  STD_LOGIC_VECTOR ((N+1) downto 0)
		 );
end CSA;

architecture Structural of CSA is

	component FA is
		port ( a, b, cin: in STD_LOGIC;
			   sum, cout: out STD_LOGIC
		);
	end component;

	-- Segnali per la rete d'implementazione

	-- Segnali riporti full adder primo livello (blocco cs)e full adder a valle
	signal cs_v : STD_LOGIC_VECTOR ((N-1) downto 0) := (others => '0');
	signal cfa_v : STD_LOGIC_VECTOR ((N) downto 0) := (others => '0');

	-- Segnali somme parziali e finali
	signal tsum_v : STD_LOGIC_VECTOR ((N) downto 0) := (others => '0');
	signal sum_v : STD_LOGIC_VECTOR ((N+1) downto 0) := (others => '0');

begin

	tsum_v(N) <= '0';
	cfa_v(0) <= '0';

	cs_blocco: for i in (N-1) downto 0 generate 
	begin
		CS_Line : FA 
		port map(a=>x(i),b=>y(i), cin=>z(i), cout=>cs_v(i),sum=>tsum_v(i));
	end generate;
			
	fa_blocco: for j in (N-1) downto 0 generate
	begin
	FA_Line : FA	  
		port map(a=>tsum_v(j+1), b=>cs_v(j), cin=>cfa_v(j), cout =>cfa_v(j+1), sum=>sum_v(j+1));
	end generate;
			

	sum_v(0) <= tsum_v(0);
	sum_v(N+1) <= cfa_v(N);
	result <= sum_v;
	
end Structural;