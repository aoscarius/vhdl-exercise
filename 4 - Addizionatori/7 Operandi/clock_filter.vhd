----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:51:00 01/07/2014 
-- Design Name: 
-- Module Name:    clock_filter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clock_filter is
generic(	clock_frequency_in : integer := 50000000;
			clock_frequency_out : integer := 5000000);
	port(	clk_in : in std_logic; 
			reset : in  STD_LOGIC;		
			clk_out : out std_logic);
end clock_filter;

architecture Behavioral of clock_filter is

signal clock: std_logic := '0';
constant max_value : integer := clock_frequency_in/(clock_frequency_out)-1;

begin

clk_out <= clock;

main: process(clk_in, reset)

variable counter : integer range 0 to max_value := 0;

begin

	if reset = '1' then
		counter := 0;
		clock <= '0';
	elsif rising_edge(clk_in) then
		if counter = max_value then
			clock <=  '1';
			counter := 0;
		else
			clock <=  '0';
			counter := counter + 1;
		end if;
	end if;

end process;

end Behavioral;

