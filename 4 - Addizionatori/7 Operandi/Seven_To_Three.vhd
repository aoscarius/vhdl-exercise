----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:13:26 01/14/2014 
-- Design Name: 
-- Module Name:    Seven_To_Three - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.log_functions.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- Componente sommatore che comprime 7 bit in 3 bit
entity Seven_To_Three is
	Port(x0: in STD_LOGIC; -- Primo bit dei sette bit in ingresso
		  x1: in STD_LOGIC; -- Secondo bit dei sette bit in ingresso
		  x2: in STD_LOGIC; -- Terzo bit dei sette bit in ingresso
		  x3: in STD_LOGIC; -- Quarto bit dei sette bit in ingresso
		  x4: in STD_LOGIC; -- Quinto bit dei sette bit in ingresso
		  x5: in STD_LOGIC; -- Sesto bit dei sette bit in ingresso
		  x6: in STD_LOGIC; -- Settimo bit dei sette bit in ingresso
		  y0: out STD_LOGIC; -- Primo bit dei tre bit in uscita (riporto della somma di c1, c2, c3)
		  y1: out STD_LOGIC; -- Secondo bit dei tre bit in uscita (somma di c1, c2, c3)
		  y2: out STD_LOGIC); -- Terzo bit dei tre bit in uscita (somma di s1, s2, x6)
end Seven_To_Three;

architecture Structural of Seven_To_Three is
	
	-- Dichiarazione del componente Full_Adder
	component Full_Adder is
		Port(x: in STD_LOGIC;
			  y: in STD_LOGIC;
			  c_in: in STD_LOGIC;
			  c_out: out STD_LOGIC;
			  s: out STD_LOGIC);
	end component;
	
	signal s1: std_logic; -- Somma dei bit x3, x4, x5
	signal s2: std_logic; -- Somma dei bit x0, x1, x2
	signal c1: std_logic; -- Riporto della somma dei bit x3, x4, x5
	signal c2: std_logic; -- Riporto della somma dei bit x0, x1, x2
	signal c3: std_logic; -- Riporto della somma dei bit s1, s2, x6 
	
	begin
		
		-- Istanzazione del componente Full_Adder
		FULL_ADDER_1: Full_Adder
			port map(x => x3,
						y => x4,
						c_in => x5,
						c_out => c1,
						s => s1);
		
		-- Istanzazione del componente Full_Adder
		FULL_ADDER_2: Full_Adder
			port map(x => x0,
						y =>x1,
						c_in => x2,
						c_out => c2,
						s => s2);
		
		-- Istanzazione del componente Full_Adder
		FULL_ADDER_3: Full_Adder
			port map(x => s2,
						y => s1,
						c_in => x6,
						c_out => c3,
						s => y2);
		
		-- Istanzazione del componente Full_Adder
		FULL_ADDER_4: Full_Adder
			port map(x => c2,
						y => c1,
						c_in => c3,
						c_out => y0,
						s => y1);
		
end Structural;

