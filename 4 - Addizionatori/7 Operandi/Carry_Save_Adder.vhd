----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:58:25 01/14/2014 
-- Design Name: 
-- Module Name:    Carry_Save_Adder - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.log_functions.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- Componente addizionatore di tre array su 8 bit con risultato su 10 bit
entity Carry_Save_Adder is
	Port(u: in STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Primo array su 8 bit
		  v: in STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Secondo array su 8 bit
		  w: in STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Terzo array su 8 bit
		  s: out STD_LOGIC_VECTOR(reg_width+1 downto 0)); -- Somma su 10 bit di u, v e w
end Carry_Save_Adder;

architecture Structural of Carry_Save_Adder is
	
	-- Dichiarazione del componente Three_To_Two_Stored_Carry_Encoder
	component Three_To_Two_Stored_Carry_Encoder is
		port(u: in STD_LOGIC_VECTOR(reg_width-1 downto 0);
			  v: in STD_LOGIC_VECTOR(reg_width-1 downto 0);
			  w: in STD_LOGIC_VECTOR(reg_width-1 downto 0);
			  c: out STD_LOGIC_VECTOR(reg_width-1 downto 0);
			  s: out STD_LOGIC_VECTOR(reg_width-1 downto 0));
	end component;
	
	-- Dichiarazione del componente Full_Adder_N_bit
	component Full_Adder_N_bit is
		port(x: in STD_LOGIC_VECTOR(reg_width-1 downto 0);
			  y: in STD_LOGIC_VECTOR(reg_width-1 downto 0);
			  c_in: in STD_LOGIC;
			  c_out: out STD_LOGIC;
			  s: out STD_LOGIC_VECTOR (reg_width downto 0));
	end component;
	
	
	signal c_temp: std_logic_vector(reg_width-1 downto 0); -- Riporto della somma di u, v e w
	signal s2_temp: std_logic_vector(reg_width-1 downto 0); -- Somma di u, v e w
	signal r: std_logic_vector(reg_width-1 downto 0);
	signal s1_temp : std_logic_vector(reg_width downto 0); -- Somma su 9 bit di r, c_temp e riporto entante zero
	signal c_out_temp: std_logic; -- Riporto della somma di r, c_temp e riporto 0 (ultimo riporto)
	
	begin
		
		-- Istanzazione del componente Three_To_Two_Stored_Carry_Encoder
		c3t2: Three_To_Two_Stored_Carry_Encoder
			port map(u => u,
						v => v,
						w => w,
						c => c_temp,
						s => s2_temp);
						
		r <= '0' & s2_temp(reg_width-1 downto 1);
		
		-- Istanzazione del componente Full_Adder_N_bit
		FULL_ADDER_NBIT: Full_Adder_N_bit
			port map(x => r,
						y => c_temp,
						c_in => '0',
						c_out => c_out_temp,
						s => s1_temp);
						
		-- La somma � data dall'unione delle somme temporanee s1_temp e s2_temp(0)
		s <= s1_temp & s2_temp(0);
		
end Structural;