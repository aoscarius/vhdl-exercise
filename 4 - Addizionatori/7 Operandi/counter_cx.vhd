----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:35:34 11/22/2012 
-- Design Name: 
-- Module Name:    counter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use work.log_functions.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity counter_cx is
	 Port ( clk : in  STD_LOGIC;
           count_up : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
			  reg : out STD_LOGIC_VECTOR(6 downto 0);
           value : out  STD_LOGIC_VECTOR (7 downto 0));
end counter_cx;

architecture Behavioral of counter_cx is

signal reset : std_logic;
signal temp,max: integer :=0;
signal sel : integer :=1;

begin

reset <= not reset_n;
max <= 129;
value <= conv_std_logic_vector(temp, 8);
reg <= conv_std_logic_vector(sel, 7);
	
counter_process: process(clk, reset, count_up)
variable i: integer :=0;
begin
	if reset = '1' then
		i := 0;
		temp <= 0;
		sel <= 1;
	elsif rising_edge(clk) and count_up = '1' then
		if temp < max then
			temp <= 2**i;
			sel <= 2**(i+1);
			i := i+1;
		else
			temp <= 0;
			sel <= 1;
			i := 0;
		end if;
	end if;

end process;

end Behavioral;

