----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:35:58 01/14/2014 
-- Design Name: 
-- Module Name:    Seven_To_Three_Stored_Carry_Encoder - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.log_functions.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- Componente che riduce i sette operandi in tre array (u, v, w)
entity Seven_To_Three_Stored_Carry_Encoder is
	Port(r0: in STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Primo operando su 8 bit
		  r1: in STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Secondo operando su 8 bit
		  r2: in STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Terzo operando su 8 bit
		  r3: in STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Quarto operando su 8 bit
		  r4: in STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Quinto operando su 8 bit
		  r5: in STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Sesto operando su 8 bit
		  r6: in STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Settimo operando su 8 bit
		  u: out STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Array u su 8 bit
		  v: out STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Array v su 8 bit
		  w: out STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Array w su 8 bit
		  v8: out STD_LOGIC; -- Riporto v8 su 1 bit
		  w8: out STD_LOGIC; -- Riporto w8 su 1 bit
		  w9: out STD_LOGIC); -- Riporto w9 su 1 bit
end Seven_To_Three_Stored_Carry_Encoder;

architecture Structural of Seven_To_Three_Stored_Carry_Encoder is
	
	-- Dichiarazione del componente Seven_To_Three
	component Seven_To_Three is
		Port(x0: in STD_LOGIC;
			  x1: in STD_LOGIC;
			  x2: in STD_LOGIC;
			  x3: in STD_LOGIC;
			  x4: in STD_LOGIC;
			  x5: in STD_LOGIC;
			  x6: in STD_LOGIC;
			  y0: out STD_LOGIC;
			  y1: out STD_LOGIC;
			  y2: out STD_LOGIC);
	end component;
	
	begin
		
		v(0) <= '0';
		w(1) <= '0';
		w(0) <= '0';
		
		-- Generazione dei Seven_To_Three
		STT: for i in 0 to reg_width-3 generate
					-- Istanzazione del componente Seven_To_Three
					SEVEN_TO_THREE_1: Seven_To_Three
						port map(x0 => r0(i),
									x1 => r1(i),
									x2 => r2(i),
									x3 => r3(i),
									x4 => r4(i),
									x5 => r5(i),
									x6 => r6(i),
									y0 => w(i+2),
									y1 => v(i+1),
									y2 => u(i));
			  end generate;
			
		-- Istanzazione del componente Seven_To_Three
		SEVEN_TO_THREE_2: Seven_To_Three
									port map(x0 => r0(reg_width-2),
												x1 => r1(reg_width-2),
												x2 => r2(reg_width-2),
												x3 => r3(reg_width-2),
												x4 => r4(reg_width-2),
												x5 => r5(reg_width-2),
												x6 => r6(reg_width-2),
												y0 => w8,
												y1 => v(reg_width-1),
												y2 => u(reg_width-2));
												
		-- Istanzazione del componente Seven_To_Three
		SEVEN_TO_THREE_3: Seven_To_Three
									port map(x0 => r0(reg_width-1),
												x1 => r1(reg_width-1),
												x2 => r2(reg_width-1),
												x3 => r3(reg_width-1),
												x4 => r4(reg_width-1),
												x5 => r5(reg_width-1),
												x6 => r6(reg_width-1),
												y0 => w9,
												y1 => v8,
												y2 => u(reg_width-1));
												
end Structural;