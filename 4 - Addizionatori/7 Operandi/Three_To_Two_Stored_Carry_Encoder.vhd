----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:07:18 01/14/2014 
-- Design Name: 
-- Module Name:    Three_To_Two_Stored_Carry_Encoder - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.log_functions.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- Componente sommatore di tre array in modo posizionale
entity Three_To_Two_Stored_Carry_Encoder is
	port(u: in STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Primo array su 8 bit
		  v: in STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Secondo array su 8 bit
		  w: in STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Terzo array su 8 bit
		  c: out STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Riporti degli otto stadi
		  s: out STD_LOGIC_VECTOR(reg_width-1 downto 0)); -- Somme degli otto stadi
end Three_To_Two_Stored_Carry_Encoder;

architecture Structural of Three_To_Two_Stored_Carry_Encoder is
	
	-- Dichiarazione del componente Full_Adder
	component Full_Adder is
		Port(x: in STD_LOGIC;
			  y: in STD_LOGIC;
			  c_in: in STD_LOGIC;
			  c_out: out STD_LOGIC;
			  s: out STD_LOGIC);
	end component;
	
	begin
		
		-- Generazione degli stadi in cui viene calcalata la somma s(i) = u(i) + v(i) + w(i) e il riporto c(i)
		TTTSCE: for i in 0 to reg_width-1 generate
						-- Istanzazione del componente Full_Adder
						FULL_ADDER_I: Full_Adder
							port map(x => u(i),
										y => v(i),
										c_in => w(i),
										c_out => c(i),
										s => s(i));
				  end generate;
		
end Structural;