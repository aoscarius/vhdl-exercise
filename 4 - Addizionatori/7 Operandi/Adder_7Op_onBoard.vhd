----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:14:30 01/16/2014 
-- Design Name: 
-- Module Name:    Adder_7Op_onBoard - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Adder_7Op_onBoard is
Port (	clk: in STD_LOGIC;
			button: in STD_LOGIC;
			load: in STD_LOGIC;
			view: in STD_LOGIC;
			reset: in STD_LOGIC;
			in_byte: in STD_LOGIC_VECTOR(7 downto 0);
			cathodes: out STD_LOGIC_VECTOR(7 downto 0);
			led_reg: out STD_LOGIC_VECTOR(6 downto 0);
			led: out STD_LOGIC;
			anodes: out STD_LOGIC_VECTOR(3 downto 0));
end Adder_7Op_onBoard;

architecture Behavioral of Adder_7Op_onBoard is

component Adder_7Operandi
	port(r0: in STD_LOGIC_VECTOR(7 downto 0); -- Primo operando su 8 bit
		  r1: in STD_LOGIC_VECTOR(7 downto 0); -- Secondo operando su 8 bit
		  r2: in STD_LOGIC_VECTOR(7 downto 0); -- Terzo operando su 8 bit
		  r3: in STD_LOGIC_VECTOR(7 downto 0); -- Quarto operando su 8 bit
		  r4: in STD_LOGIC_VECTOR(7 downto 0); -- Quinto operando su 8 bit
		  r5: in STD_LOGIC_VECTOR(7 downto 0); -- Sesto operando su 8 bit
		  r6: in STD_LOGIC_VECTOR(7 downto 0); -- Settimo operando su 8 bit
		  s: out STD_LOGIC_VECTOR(10 downto 0)); -- Somma su 11 bit dei sette operandi
end component;

component Controller 
port(			clk : in std_logic;
				reset : in std_logic;
				button : in std_logic;
				load : in std_logic;
				view : in std_logic;
				count_sel: in STD_LOGIC_VECTOR(7 downto 0);
				view_reg : in STD_LOGIC_VECTOR(6 downto 0);
				led_reg: out STD_LOGIC_VECTOR(6 downto 0);
				led: out STD_LOGIC;
				go_on : out STD_LOGIC;
				resetC: out STD_LOGIC;
				reg_sel : out STD_LOGIC_VECTOR(7 downto 0);
				mux_sel: out STD_LOGIC_VECTOR(6 downto 0);
				en : out std_logic_vector(3 downto 0));
end component;

component Display7Seg
Generic(		clock_frequency_in : integer := 50000000;
				clock_frequency_out : integer := 400);
 Port ( 		clk : in  STD_LOGIC;
				reset : in  STD_LOGIC;
				value : in  STD_LOGIC_VECTOR (15 downto 0);
				enable : in  STD_LOGIC_VECTOR (3 downto 0);
				dots : in  STD_LOGIC_VECTOR (3 downto 0);
				anodes : out  STD_LOGIC_VECTOR (3 downto 0);
				cathodes : out  STD_LOGIC_VECTOR (7 downto 0));
end component;

component Mux_generic
    Port ( a : in  STD_LOGIC_VECTOR(15 downto 0);
           b : in  STD_LOGIC_VECTOR(15 downto 0);
           c : in  STD_LOGIC_VECTOR(15 downto 0);
           d : in  STD_LOGIC_VECTOR(15 downto 0);
			  e : in  STD_LOGIC_VECTOR(15 downto 0);
           f : in  STD_LOGIC_VECTOR(15 downto 0);
           g : in  STD_LOGIC_VECTOR(15 downto 0);
           h : in  STD_LOGIC_VECTOR(15 downto 0);
			  i : in STD_LOGIC_VECTOR(15 downto 0);
           sel : in  STD_LOGIC_VECTOR(7 downto 0);
			  o : out  STD_LOGIC_VECTOR(15 downto 0));
end component;

component Registro
    Generic( width : natural := 8);
    Port ( 
			  d : in  STD_LOGIC_VECTOR (width-1 downto 0);	
           clk : in  STD_LOGIC;									
           reset : in  STD_LOGIC;								
           en : in  STD_LOGIC;									
			  q : out  STD_LOGIC_VECTOR (width-1 downto 0));
end component;

component counter_cx 
	 Port ( clk : in  STD_LOGIC;
           count_up : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
			  reg : out STD_LOGIC_VECTOR(6 downto 0);
           value : out  STD_LOGIC_VECTOR (7 downto 0));
end component;

component filter 
    Port ( button : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           reset_n : in  STD_LOGIC;
           enable : out  STD_LOGIC);
end component;

signal reset_n,go_on,enable_fx,resetC,resetC_n : STD_LOGIC := '0';
signal uscitaMux,updateA,updateB,updateC,updateD,updateE,updateF,updateG,updateH,updateI: STD_LOGIC_VECTOR(15 downto 0):= (others=>'0'); 
signal en : STD_LOGIC_VECTOR(3 downto 0):= (others=>'0'); 
signal sel,view_reg: STD_LOGIC_VECTOR(6 downto 0):= (others=> '0');
signal abilita,reg_sel,one,two,three,four,five,six,zero : STD_LOGIC_VECTOR(7 downto 0) := (others=>'0');
signal somma : STD_LOGIC_VECTOR(10 downto 0):= (others=> '0');

begin

reset_n <= not reset;
resetC_n <= not resetC;

DISPLAY: Display7Seg
Generic map(clock_frequency_in => 50000000,
				clock_frequency_out => 400)
 Port map(	clk => clk,
				reset =>reset,
				value =>uscitaMux,
				enable => en,
				dots => x"0",
				anodes => anodes,
				cathodes => cathodes
				);

FILTER_EN: filter 
    Port map( 	button =>go_on,
					clk =>clk,
					reset_n =>reset_n,
					enable =>enable_fx
					);

COUNTER_EN: counter_cx
    Port map(	clk =>clk,
					count_up => enable_fx,
					reset_n =>resetC_n,
					reg => view_reg,
					value =>abilita
				);

ADDER: Adder_7Operandi
port map(r0=>zero,
			r1=>one,
			r2=>two,
			r3=>three,
			r4=>four,
			r5=>five,
			r6=>six,
			s=>somma
		  );
		  
CU: Controller 
port map(	clk => clk,
				reset => reset,
				button => button,
				load => load,
				view => view,
				count_sel => abilita,
				view_reg => view_reg,
				led_reg => led_reg,
				led => led,
				go_on => go_on,
				mux_sel=> sel,
				resetC => resetC,
				reg_sel => reg_sel,
				en => en
		);
			
REG_0: Registro
Generic map( width => 8)
    Port map(	d => in_byte,	
					clk => clk,									
					reset => reset,								
					en =>sel(0),									
					q => zero
					);

REG_1: Registro
Generic map( width => 8)
    Port map(	d => in_byte,	
					clk => clk,									
					reset=> reset,								
					en =>sel(1),									
					q => one
					);

REG_2: Registro
Generic map( width => 8)
    Port map(	d => in_byte,	
					clk => clk,									
					reset =>reset,								
					en =>	sel(2),					
					q => two
					);

REG_3: Registro
Generic map( width => 8)
    Port map(	d => in_byte,	
					clk => clk,									
					reset=> reset,								
					en =>	sel(3),								
					q => three
					);
			  
REG_4: Registro
Generic map( width => 8)
    Port map(	d => in_byte,	
					clk => clk,									
					reset =>reset,								
					en =>	sel(4),								
					q => four
					);
			  
REG_5: Registro
Generic map( width => 8)
    Port map(	d => in_byte,	
					clk => clk,									
					reset =>reset,								
					en =>	sel(5),								
					q => five
					);
			  
REG_6: Registro
Generic map( width => 8)
    Port map(	d => in_byte,	
					clk => clk,									
					reset =>reset,								
					en =>	sel(6),								
					q => six
					);

updateA <= x"10" & zero;
updateB <= x"20" & one;
updateC <= x"30" & two;
updateD <= x"40" & three;
updateE <= x"50" & four;
updateF <= x"60" & five;
updateG <= x"70" & six;
updateH <= "00000" & somma;
updateI <= (others => '0');
			  
MUX: Mux_generic
 Port map( a =>updateA,
           b =>updateB,
           c =>updateC,
           d =>updateD,
			  e =>updateE,
           f =>updateF,
           g =>updateG,
           h =>updateH,
			  i => updateI,
           sel => reg_sel,
			  o => uscitaMux
			 );

end Behavioral;

