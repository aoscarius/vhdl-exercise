----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:20:54 01/06/2014 
-- Design Name: 
-- Module Name:    Controller - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Controller is
port(			clk : in std_logic;
				reset : in std_logic;
				button : in std_logic;
				load : in std_logic;
				view : in std_logic;
				count_sel : in std_logic_vector(7 downto 0);
				view_reg : in STD_LOGIC_VECTOR(6 downto 0);
				led_reg: out STD_LOGIC_VECTOR(6 downto 0);
				led: out STD_LOGIC;
				go_on: out STD_LOGIC;
				reg_sel : out STD_LOGIC_VECTOR(7 downto 0);
				resetC: out STD_LOGIC;
				mux_sel: out STD_LOGIC_VECTOR(6 downto 0);
				en : out std_logic_vector(3 downto 0));
end Controller;

architecture Behavioral of Controller is

signal reg_tmp : STD_LOGIC_VECTOR(7 downto 0):= (others=>'0');
signal temp: STD_LOGIC_VECTOR(6 downto 0):= (others=>'0');
signal reg_enable : STD_LOGIC_VECTOR(3 downto 0) := "1011";
type stato is (idle,loading,visual,w8);
signal stato_prox,nxt : stato := idle;

begin

en <= reg_enable;
stato_prox <= nxt;
mux_sel <= temp;
reg_sel <= reg_tmp;

FSM_MAIN: process (clk,load,view,button,reset,stato_prox)
		begin

if reset='1' then
	led_reg <= (others =>'0');
	led <='1';
	reg_enable <= "1011";
	temp <= (others =>'0');
	reg_tmp <= (others =>'0');
	go_on <= '0';
	resetC <= '1';
	nxt <= idle;
	
elsif rising_edge(clk) then

	case  stato_prox is 
				
				when idle =>if button = '1' then
									nxt <= loading;
								elsif view = '1' then
									nxt <= w8;
								else	
									nxt <= idle;
								end if;
																 
				when loading=>	led <= '1';
									led_reg <= (others => '0');
									resetC <= '0';
									reg_tmp <= count_sel;
									if button = '1' then
											if count_sel < "10000000" then
													reg_enable <="1011";
											else
													reg_enable <="0111";
											end if;
											temp <= view_reg;
											go_on <= '1';
									else
											go_on <= '0';
											temp <= (others =>'0');
											nxt <= idle;
									end if;
										
				when w8 => 	temp <= (others =>'0');
								led_reg <= (others =>'0');
								resetC <= '1';
								go_on <= '0';
								reg_tmp <= (others =>'0');
								if view = '1' then
										led <= '0';
										reg_enable <="0011";
										nxt <= visual;
								else
										led <= '1';
										reg_enable <="1011";
										nxt <= idle;
								end if;
										
				
				when visual =>	led <= '0';
									temp <= (others =>'0');
									resetC <= '0';
									led_reg <= count_sel(6 downto 0);
									reg_tmp <= count_sel;
									if button = '1' then
											if count_sel < "10000000" then
													reg_enable <="0011";
											else
													reg_enable <="0111";
											end if;
											go_on <= '1';
									elsif load = '1' then
											nxt <= w8;
									else
											go_on <= '0';
											nxt <= visual;
									end if;
									
				when others => nxt <= idle;
				
			end case;
	end if;
end process;

end Behavioral;

