----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:54:51 01/14/2014 
-- Design Name: 
-- Module Name:    Adder_7Operandi - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.log_functions.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- Componente addizionatore a sette operandi su 8 bit
entity Adder_7Operandi is
	port(r0: in STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Primo operando su 8 bit
		  r1: in STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Secondo operando su 8 bit
		  r2: in STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Terzo operando su 8 bit
		  r3: in STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Quarto operando su 8 bit
		  r4: in STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Quinto operando su 8 bit
		  r5: in STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Sesto operando su 8 bit
		  r6: in STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Settimo operando su 8 bit
		  s: out STD_LOGIC_VECTOR(reg_width+2 downto 0)); -- Somma su 11 bit dei sette operandi
end Adder_7Operandi;

architecture Structural of Adder_7Operandi is
	
	-- Dichiarazione del componente Seven_To_Three_Stored_Carry_Encoder
	component Seven_To_Three_Stored_Carry_Encoder is
		Port(r0: in STD_LOGIC_VECTOR(reg_width-1 downto 0);
			  r1: in STD_LOGIC_VECTOR(reg_width-1 downto 0);
			  r2: in STD_LOGIC_VECTOR(reg_width-1 downto 0);
			  r3: in STD_LOGIC_VECTOR(reg_width-1 downto 0);
			  r4: in STD_LOGIC_VECTOR(reg_width-1 downto 0);
			  r5: in STD_LOGIC_VECTOR(reg_width-1 downto 0);
			  r6: in STD_LOGIC_VECTOR(reg_width-1 downto 0);
			  u: out STD_LOGIC_VECTOR(reg_width-1 downto 0);
			  v: out STD_LOGIC_VECTOR(reg_width-1 downto 0);
			  w: out STD_LOGIC_VECTOR(reg_width-1 downto 0);
			  v8: out STD_LOGIC;
			  w8: out STD_LOGIC;
			  w9: out STD_LOGIC);
	end component;
	
	-- Dichiarazione del componente Carry_Save_Adder
	component Carry_Save_Adder is
		Port(u: in STD_LOGIC_VECTOR(reg_width-1 downto 0);
			  v: in STD_LOGIC_VECTOR(reg_width-1 downto 0);
			  w: in STD_LOGIC_VECTOR(reg_width-1 downto 0);
			  s: out STD_LOGIC_VECTOR(reg_width+1 downto 0));
	end component;
	
	-- Dichiarazione del componente Full_Adder
	component Full_Adder is
		Port(x: in STD_LOGIC;
			  y: in STD_LOGIC;
			  c_in: in STD_LOGIC;
			  c_out: out STD_LOGIC;
			  s: out STD_LOGIC);
	end component;
	
	signal u_temp: std_logic_vector(reg_width-1 downto 0); -- Uscita u su 8 bit del Seven_To_Three_Stored_Carry_Encoder
	signal v_temp: std_logic_vector(reg_width-1 downto 0); -- Uscita v su 8 bit del Seven_To_Three_Stored_Carry_Encoder
	signal w_temp: std_logic_vector(reg_width-1 downto 0); -- Uscita w su 8 bit del Seven_To_Three_Stored_Carry_Encoder
	signal v8: std_logic; -- Riporto v8 del Seven_To_Three_Stored_Carry_Encoder
	signal w8: std_logic; -- Riporto w8 del Seven_To_Three_Stored_Carry_Encoder
	signal w9: std_logic; -- Riporto w9 del Seven_To_Three_Stored_Carry_Encoder
	signal s_10bit: std_logic_vector(reg_width+1 downto 0); -- Uscita del Carry_Save_Adder (somma parziale su 10 bit da sommare con v8, w8 e w9)
	signal c_temp: std_logic; -- Riporto della somma di v8, w8 e s_10bit(8)
	signal s8: std_logic; -- Somma di v8, w8 e s_10bit(8)
	signal s9: std_logic; -- Somma di s_10bit(9), w9 e c_temp
	signal s10: std_logic; -- Riporto della somma di s_10bit(9), w9 e c_temp
	
	begin
		
		-- Istanzazione del componente Seven_To_Three_Stored_Carry_Encoder
		STTSCE: Seven_To_Three_Stored_Carry_Encoder
			port map(r0 => r0,
						r1 => r1,
						r2 => r2,
						r3 => r3,
						r4 => r4,
						r5 => r5,
						r6 => r6,
						u => u_temp,
						v => v_temp,
						w => w_temp,
						v8 => v8,
						w8 => w8,
						w9 => w9);
						
		-- Istanzazione del componente Carry_Save_Adder
		CSA: Carry_Save_Adder
			port map(u => u_temp,
						v => v_temp,
						w => w_temp,
						s => s_10bit);
						
		-- Istanzazione del componente Full_Adder
		FULL_ADDER_1: Full_Adder
			port map(x => v8,
						y => w8,
						c_in => s_10bit(reg_width),
						c_out => c_temp,
						s => s8);
						
		-- Istanzazione del componente Full_Adder
		FULL_ADDER_2: Full_Adder
			port map(x => s_10bit(reg_width+1),
						y => w9,
						c_in => c_temp,
						c_out => s10,
						s => s9);
						
		-- Il risultato finale � dato dall'unione di s10, s9, s8, s7, s6, s5, s4, s3, s2, s1, s0 
		s <= s10 & s9 & s8 & s_10bit(reg_width-1 downto 0);
		
end Structural;