----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:26:06 01/14/2014 
-- Design Name: 
-- Module Name:    FullAdder_N_bit - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.log_functions.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- Componente addizionatore a n bit
entity Full_Adder_N_bit is
	port(x: in STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Primo array su 8 bit
		  y: in STD_LOGIC_VECTOR(reg_width-1 downto 0); -- Secondo array su 8 bit
		  c_in: in STD_LOGIC; -- Riporto entrante
		  c_out: out STD_LOGIC; -- Riporto della somma degli array x ed y
		  s: out STD_LOGIC_VECTOR (reg_width downto 0)); -- Somma su 9 bit degli array x ed y
end Full_Adder_N_bit;

architecture Structural of Full_Adder_N_bit is
	
	-- Dichiarazione del componente Full_Adder
	component Full_Adder is
		Port(x: in STD_LOGIC;
			  y: in STD_LOGIC;
			  c_in: in STD_LOGIC;
			  c_out: out STD_LOGIC;
			  s: out STD_LOGIC);
	end component;
	
	signal carry_temp: std_logic_vector(reg_width-1 downto 0); -- Vettore dei riporti dei primi sette stadi
	signal carry_out_temp: std_logic; -- Riporto dell'ottavo stadio
	signal s_temp: std_logic_vector(reg_width downto 0); -- Somma temporanea su 8 bit
	
	begin
		
		-- Istanzazione del componente Full_Adder
		FULL_ADDER_1: Full_Adder
			port map(x => x(0),
						y => y(0),
						c_in => c_in,
						c_out => carry_temp(0),
						s => s_temp(0));
						
		FULL_ADDER_INTERNI:
			for i in 1 to reg_width-2 generate
				FULL_ADDER_I: Full_Adder
					port map(x => x(i),
								y => y(i),
								c_in => carry_temp(i-1),
								c_out => carry_temp(i),
								s => s_temp(i));
			end generate;
		
		FULL_ADDER_FINALE: Full_Adder
			port map(x => x(reg_width-1),
						y => y(reg_width-1),
						c_in => carry_temp(reg_width-2),
						c_out => carry_out_temp,
						s => s_temp(reg_width-1));
						
		-- Il riporto della somma degli array x ed y � dato dal riporto dell'ultimo stadio
		c_out <= carry_out_temp;
		-- Lo somma su 9 bit degli array x ed y � ottenuta dall'unione del riporto dell'ultimo stadio e dalla somma temporaneo su 8 bit
		s <= carry_out_temp & s_temp(reg_width-1 downto 0);
		
end Structural;