----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:03:34 01/14/2014 
-- Design Name: 
-- Module Name:    FullAdder - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.log_functions.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- Componente addizionatore a 1 bit
entity Full_Adder is
	Port(x: in STD_LOGIC; -- Primo bit
		  y: in STD_LOGIC; -- Secondo bit
		  c_in: in STD_LOGIC; -- Riporto entrante
		  c_out: out STD_LOGIC; -- Riporto della somma di x ed y
		  s: out STD_LOGIC); -- Somma dei bit x ed y
end Full_Adder;

architecture Behavioral of Full_Adder is
	
	begin
		
		-- La somma � data dall'operazione di xor tra i due bit di ingresso e il riporto entrante 
		s <= x xor y xor c_in;
		-- Il riporto della somma s � data dall'operazione di or tra le operazioni di and degli ingressi
		c_out <= ((x and y) or (x and c_in) or (y and c_in));
		
end Behavioral;