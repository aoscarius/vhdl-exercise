--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   13:33:29 01/15/2014
-- Design Name:   
-- Module Name:   C:/Progetti/33 Adder_7Operandi/tb_Adder_7Operandi.vhd
-- Project Name:  Adder_7Operandi
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Adder_7Operandi
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY tb_Adder_7Operandi IS
END tb_Adder_7Operandi;
 
ARCHITECTURE behavior OF tb_Adder_7Operandi IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Adder_7Operandi
    PORT(
         r0 : IN  std_logic_vector(7 downto 0);
         r1 : IN  std_logic_vector(7 downto 0);
         r2 : IN  std_logic_vector(7 downto 0);
         r3 : IN  std_logic_vector(7 downto 0);
         r4 : IN  std_logic_vector(7 downto 0);
         r5 : IN  std_logic_vector(7 downto 0);
         r6 : IN  std_logic_vector(7 downto 0);
         s : OUT  std_logic_vector(10 downto 0)
        );
    END COMPONENT;
    
   --Inputs
   signal r0 : std_logic_vector(7 downto 0) := (others => '0');
   signal r1 : std_logic_vector(7 downto 0) := (others => '0');
   signal r2 : std_logic_vector(7 downto 0) := (others => '0');
   signal r3 : std_logic_vector(7 downto 0) := (others => '0');
   signal r4 : std_logic_vector(7 downto 0) := (others => '0');
   signal r5 : std_logic_vector(7 downto 0) := (others => '0');
   signal r6 : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal s : std_logic_vector(10 downto 0);
   
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Adder_7Operandi PORT MAP (
          r0 => r0,
          r1 => r1,
          r2 => r2,
          r3 => r3,
          r4 => r4,
          r5 => r5,
          r6 => r6,
          s => s
        );
	
   -- Stimulus process
   stim_proc: process
		
		begin
			
			-- insert stimulus here
			
			-- TEST 1: Somma di 7 operandi "00000000"
			r0 <= "00000000";
			r1 <= "00000000";
			r2 <= "00000000";
			r3 <= "00000000";
			r4 <= "00000000";
			r5 <= "00000000";
			r6 <= "00000000";
			wait for 100 ns;
			
			-- TEST 2: Somma dei primi 7 numeri naturali
			r0 <= "00000001";
			r1 <= "00000010";
			r2 <= "00000011";
			r3 <= "00000100";
			r4 <= "00000101";
			r5 <= "00000110";
			r6 <= "00000111";
			wait for 100 ns;
			
			-- TEST 3: Somma di 7 operandi qualsiasi
			r0 <= "01011001";
			r1 <= "00101010";
			r2 <= "00110011";
			r3 <= "01100000";
			r4 <= "00001111";
			r5 <= "00101000";
			r6 <= "01000111";
			wait for 100 ns;

			-- TEST 4: Somma di 7 operandi "11111111"
			r0 <= "11111111";
			r1 <= "11111111";
			r2 <= "11111111";
			r3 <= "11111111";
			r4 <= "11111111";
			r5 <= "11111111";
			r6 <= "11111111";
			wait for 100 ns;
			
			wait;
			
	end process;
	
END;