-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Carry Look Ahead Adder
-- Create Date:  19/12/2014
-- Module Name:  CLANetwork.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione della rete di anticipazione del riporto per l'addizionatore CLA
--
-- Dependencies: CLA.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity CLANetwork is
    GENERIC(N: Integer := 8);
    Port ( p_i : in  STD_LOGIC_VECTOR ((N-1) downto 0);
           g_i : in  STD_LOGIC_VECTOR ((N-1) downto 0);
		     c0 : in STD_LOGIC;
           c_out : out  STD_LOGIC_VECTOR (N downto 0));
end CLANetwork;

architecture Structural of CLANetwork is
	COMPONENT CLA is
		Port ( p : in  STD_LOGIC;
			    g : in  STD_LOGIC;
			    cin : in  STD_LOGIC;
			    cout : out  STD_LOGIC);
	END COMPONENT;

	signal common_carry : STD_LOGIC_VECTOR (N downto 0) := (others => '0');

begin

	c_out <= common_carry;
	common_carry(0) <= c0;
	
	CLA_NET: for i in 0 to N-1 generate
		CLACell: CLA port map(p_i(i),g_i(i),common_carry(i),common_carry(i+1));
	end generate;

end Structural;

-- L'architettura classica per una rete di anticipazione di riporto, sebbene sia piu efficiente in termini di
-- velocit�, in quanto presenta un numero di livelli fissi e quindi un ritardo di elaborazione fisso, quando riportata
-- su un FPGA, perde questa sua caratteristica. Questo deriva dalla natura intrinseca dell'FPGA che mappa l'intera rete
-- nelle due versioni allo stesso identico modo, ovvero tramite la loro tabella di verit�, occupando cos� lo stesso numero 
-- di LUT e portando quindi ad un efficienza identica in termini di ritardo.

architecture Dataflow of CLANetwork is
begin
	c_out(0) <= c0;
	
	c_out(1) <= g_i(0) or 
	           (p_i(0) and c0);
				  
	c_out(2) <= g_i(1) or 
				  (p_i(1) and g_i(0)) or 
				  (p_i(1) and p_i(0) and c0);
	
	c_out(3) <= g_i(2) or 
				  (p_i(2) and g_i(1)) or 
				  (p_i(2) and p_i(1) and g_i(0)) or 
				  (p_i(2) and p_i(1) and p_i(0) and c0);
	
	c_out(4) <= g_i(3) or 
				  (p_i(3) and g_i(2)) or 
				  (p_i(3) and p_i(2) and g_i(1)) or 
				  (p_i(3) and p_i(2) and p_i(1) and g_i(0)) or 
				  (p_i(3) and p_i(2) and p_i(1) and p_i(0) and c0);
	
	c_out(5) <= g_i(4) or 
				  (p_i(4) and g_i(3)) or 
				  (p_i(4) and p_i(3) and g_i(2)) or 
				  (p_i(4) and p_i(3) and p_i(2) and g_i(1)) or 
				  (p_i(4) and p_i(3) and p_i(2) and p_i(1) and g_i(0)) or 
				  (p_i(4) and p_i(3) and p_i(1) and p_i(1) and p_i(0) and c0);
	
	c_out(6) <= g_i(5) or 
				  (p_i(5) and g_i(4)) or 
				  (p_i(5) and p_i(4) and g_i(3)) or 
				  (p_i(5) and p_i(4) and p_i(3) and g_i(2)) or 
				  (p_i(5) and p_i(4) and p_i(3) and p_i(2) and g_i(1)) or 
				  (p_i(5) and p_i(4) and p_i(3) and p_i(2) and p_i(1) and g_i(0)) or 
				  (p_i(5) and p_i(4) and p_i(3) and p_i(2) and p_i(1) and p_i(0) and c0);
	
	c_out(7) <= g_i(6) or 
				  (p_i(6) and g_i(5)) or 
				  (p_i(6) and p_i(5) and g_i(4)) or 
				  (p_i(6) and p_i(5) and p_i(4) and g_i(3)) or 
				  (p_i(6) and p_i(5) and p_i(4) and p_i(3) and g_i(2)) or 
				  (p_i(6) and p_i(5) and p_i(4) and p_i(3) and p_i(2) and g_i(1)) or 
				  (p_i(6) and p_i(5) and p_i(4) and p_i(3) and p_i(2) and p_i(1) and g_i(0)) or 
				  (p_i(6) and p_i(5) and p_i(4) and p_i(3) and p_i(2) and p_i(1) and p_i(0) and c0);
	
	c_out(8) <= g_i(7) or 
				  (p_i(7) and g_i(6)) or 
				  (p_i(7) and p_i(6) and g_i(5)) or 
				  (p_i(7) and p_i(6) and p_i(5) and g_i(4)) or 
				  (p_i(7) and p_i(6) and p_i(5) and p_i(4) and g_i(3)) or 
				  (p_i(7) and p_i(6) and p_i(5) and p_i(4) and p_i(3) and g_i(2)) or 
				  (p_i(7) and p_i(6) and p_i(5) and p_i(4) and p_i(3) and p_i(2) and g_i(1)) or 
				  (p_i(7) and p_i(6) and p_i(5) and p_i(4) and p_i(3) and p_i(2) and p_i(1) and g_i(0)) or 
				  (p_i(7) and p_i(6) and p_i(5) and p_i(4) and p_i(3) and p_i(2) and p_i(1) and p_i(0) and c0);
end Dataflow;
