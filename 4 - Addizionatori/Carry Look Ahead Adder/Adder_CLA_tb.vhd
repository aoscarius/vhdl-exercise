-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Generic Adder
-- Create Date:  24/11/2014
-- Module Name:  FA.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Testbench del Carry Look Ahead Adder ad N bit
--
-- Dependencies: none
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY Adder_CLA_tb IS
END Adder_CLA_tb;
 
ARCHITECTURE behavior OF Adder_CLA_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
    COMPONENT Adder_CLA
     GENERIC( bit_depth: Integer := 8);
     Port ( x_in, y_in: in  STD_LOGIC_VECTOR ((bit_depth-1) downto 0);
		   c_in : in STD_LOGIC;
		   nadd_sub: in STD_LOGIC;
           sum : out  STD_LOGIC_VECTOR ((bit_depth-1) downto 0);
		   overflow : out STD_LOGIC; 
		   c_out : out STD_LOGIC);
    END COMPONENT;

   --Inputs
   signal x_in : std_logic_vector(7 downto 0) := (others => '0');
   signal y_in : std_logic_vector(7 downto 0) := (others => '0');
   signal c_in : std_logic := '0';
	signal nadd_sub : STD_LOGIC := '0';

 	--Outputs
   signal sum : std_logic_vector(7 downto 0);
	signal overflow : std_logic;
   signal c_out : std_logic;

BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Adder_CLA PORT MAP (
          x_in => x_in,
          y_in => y_in,
          c_in => c_in,
			 nadd_sub => nadd_sub,
          sum => sum,
			 overflow => overflow,
          c_out => c_out
        );

   -- Stimulus process
   stim_proc: process
   begin		
		c_in <= '0';
		nadd_sub <= '0';
		
		x_in <= "10101111";
		y_in <= "11110000";
		
		wait for 10 ns;
		
		x_in <= "01001111";
		y_in <= "11110101";
		
		wait for 10 ns;
		
		c_in <= '1';

		x_in <= "00101111";
		y_in <= "01010010";

		wait for 10 ns;

		c_in <= '0';
		nadd_sub <= '1';
		
		x_in <= "10101111";
		y_in <= "11110000";
		
		wait for 10 ns;
		
		x_in <= "01001111";
		y_in <= "11110101";
		
		wait for 10 ns;
		
		c_in <= '1';

		x_in <= "00101111";
		y_in <= "01010010";

		wait for 10 ns;
		
   end process;

END;
