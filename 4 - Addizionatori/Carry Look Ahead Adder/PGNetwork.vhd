-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Carry Look Ahead Adder
-- Create Date:  19/12/2014
-- Module Name:  PGNetwork.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione della rete di generazione e propagazione per l'addizionatore CLA
--
-- Dependencies: PG.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity PGNetwork is
    GENERIC(N: Integer := 8);
    Port ( x_in : in  STD_LOGIC_VECTOR (N-1 downto 0);
           y_in : in  STD_LOGIC_VECTOR (N-1 downto 0);
           p_out : out  STD_LOGIC_VECTOR (N-1 downto 0);
           g_out : out  STD_LOGIC_VECTOR (N-1 downto 0));
end PGNetwork;

architecture Structural of PGNetwork is
	COMPONENT PG is
		Port ( x: in  STD_LOGIC;
			   y: in  STD_LOGIC;
			   p: out  STD_LOGIC;
			   g: out  STD_LOGIC);
	END COMPONENT;
	
begin

	PG_NET:for i in 0 to N-1 generate
		PGCell: PG port map(x_in(i),y_in(i),p_out(i),g_out(i));
	end generate;
	
end Structural;

