-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Carry Look Ahead Adder
-- Create Date:  19/12/2014
-- Module Name:  CLA.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione di una cella di anticipazione del riporto
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity CLA is
    Port ( p : in  STD_LOGIC;
           g : in  STD_LOGIC;
           cin : in  STD_LOGIC;
           cout : out  STD_LOGIC);
end CLA;

architecture Dataflow of CLA is
begin
	cout <= g or (cin and p);
end Dataflow;