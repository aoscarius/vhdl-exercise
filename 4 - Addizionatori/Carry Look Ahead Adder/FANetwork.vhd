-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Carry Look Ahead Adder
-- Create Date:  19/12/2014
-- Module Name:  FANetwork.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione del livello di Full Adder per l'addizionatore CLA
--
-- Dependencies: FA.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FANetwork is
    GENERIC(N: integer := 8);
    Port ( x_in, y_in, c_in : in  STD_LOGIC_VECTOR (N-1 downto 0);
           s_out : out  STD_LOGIC_VECTOR (N-1 downto 0));
end FANetwork;

architecture Structural of FANetwork is
	COMPONENT FA 
		port ( a, b, cin: in std_logic;
			   sum, cout: out std_logic);
	END COMPONENT;

begin

	FA_NET:for i in 0 to N-1 generate
		FACell: FA port map(x_in(i),y_in(i),c_in(i),s_out(i),open);
	end generate; 
	
end Structural;