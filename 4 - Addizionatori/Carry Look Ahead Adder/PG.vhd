-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Carry Look Ahead Adder
-- Create Date:  19/12/2014
-- Module Name:  PG.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione di una cella di generazione e propagazione
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity PG is
    Port ( x : in  STD_LOGIC;
           y : in  STD_LOGIC;
           p : out  STD_LOGIC;
           g : out  STD_LOGIC);
end PG;

architecture Dataflow of PG is
begin
	g <= x and y;
	p <= x or y;
end Dataflow;

