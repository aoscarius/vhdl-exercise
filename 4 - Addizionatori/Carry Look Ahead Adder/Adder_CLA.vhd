-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Carry Look Ahead Adder
-- Create Date:  19/12/2014
-- Module Name:  Adder_CLA.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione di un Adder ad anticipazione del riporto Carry Look Ahead
--
-- Dependencies: PGNetwork.vhd, PG.vhd, CLANetwork.vhd, CLA.vhd, FANetwork.vhd, FA.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Adder_CLA is
    GENERIC( bit_depth: Integer := 8);
    Port ( x_in, y_in: in  STD_LOGIC_VECTOR ((bit_depth-1) downto 0);
		   c_in : in STD_LOGIC;
		   nadd_sub: in STD_LOGIC;
           sum : out  STD_LOGIC_VECTOR ((bit_depth-1) downto 0);
		   overflow : out STD_LOGIC; 
		   c_out : out STD_LOGIC);
end Adder_CLA;

architecture Structural of Adder_CLA is

	COMPONENT PGNetwork
		GENERIC(N: Integer := bit_depth);
		Port ( x_in : in  STD_LOGIC_VECTOR ((N-1) downto 0);
			    y_in : in  STD_LOGIC_VECTOR ((N-1) downto 0);
			    p_out : out  STD_LOGIC_VECTOR ((N-1) downto 0);
			    g_out : out  STD_LOGIC_VECTOR ((N-1) downto 0));
	END COMPONENT;

	COMPONENT CLANetwork
		GENERIC(N: Integer := bit_depth);
		Port ( p_i : in  STD_LOGIC_VECTOR ((N-1) downto 0);
			    g_i : in  STD_LOGIC_VECTOR ((N-1) downto 0);
			    c0 : in STD_LOGIC;
			    c_out : out  STD_LOGIC_VECTOR (N downto 0));
	END COMPONENT;


	COMPONENT FANetwork 
		GENERIC(N: integer := bit_depth);
		Port ( x_in, y_in, c_in : in  STD_LOGIC_VECTOR (N-1 downto 0);
			    s_out : out  STD_LOGIC_VECTOR (N-1 downto 0));
	END COMPONENT;

	signal p_bus,g_bus: STD_LOGIC_VECTOR ((bit_depth-1) downto 0) := (others => '0');
	signal c_bus: STD_LOGIC_VECTOR (bit_depth downto 0) := (others => '0');
	
	-- Segnali per la rete di complementazione ----------------------------------
	signal yc_in: STD_LOGIC_VECTOR ((bit_depth-1) downto 0) := (others => '0');
	signal cc_in: STD_LOGIC := '0';
	constant xones: STD_LOGIC_VECTOR ((bit_depth-1) downto 0) := (others => '1');
	-----------------------------------------------------------------------------
	
	for all: CLANetwork use entity work.CLANetwork(Dataflow);
	
begin
	
	-- Rete di complementazione a 2 di y ------------------
	yc_in <= (y_in xor xones) when (nadd_sub='1') else y_in;
	cc_in <= (not c_in) when (nadd_sub='1') else c_in;
	-------------------------------------------------------

	PGNET: PGNetwork port map(x_in,yc_in,p_bus,g_bus);
	CLANET: CLANetwork port map(p_bus,g_bus,cc_in,c_bus);
	FANET: FANetwork port map(x_in,yc_in,c_bus((bit_depth-1) downto 0),sum);

	overflow <= c_bus(bit_depth) xor c_bus(bit_depth-1);
	c_out <= c_bus(bit_depth);

end Structural;

