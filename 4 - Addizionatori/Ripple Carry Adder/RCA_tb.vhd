-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Generic Adder
-- Create Date:  27/11/2014
-- Module Name:  RPCAddSub.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Testbench Ripple Carry con cella 
--               elementare di tipo Full Adder.
--
-- Dependencies: RCA.vhd, FA.vhd
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY RCA_tb IS
END RCA_tb;
 
ARCHITECTURE behavior OF RCA_tb IS 
 
   -- Component Declaration for the Unit Under Test (UUT)
 	COMPONENT RCA
		generic ( N: natural := 8 );
		port ( x_in, y_in: in  std_logic_vector ((N-1) downto 0);
			   c_in : in std_logic;
		       nadd_sub: in std_logic;
               sum : out  std_logic_vector ((N-1) downto 0);
		       overflow : out std_logic; 
		       c_out : out std_logic);
	END COMPONENT;
    

   --Inputs
   signal x_in : std_logic_vector(7 downto 0) := (others => '0');
   signal y_in : std_logic_vector(7 downto 0) := (others => '0');
   signal c_in : std_logic := '0';
   signal nadd_sub : std_logic := '0';

 	--Outputs
   signal sum : std_logic_vector(7 downto 0);
   signal overflow: std_logic;
   signal c_out : std_logic;
  
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: RCA PORT MAP (
          x_in => x_in,
          y_in => y_in,
          c_in => c_in,
          nadd_sub => nadd_sub,
          sum => sum,
		  overflow => overflow,
          c_out => c_out
        );

    -- Stimulus process
   stim_proc: process
   begin		
		c_in <= '0';
		nadd_sub <= '0';
		
		x_in <= "10101111";
		y_in <= "11110000";
		
		wait for 10 ns;
		
		x_in <= "01001111";
		y_in <= "11110101";
		
		wait for 10 ns;
		
		c_in <= '1';

		x_in <= "00101111";
		y_in <= "01010010";

		wait for 10 ns;

		c_in <= '0';
		nadd_sub <= '1';
		
		x_in <= "10101111";
		y_in <= "11110000";
		
		wait for 10 ns;
		
		x_in <= "01001111";
		y_in <= "11110101";
		
		wait for 10 ns;
		
		c_in <= '1';

		x_in <= "00101111";
		y_in <= "01010010";

		wait for 10 ns;
		
   end process;

END;
