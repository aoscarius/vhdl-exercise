-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ROM Mapped RPC Adder
-- Create Date:  24/11/2014
-- Module Name:  HF.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione di un Half Adder
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity HF is
	port ( a, b: in std_logic;
		   sum, cout: out std_logic
	);
end HF;

architecture Dataflow of HF is
begin
	sum <= ((not a) and b) or (a and (not b));
	cout <= a and b;
end Dataflow;