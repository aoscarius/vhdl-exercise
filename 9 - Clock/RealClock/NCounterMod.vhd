-------------------------------------------------------------------------------
-- Engineers: Apicella Alfredo
--            Biscardi Mariano Alfonso, 
--            Castello Oscar


-- 
-- Project Name: ASE Component Library
-- Create Date:  15/11/2014 
-- Module Name:  NCounterMod.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Contatore generico ad N bit, che conta per incrementi in modulo
--               di 2^N
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity NCounterMod is
	generic ( N : natural := 4;
				 hit_to : natural := 9
	);
	port ( clk, reset : in std_logic;
		   count_hit: out std_logic;   
		   count : out std_logic_vector((N-1) downto 0)
	); 
end NCounterMod;

architecture Behavioral of NCounterMod is

	constant max_count : std_logic_vector((N-1) downto 0) := conv_std_logic_vector(hit_to, N);
	signal tmp_cnt : std_logic_vector((N-1) downto 0) := (others => '0');

begin
	process(clk, reset, tmp_cnt)
	begin
		if (reset='1') then
			count_hit <= '0';
			tmp_cnt <= (others => '0');
		elsif (clk'event and clk='1') then
			tmp_cnt <= tmp_cnt + 1;
			if (tmp_cnt=max_count) then
				count_hit <= '1';
				tmp_cnt <= (others => '0');
			else
				count_hit <= '0';
			end if;	
		end if;
		
	end process;
	
	count <= tmp_cnt;
	
end Behavioral;