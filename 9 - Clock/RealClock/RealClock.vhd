-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Real Clock
-- Create Date:  28/12/2014 
-- Module Name:  RealClock.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione hardware di un orologio
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity RealClock is
	port ( clk : in std_logic;
			 hour_h_digit : out std_logic_vector (3 downto 0);
			 hour_l_digit : out std_logic_vector (3 downto 0);
			 min_h_digit : out std_logic_vector (3 downto 0);
			 min_l_digit : out std_logic_vector (3 downto 0);
			 sec_dp : out std_logic);
end RealClock;

architecture Behavioral of RealClock is

	component NCounterMod is
		generic ( N : natural := 4; hit_to : natural );
		port ( clk, reset : in std_logic;
				 count_hit: out std_logic;   
				 count : out std_logic_vector((N-1) downto 0)
		); 
	end component;
	
	signal reset : std_logic := '0';
	signal incML, incMH, incHL, incHH : std_logic := '0';
	signal tmpML, tmpMH, tmpHL, tmpHH : std_logic_vector (3 downto 0) := (others => '0');
	signal tmpSEC : std_logic_vector (7 downto 0) := (others => '0');
begin

	Sec		: NCounterMod
		generic map (N => 8, hit_to => 60)
		port map ( 
			clk => clk,
			reset => reset,
			count_hit => incML,
			count => tmpSEC
		);

	MD_Low	: NCounterMod
		generic map (hit_to => 9)
		port map ( 
			clk => incML,
			reset => reset,
			count_hit => incMH,
			count => tmpML
		);

	MD_Hight : NCounterMod
		generic map (hit_to => 5)
		port map ( 
			clk => incMH,
			reset => reset,
			count_hit => incHL,
			count => tmpMH
		);

	HD_Low	: NCounterMod
		generic map (hit_to => 9)
		port map ( 
			clk => incHL,
			reset => reset,
			count_hit => incHH,
			count => tmpHL
		);

	HD_Hight : NCounterMod
		generic map (hit_to => 2)
		port map ( 
			clk => incHH,
			reset => reset,
			count_hit => open,
			count => tmpHH
		);
		
	auto_reset: process (clk, tmpHH, tmpHL)
	begin
		if (tmpHH = "0010" and tmpHL = "0100") then
			reset <= '1';
		else 
			reset <= '0';
		end if;
	end process;	
	
	hour_h_digit <= tmpHH;
	hour_l_digit <= tmpHL;
	min_h_digit <= tmpMH;
	min_l_digit <= tmpML;
	sec_dp <= tmpSEC(0);

end Behavioral;

