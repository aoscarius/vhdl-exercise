-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Real Clock
-- Create Date:  28/12/2014 
-- Module Name:  Nexys2_RealClock.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione hardware di un orologio, test fisico sulla Nexys2
--
-- Dependencies: RealClock.vhd, ASELibrary.vhd, SSDriver.vhd, NCounterMod.vhd 
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Nexys2_RealClock is
	port (
		-- System Clock
		clk: in std_logic;
		-- 4 x Switch
--		sw : in std_logic_vector (0 to 3);
		-- 8 x Buttons 
--		btn : in std_logic_vector (0 to 7);
		-- 1 x JA Extension PAD
--		JA : out std_logic_vector (0 to 7);
		-- 8 x Leds
		led : out std_logic; --_vector(0 to 7);
		-- 1 x Seven Segment Display
		seg : out std_logic_vector(0 to 6);
		dp : out std_logic;
		an : out std_logic_vector(3 downto 0)
	);
end Nexys2_RealClock;

architecture Behavioral of Nexys2_RealClock is

	component clkGen is
		generic ( FHzIn : integer := 50000000; FHzOut : integer := 60000 );
		port ( clk_in : in  STD_LOGIC;
				 clk_out: out STD_LOGIC
	   );
	end component;

	component RealClock is
		port ( clk : in std_logic;
				 hour_h_digit : out std_logic_vector (3 downto 0);
				 hour_l_digit : out std_logic_vector (3 downto 0);
				 min_h_digit : out std_logic_vector (3 downto 0);
				 min_l_digit : out std_logic_vector (3 downto 0);
				 sec_dp : out std_logic);
	end component;

	component SSDriver is
		port ( d3 : in std_logic_vector(3 downto 0);
				d2 : in std_logic_vector(3 downto 0);
				d1 : in std_logic_vector(3 downto 0);
				d0 : in std_logic_vector(3 downto 0);
				clk_ref: in std_logic;
				seg : out std_logic_vector(0 to 6);
				dp : out std_logic;
				an : out std_logic_vector(3 downto 0)
		);
	end component;

	signal digit_3, digit_2, digit_1, digit_0 : std_logic_vector(3 downto 0) := (others => '0');
	signal clk_gen_refresh, clk_gen_1hz : std_logic := '0';
	
begin
	
	clk_1Hz: clkGen generic map (FHzOut=> 1) port map (clk, clk_gen_1hz);
	clk_refresh: clkGen generic map (FHzOut=> 400) port map (clk, clk_gen_refresh);
	
	RClock : RealClock port map (clk_gen_1hz, digit_3, digit_2, digit_1, digit_0, led);
	SSDisplay : SSDriver port map (digit_3, digit_2, digit_1, digit_0, clk_gen_refresh, seg, dp, an);

end Behavioral;

