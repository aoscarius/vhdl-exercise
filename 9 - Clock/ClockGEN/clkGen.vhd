-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  15/11/2014 
-- Module Name:  clkGen.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Generatore di un treno di impulsi di larghezza pari ad un 
--               periodo della frequenza di ingresso e avente una frequenza
--               di FHzOut
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity clkGen is
		generic ( FHzIn : integer := 50000000; FHzOut : integer := 60000);
		port ( clk_in : in  STD_LOGIC;
			   clk_out: out STD_LOGIC
	    );
end clkGen;

architecture Behavioral of clkGen is
    -- Costante contenente il numero di periodi da contare
	constant DivFactor: integer := (FHzIn/FHzOut)-1;
	
	signal clk_temp: STD_LOGIC := '0';
	signal counter : integer range 0 to DivFactor := 0;
begin
	freq_divider: process (clk_in) 
	begin
		if (clk_in'event and clk_in='1') then
			if counter = DivFactor then
				clk_temp <= '1';
				counter <= 0;
			else
				clk_temp <= '0';
				counter <= counter + 1; 
			end if;
		end if;
	end process;
	
    -- Uscita bufferizzata
	clk_out <= clk_temp;
end Behavioral;