--------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  15/11/2014 
-- Module Name:  clkDivSel.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description: Clock Divider a 16 livelli, restituisce in uscita un clock pari
--              ad un sottomultiplo di quello di ingresso. Fare riferimento 
--              alla seguente tabella per le specifiche di funzionamento.
--
--              ----- ----------- -------------------------
--             | sel |  clk_out  |     clk_out function    |
--              ----- ----------- -------------------------
--             | 0   | clk_in/2  | clk_out = clk_in / 2^0  |
--             | 1   | clk_in/4  | clk_out = clk_in / 2^2  |
--             | 2   | clk_in/6  | clk_out = clk_in / 2^4  |
--             | 3   | clk_in/8  | clk_out = clk_in / 2^6  |
--             | 4   | clk_in/10 | clk_out = clk_in / 2^8  |
--             | 5   | clk_in/12 | clk_out = clk_in / 2^10 |
--             | 6   | clk_in/14 | clk_out = clk_in / 2^12 |
--             | 7   | clk_in/16 | clk_out = clk_in / 2^14 |
--             | 8   | clk_in/18 | clk_out = clk_in / 2^16 |
--             | 9   | clk_in/20 | clk_out = clk_in / 2^18 |
--             | 10  | clk_in/22 | clk_out = clk_in / 2^20 |
--             | 11  | clk_in/24 | clk_out = clk_in / 2^22 |
--             | 12  | clk_in/26 | clk_out = clk_in / 2^24 |
--             | 13  | clk_in/28 | clk_out = clk_in / 2^26 |
--             | 14  | clk_in/30 | clk_out = clk_in / 2^28 | 
--             | 15  | clk_in/32 | clk_out = clk_in / 2^30 |
--              ----- ----------- -------------------------

-- Dependencies: none
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity clkDivSel is
	port ( sel : in std_logic_vector(3 downto 0);
		   reset : in std_logic;
		   clk_in : in std_logic;   
		   clk_out : out std_logic
	); 
end clkDivSel;

architecture Behavioral of clkDivSel is
	signal t_cnt : std_logic_vector(31 downto 0);
	signal t_ck_out : std_logic;
begin
	-- Processo di Clock Divider
	clk_counter:process(clk_in, reset)
	begin
		if reset = '1' then
			t_cnt <= (others => '0');
		elsif (clk_in'event and clk_in='1') then
			t_cnt <= t_cnt + 1;
		end if;
	end process;
	
	-- Processo di selezione del Clock di uscita
	clk_sel:process(sel)
	begin
		case sel is
			when "0000" => t_ck_out <= t_cnt(0);
			when "0001" => t_ck_out <= t_cnt(2);
			when "0010" => t_ck_out <= t_cnt(4);
			when "0011" => t_ck_out <= t_cnt(6);
			when "0100" => t_ck_out <= t_cnt(8);
			when "0101" => t_ck_out <= t_cnt(10);
			when "0110" => t_ck_out <= t_cnt(12);
			when "0111" => t_ck_out <= t_cnt(14);
			when "1000" => t_ck_out <= t_cnt(16);
			when "1001" => t_ck_out <= t_cnt(18);
			when "1010" => t_ck_out <= t_cnt(20);
			when "1011" => t_ck_out <= t_cnt(22);
			when "1100" => t_ck_out <= t_cnt(24);
			when "1101" => t_ck_out <= t_cnt(26);
			when "1110" => t_ck_out <= t_cnt(28);
			when "1111" => t_ck_out <= t_cnt(30);
			when others => t_ck_out <= 'X';
		end case; 
	end process;
	
	-- Uscita bufferizzata
	clk_out <= t_ck_out;
end Behavioral;