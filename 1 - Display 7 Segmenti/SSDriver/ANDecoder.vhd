-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: SevSegment Driver
-- Create Date:  15/11/2014 
-- Module Name:  ANDecoder.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Decoder per led timing division a logica attiva bassa
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ANDecoder is
	port ( sel : in std_logic_vector(1 downto 0);
		   an : out std_logic_vector(3 downto 0)
	);
end ANDecoder;

architecture Behavioral of ANDecoder is
begin
process(sel)
	begin
		case sel is
			when "00" => an <= "0111";
			when "01" => an <= "1011";
			when "10" => an <= "1101";
			when "11" => an <= "1110";
			when others => an <= "1111";
		end case; 
	end process;
end Behavioral;