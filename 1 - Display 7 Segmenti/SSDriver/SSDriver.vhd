-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: SevSegment Driver
-- Create Date:  15/11/2014 
-- Module Name:  SSDriver.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Driver per display a 7 segmenti a 4 cifre
--
-- Dependencies: SEGTranscoder.vhd, ANDecoder.vhd, BUSMux_4_1.vhd, NCounter.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SSDriver is
	port ( d3 : in std_logic_vector(3 downto 0);
		   d2 : in std_logic_vector(3 downto 0);
		   d1 : in std_logic_vector(3 downto 0);
		   d0 : in std_logic_vector(3 downto 0);
		   clk_ref: in std_logic;
		   seg : out std_logic_vector(0 to 6);
		   dp : out std_logic;
		   an : out std_logic_vector(3 downto 0)
	);
end SSDriver;

architecture Mixed of SSDriver is
	component SEGTranscoder
		port ( sel : in std_logic_vector(3 downto 0);		   
		       dp_in : in std_logic;
		       seg : out std_logic_vector(0 to 6); 
		       dp_out : out std_logic
		); 
	end component;

	component ANDecoder
		port ( sel : in std_logic_vector(1 downto 0);
		       an : out std_logic_vector(3 downto 0)
		);
	end component;

	component BUSMux_4_1
		generic ( N: natural := 4 );
		port (din_0 : in std_logic_vector((N-1) downto 0);   
			  din_1 : in std_logic_vector((N-1) downto 0); 
			  din_2 : in std_logic_vector((N-1) downto 0);
			  din_3 : in std_logic_vector((N-1) downto 0);
			  sel : in std_logic_vector(1 downto 0); 
			  dout : out std_logic_vector((N-1) downto 0)
		);
	end component;

	component NCounter
		generic ( N : natural := 2 );
		port ( clk, reset : in std_logic;   
			   cout : out std_logic_vector((N-1) downto 0)
		); 
	end component;

	-- Internal buffer for digit storage
	signal digit_3, digit_2, digit_1, digit_0 : std_logic_vector(3 downto 0) := (others => '0');
	-- Segnali di connessione
	signal sseg_data : std_logic_vector(3 downto 0);
	signal digit_sel : std_logic_vector(1 downto 0);
	
	for all: SEGTranscoder use entity work.SEGTranscoder(bhv_HEX);
	
begin
	-- Inserisco i segnali di ingresso
	digit_3 <= d3; digit_2 <= d2; digit_1 <= d1; digit_0 <= d0;
	
	bus_multiplexer: BUSMux_4_1 port map (digit_3, digit_2, digit_1, digit_0, digit_sel, sseg_data);
	SS_SEGTranscoder: SEGTranscoder port map (sseg_data, '1', seg, dp);
	SS_ANDecoder: ANDecoder port map (digit_sel, an);
	refresh_counter: NCounter port map (clk_ref, '0', digit_sel);

end Mixed;