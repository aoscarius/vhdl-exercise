-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: SevSegment Driver
-- Create Date:  15/11/2014 
-- Module Name:  SSDriver_tb.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Testbench del driver per display a 7 segmenti a 4 cifre
--
-- Dependencies: SEGTranscoder.vhd, ANDecoder.vhd, BUSMux_4_1.vhd, NCounter.vhd, SSDriver.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SSDriver_tb is
end SSDriver_tb;

architecture Mixed of SSDriver_tb is
	component SSDriver
		port ( d3 : in std_logic_vector(3 downto 0);
			   d2 : in std_logic_vector(3 downto 0);
			   d1 : in std_logic_vector(3 downto 0);
			   d0 : in std_logic_vector(3 downto 0);
			   clk_ref: in std_logic;
			   seg : out std_logic_vector(0 to 6);
			   dp : out std_logic;
			   an : out std_logic_vector(3 downto 0)
		);
	end component;
	-- Iputs
	signal d3, d2, d1, d0 : std_logic_vector(3 downto 0) := (others => '0');
	signal clk_ref : std_logic := '0';
	-- Outputs
	signal seg : std_logic_vector(0 to 6) := (others => '0');
	signal dp : std_logic := '0';
	signal an : std_logic_vector(3 downto 0) := (others => '0');

	-- Clock period definitions
	constant clk_period : time := 20 ns;
   
begin
	-- Instantiate the Unit Under Test (UUT)
	uut: SSDriver PORT MAP (
		d3 => d3,
		d2 => d2,
		d1 => d1,
		d0 => d0,
		clk_ref => clk_ref,
		seg => seg,
		dp => dp,
		an => an
	);

	-- Clock process definitions
	clk_process :process
	begin
		clk_ref <= '0';
		wait for clk_period/2;
		clk_ref <= '1';
		wait for clk_period/2;
	end process;

	-- Stimulus process
	stim_proc: process
	begin		
		wait for clk_period*2;
		d3 <= "0000";
		d2 <= "0010";
		d1 <= "0011";
		d0 <= "0100";
		wait for clk_period*8;
		wait;
	end process;	

end Mixed;