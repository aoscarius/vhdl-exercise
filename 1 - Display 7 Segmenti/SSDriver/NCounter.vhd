-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  15/11/2014 
-- Module Name:  NCounter.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Contatore generico ad N bit, che conta per incrementi in modulo
--               di 2^N
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity NCounter is
	generic ( N : natural := 8 );
	port ( clk, reset : in std_logic;   
		   cout : out std_logic_vector((N-1) downto 0)
	); 
end NCounter;

architecture Behavioral of NCounter is
	signal tmp_cnt : std_logic_vector((N-1) downto 0) := (others => '0');
begin
	process(clk, reset)
	begin
		if (reset='1') then
			tmp_cnt <= (others => '0');
		elsif (clk'event and clk='1') then
			tmp_cnt <= tmp_cnt + 1;
		end if;
	end process;
	cout <= tmp_cnt;
end Behavioral;