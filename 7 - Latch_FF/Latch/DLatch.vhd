-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  15/11/2014 
-- Module Name:  DLatch.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazioni di Latch di tipo D
--
-- Dependencies: none
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity DLatch is
	port ( d, en: in std_logic;
		   q : out std_logic
	);
end DLatch;

architecture Behavioral of DLatch is
begin
	process (en)
	begin
		if (en = '1') then
			q <= d;
		end if;
	end process;
end Behavioral;