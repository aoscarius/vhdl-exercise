-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  15/11/2014 
-- Module Name:  SRLatchPure.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazioni di Latch di tipo SR puro
--
-- Dependencies: none
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity SRLatch is
	port ( s,r: in std_logic;
		   q : out std_logic
	);
end SRLatch;

architecture Behavioral of SRLatch is
begin
	process (s, r)
	begin
		if (s = '1' and r = '0') then
			q <= '1';
		elsif (s = '0' and r = '1') then
			q <= '0';
		elsif (s = '1' and r = '1') then
			q <= 'X';
		end if;
	end process;
end Behavioral;