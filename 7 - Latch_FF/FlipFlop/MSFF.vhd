-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  15/11/2014 
-- Module Name:  MSFF.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Diverse implementazioni di Flip-Flop di tipo Master-Slave SR
--
-- Dependencies: none
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity MSFF is
    port ( clk: in std_logic;
           s,r : in std_logic;
           q : out std_logic
    );
end MSFF;

architecture Structural of MSFF is
    component SRLatch is
	    port ( s,r,en : in std_logic;
	           q : out std_logic
	    );
	end component;

    signal t_q, t_nq, t_nclk : std_logic;
begin
	t_nq <= not t_q;
	t_nclk <= not clk;

    SRM: SRLatch port map (s,r,clk,j,k, t_q);
    SRS: SRLatch port map (t_q,t_nq,t_nclk, q);
end Structural;