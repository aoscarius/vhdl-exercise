-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  15/11/2014 
-- Module Name:  JKFFS.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazioni di Flip-Flop di tipo JK Sincrono (edge triggered)
--
-- Dependencies: none
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity JKFFS is
	port ( clk: in std_logic;
		   j,k : in std_logic;
		   q : out std_logic
	);
end JKFFS;

architecture Behavioral of JKFFS is
begin
	process(clk)
	begin
		if (clk'event and clk='1') then
			if (j='0' and k='0') then
				q <= '0';
			elsif (j='1' and k='1') then
				q <= not q;
			elsif (r='1') then
				q <= '0';
			elsif (s='1') then
				q <= '1';
			end if;
		end if;
	end process;
end Behavioral;