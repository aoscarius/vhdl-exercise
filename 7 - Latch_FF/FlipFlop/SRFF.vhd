-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  15/11/2014 
-- Module Name:  SRFF.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazioni di Flip-Flop di tipo SR
--
-- Dependencies: none
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity SRFF is
	port ( clk: in std_logic;
		   s,r : in std_logic;
		   q : out std_logic
	);
end SRFF;

architecture Behavioral of SRFF is
begin
	process(clk)
	begin
		if (clk'event and clk='1') then
			if (s='1' and r='1') then
				q <= 'X';
			elsif (r='1') then
				q <= '0';
			elsif (s='1') then
				q <= '1';
			end if;
		end if;
	end process;
end Behavioral;