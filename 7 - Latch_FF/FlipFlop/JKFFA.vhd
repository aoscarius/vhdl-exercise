-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  15/11/2014 
-- Module Name:  JKFFA.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazioni del Flip-Flop di tipo JK Asincrono
--
-- Dependencies: none
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity JKFFA is
	port ( j,k : in std_logic;
		   q, qn : out std_logic
	);
end JKFFA;

architecture Behavioral of JKFFA is
begin
	process (j, k)
	begin
		if (j = '0' and k = '0') then
			q <= '0';
			qn <= '1';
		elsif (j = '1' and k = '1') then
			q <= qn;
			qn <= q;
		elsif (j = '1' and k = '0') then
			q <= '1';
			qn <= '0';
		elsif (k = '1' and j = '0') then
			q <= '0';
			qn <= '1';
		end if;
	end process;
end Behavioral;