-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Robertson Multiplier
-- Create Date:  31/12/2014 
-- Module Name:  ControlUnit.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Unit� di controllo per il moltiplicatore Robertson. Questo modulo
--               implementa una macchina a stati finiti che esegue l'algoritmo di 
--               moltiplicazione di Robertson per operandi interi relativi.
--
-- Dependencies: none
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity ControlUnit is
	port(	clk : in  std_logic;
			reset : in  std_logic;
			start : in std_logic;
			q_lsb : in std_logic;			-- q[0]
         count_hit : in std_logic;		-- conteggio terminato
			nadd_sub : out  std_logic;		-- somma(0) sottrazione(1)
			a_reset : out  std_logic;		-- reset registro A
			q_reset : out  std_logic;		-- reset registro Q
			m_reset : out  std_logic;		-- reset registro M
			f_reset : out  std_logic;		-- reset flipflop F
			c_reset : out  std_logic;		-- reset contatore
			a_load : out  std_logic;		-- caricamento registro A
			q_load : out  std_logic;		-- caricamento registro Q
			m_load : out  std_logic;		-- caricamento registro M
			sel_mux_m : out  std_logic;	-- 00..0(0) m(1)
			sel_mux_a : out  std_logic;	-- a[7]=f_out(0) a[7]=a[7](1)
			shift_en : out  std_logic;		-- abilitazione shift A e Q
			count_inc : out  std_logic;	-- incremento conteggio
			stop : out std_logic				-- sistema fermo
	);
end ControlUnit;


architecture behavioral of ControlUnit is

type state_type is (s_idle, s_init, s_sum, s_shift, s_sub, s_fshift, s_end);
signal s_current, s_next: state_type := s_idle;

begin

	next_state: process(clk, reset)
	begin
		if reset='1' then
			s_current <= s_idle;
		elsif (clk'event and clk='1') then
			s_current <= s_next;
		end if;
	end process;


	fsm_main: process (start, s_current, count_hit, q_lsb) 		-- gestisce la fsm
	begin

		-- reset generico ad ogni attivazione del process
		stop <= '0';			
		nadd_sub <= '0';
		a_reset <= '0';
		q_reset <= '0';
		m_reset <= '0';
		f_reset <= '0';
		c_reset <= '0';
		a_load <= '0';
      q_load <= '0';
      m_load <= '0';
		sel_mux_m <= '0';
		sel_mux_a <= '0';
		shift_en <= '0';
		count_inc <= '0';
				
		case s_current is

			-- stato di quiete, il sistema non fa niente	
			when s_idle => 	stop <= '1';				

									if start = '1' then
										s_next <= s_init;
									else 
										s_next <= s_idle;
									end if;
									
			-- stato di inizializzazione, il sistema si prepara a memorizzare gli operandi
			when s_init => 	f_reset <= '1'; -- BEGIN
									a_reset <= '1'; 
									c_reset <= '1';
									
									q_load <= '1';	--INPUT
									m_load <= '1';
									
									s_next <= s_sum;
									
			-- stato di somma					
			when s_sum => 		a_load <='1';
									
									-- M[7:0]*Q[0]
									if q_lsb = '0' then
										sel_mux_m <= '0';
									else  
										sel_mux_m <= '1';
									end if;
									count_inc <= '1';
									
									s_next <= s_shift;
			
			-- stato di shift a destra di a e q
			when s_shift => 	shift_en <='1';
																		
									if count_hit = '1' then
										s_next <= s_sub;
									else 	
										s_next <= s_sum;
									end if;
									
				
			-- stato di sottrazione
			when s_sub => 		nadd_sub <= '1';
									
									a_load <='1';
									
									-- M[7:0]*Q[0]	
									if q_lsb = '0' then
										sel_mux_m <= '0';
									else 
										sel_mux_m <= '1'; 
									end if;
									
									s_next <= s_fshift;
									
									
			-- stato di shift finale a destra di a e q	
			when s_fshift =>	sel_mux_a <= '1';				-- a7 in scan_in di a

									shift_en <= '1';
									
									s_next <= s_end;
									
									
			-- stato finale, il sistema si ferma e restituisce il prodotto finale
			when s_end =>		stop <= '1';
			
									if start ='0' then
										s_next <= s_idle;
									else 
										s_next <= s_end;
									end if;
									
			
			when others => 	s_next <= s_idle;
				
			end case;
	
		
	end process;

end behavioral;