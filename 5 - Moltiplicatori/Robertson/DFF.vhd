-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  15/11/2014 
-- Module Name:  DFF.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description: Flip-Flop di tipo D, per i moduli di memoria con reset asincron. 
--              Il sintetizzatore XST integrato in ISE, riconosce automaticamente 
--              queste strutture come DFF.
--
-- Dependencies: none
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all ;

-- DFF with asynchronous reset
entity DFF is
	port ( clk, reset: in std_logic;
		   d : in std_logic;
		   q : out std_logic
	);
end DFF;

architecture Behavioral of DFF is
begin
	process(clk, reset)
	begin
		if (reset='1') then
			q <= '0';
		elsif (clk'event and clk='1') then
			q <= d;
		end if;
	end process;
end Behavioral;