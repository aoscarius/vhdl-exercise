-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Generic Adder
-- Create Date:  27/11/2014
-- Module Name:  RPC.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione di un Ripple Carry con cella 
--               elementare di tipo Full Adder.
--
-- Dependencies: FA.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity RCA is
	generic ( N: natural := 8 );
    port ( x_in, y_in: in  std_logic_vector ((N-1) downto 0);
		   c_in : in std_logic;
		   nadd_sub: in std_logic;
         sum : out  std_logic_vector ((N-1) downto 0);
		   overflow : out std_logic; 
		   c_out : out std_logic);
end  RCA;

architecture Structural of  RCA is
	component FA
		port ( a, b, cin: in std_logic;
			   sum, cout: out std_logic
		);
	end component;
	
	-- Vettore di segnali di propagazione del carry
	signal c_vect : std_logic_vector(N downto 0);
	
	-- Segnali per la rete di complementazione ----------------------------------
	signal yc_in: STD_LOGIC_VECTOR ((N-1) downto 0) := (others => '0');
	signal cc_in: STD_LOGIC := '0';
	constant xones: STD_LOGIC_VECTOR ((N-1) downto 0) := (others => '1');
	-----------------------------------------------------------------------------
	
begin
	-- Rete di complementazione a 2 di b ------------------
	yc_in <= (y_in xor xones) when (nadd_sub='1') else y_in;
	cc_in <= (not c_in) when (nadd_sub='1') else c_in;
	-------------------------------------------------------

	c_vect(0) <= cc_in;
	
	-- Genero la catena di Full Adder in cascata
	rpc_gen: for i in (N-1) downto 0 generate 
		facell: FA port map(x_in(i), yc_in(i), c_vect(i), sum(i), c_vect(i+1));
	end generate;
	
	-- Calcolo dell'overflow
	overflow <= c_vect(N) xor c_vect(N-1);
	-- E riporto in uscita l'ultimo carry della cascata
	c_out <= c_vect(N);
	
end Structural;