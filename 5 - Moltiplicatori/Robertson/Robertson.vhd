-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Robertson Multiplier
-- Create Date:  31/12/2014 
-- Module Name:  Robertson.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Moltiplicatore di Robertson che essegue la moltiplicazione
--               tra operandi interi relativi ad N bit.
--
-- Dependencies: RSRegister.vhd, DFF.vhd, BUSMux_2_1.vhd, Mux_2_1.vhd, RCA.vhd,
--				     FA.vhd, HitCounter.vhd, ControlUnit.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Robertson is
	Generic (bits : integer := 8;
			   log_bits : integer := 3);
	Port (clk : in  STD_LOGIC;
		   reset : in  STD_LOGIC;
		   start : in STD_LOGIC;
         x : in  STD_LOGIC_VECTOR ((bits-1) downto 0);
         y : in  STD_LOGIC_VECTOR ((bits-1) downto 0);
		   stop : out STD_LOGIC;
         p_out : out STD_LOGIC_VECTOR (((2*bits)-1) downto 0));
end Robertson;

architecture Structural of Robertson is

	component RCA is
		generic ( N: natural := bits );
		port ( x_in, y_in: in  std_logic_vector ((N-1) downto 0);
				 c_in : in std_logic;
				 nadd_sub: in std_logic;
				 sum : out  std_logic_vector ((N-1) downto 0);
				 overflow : out std_logic; 
				 c_out : out std_logic);
	end component;

	component RSRegister is
		generic(N: integer := bits);
		port(	clk, load, reset, rshift: in std_logic;
				d_in: in std_logic;
				data_in: in std_logic_vector((N-1) downto 0);
				data_out: out std_logic_vector((N-1) downto 0)			
		);
	end component;

	component DFF is
		port ( clk, reset: in std_logic;
				 d : in std_logic;
				 q : out std_logic
		);
	end component;

	component Mux_2_1 is
		port ( din_0 : in std_logic;   
			    din_1 : in std_logic; 
			    sel : in std_logic; 
			    dout : out std_logic
		);
	end component;

	component BUSMux_2_1 is
		generic ( N: natural := bits );
		port ( din_0 : in std_logic_vector((N-1) downto 0);   
			    din_1 : in std_logic_vector((N-1) downto 0); 
			    sel : in std_logic; 
			    dout : out std_logic_vector((N-1) downto 0)
		);
	end component;

	component HitCounter is
		generic ( N : natural := (bits-1) );
		port ( clk, reset, count_inc: in std_logic;   
				 count_hit: out std_logic
		); 
	end component;

	component ControlUnit is
		port(	clk : in  std_logic;
				reset : in  std_logic;
				start : in std_logic;
				q_lsb : in std_logic;			-- q[0]
				count_hit : in std_logic;		-- conteggio terminato
				nadd_sub : out  std_logic;		-- somma(0) sottrazione(1)
				a_reset : out  std_logic;		-- reset registro A
				q_reset : out  std_logic;		-- reset registro Q
				m_reset : out  std_logic;		-- reset registro M
				f_reset : out  std_logic;		-- reset flipflop F
				c_reset : out  std_logic;		-- reset contatore
				a_load : out  std_logic;		-- caricamento registro A
				q_load : out  std_logic;		-- caricamento registro Q
				m_load : out  std_logic;		-- caricamento registro M
				sel_mux_m : out  std_logic;		-- 00..0(0) m(1)
				sel_mux_a : out  std_logic;	-- a[7]=f_out(0) a[7]=a[7](1)
				shift_en : out  std_logic;		-- abilitazione shift A e Q
				count_inc : out  std_logic;	-- incremento conteggio
				stop : out std_logic				-- sistema fermo
		);
	end component;

	constant mux_nvect_in : std_logic_vector((bits-1) downto 0) := (others => '0');
	signal mux_m_out : std_logic_vector((bits-1) downto 0) := (others => '0');
	
	signal F_In, F_Out : std_logic;
	signal mux_a_out: std_logic;
	
	signal A_Out, Q_Out, M_Out : std_logic_vector((bits-1) downto 0) := (others => '0');
	signal sum_out : std_logic_vector((bits-1) downto 0) := (others => '0');
	
	signal cu_nadd_sub : std_logic;
	signal cu_a_reset, cu_q_reset, cu_m_reset, cu_f_reset, cu_c_reset : std_logic;
	signal cu_a_load, cu_q_load, cu_m_load: std_logic;
	signal cu_sel_mux_m, cu_sel_mux_a : std_logic;
	signal cu_shift_en : std_logic;	
	signal cu_count_inc, cu_count_hit : std_logic;


begin

	F_In <= (Q_Out(0) and M_Out((bits-1))) or F_Out; 
	p_out <= A_Out & Q_Out;	-- Composizione del prodotto finale


	A: RSRegister -- Registro A
		port map(
			clk => clk,
			load => cu_a_load,
			reset => cu_a_reset,
			rshift => cu_shift_en,
			d_in => mux_a_out,
			data_in => sum_out,
			data_out => A_Out			
		);

	Q: RSRegister -- Registro Q
		port map(
			clk => clk,
			load => cu_q_load,
			reset => cu_q_reset,
			rshift => cu_shift_en,
			d_in => A_Out(0),
			data_in => x,
			data_out => Q_Out			
		);
				
	M: RSRegister -- Registro M
		port map(
			clk => clk,
			load => cu_m_load,
			reset => cu_m_reset,
			rshift => '0',
			d_in => '0',
			data_in => y,
			data_out => M_Out			
		);

	F: DFF -- FlipFlop per lo shift
		port map( 
			clk => clk,
			reset => cu_f_reset,
			d => F_In,
			q => F_Out
		);
		
	MUX_M: BUSMux_2_1 -- Seleziona il moltiplicando 00...0 (0) o M (1)
		port map( 
			din_0 => mux_nvect_in,
			din_1 => M_Out, 
			sel => cu_sel_mux_m,
			dout => mux_m_out
		);

	MUX_A: Mux_2_1 -- Seleziona A[7]=F_In (0) o A[7]=A[7] (1)
		port map( 
			din_0 => F_In,
			din_1 => A_Out((bits-1)), 
			sel => cu_sel_mux_a,
			dout => mux_a_out
		);

	ParallelAdder: RCA -- Sommatore/Sottrattore
		port map( 
			x_in => A_Out,
			y_in => mux_m_out,
			c_in => '0',
			nadd_sub => cu_nadd_sub,
			sum => sum_out,
			overflow => open,
			c_out => open
		);

	Counter: HitCounter	-- Contatore Mod(bits)
		port map( 
			clk => clk,
			reset => cu_c_reset, 
			count_inc => cu_count_inc,
			count_hit => cu_count_hit
		); 

	CUnit: ControlUnit -- Unit� di Controllo
		port map(	
			clk => clk,
			reset => reset,
			start => start,
			q_lsb => Q_Out(0),
			count_hit => cu_count_hit,
			nadd_sub => cu_nadd_sub,
			a_reset => cu_a_reset,
			q_reset => cu_q_reset,
			m_reset => cu_m_reset,
			f_reset => cu_f_reset,
			c_reset => cu_c_reset,
			a_load => cu_a_load,
			q_load => cu_q_load,
			m_load => cu_m_load,
			sel_mux_m => cu_sel_mux_m,
			sel_mux_a => cu_sel_mux_a,
			shift_en => cu_shift_en,
			count_inc => cu_count_inc,
			stop => stop
		);

end Structural;

