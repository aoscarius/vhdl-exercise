-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  15/11/2014 
-- Module Name:  BUSMux_2_1.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Multiplexer 2:1
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Mux_2_1 is
	port (din_0 : in std_logic;   
		  din_1 : in std_logic; 
		  sel : in std_logic; 
		  dout : out std_logic
	);
end Mux_2_1;

architecture Behavioral of Mux_2_1 is
begin
	process(sel, din_0, din_1)
	begin
		case sel is
			when '0' => dout <= din_0;
			when '1' => dout <= din_1;
			when others => dout <= 'X';
		end case; 
	end process;
end Behavioral;