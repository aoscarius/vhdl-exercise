-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar
-- 
-- Project Name: Generic Adder
-- Create Date:  24/11/2014
-- Module Name:  FA.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione di un Full Adder
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FA is
	port ( a, b, cin: in std_logic;
		   sum, cout: out std_logic
	);
end FA;

architecture Dataflow of FA is
begin
	sum <= a xor b xor cin;
	cout <= (a and b) or (cin and a) or (cin and b);
end Dataflow;