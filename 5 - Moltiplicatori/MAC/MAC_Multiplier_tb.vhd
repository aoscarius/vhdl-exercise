-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar


-- 
-- Project Name: Robertson Multiplier
-- Create Date:  31/12/2014 
-- Module Name:  Robertson.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Testbench per il moltiplicatore di Robertson
--
-- Dependencies: Robertson.vhd, SRegister.vhd, DFF.vhd, BUSMux_2_1.vhd,
--				      Mux_2_1.vhd, RCA.vhd, FA.vhd, NCounter.vhd, ControlUnit.vhd
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
 
-- uncomment the following library declaration if using
-- arithmetic functions with signed or unsigned values
--use ieee.numeric_std.all;
 
entity MAC_Multiplier_tb is
end MAC_Multiplier_tb;
 
architecture behavior of MAC_Multiplier_tb is 
 
    -- component declaration for the unit under test (uut)
 
    component MAC_Multiplier
    port(
         x_in : in  std_logic_vector(7 downto 0);
         y_in : in  std_logic_vector(7 downto 0);
         p_out : out  std_logic_vector(15 downto 0)
        );
    end component;
    
   --inputs
   signal x_in : std_logic_vector(7 downto 0) := (others => '0');
   signal y_in : std_logic_vector(7 downto 0) := (others => '0');

 	--outputs
   signal p_out : std_logic_vector(15 downto 0);
 
begin
 
	-- instantiate the unit under test (uut)
   uut: MAC_Multiplier port map (
          x_in => x_in,
          y_in => y_in,
          p_out => p_out
        );

   -- stimulus process
   stim_proc: process
   begin		
	
		-- Il moltiplicatore MAC funziona solo con gli interi positivi
		
		x_in <= "00000011";
		y_in <= "00000011";

      wait for 100 ns;	
		
		x_in <= "10110011";
		y_in <= "11010101";

      wait;
   end process;

end;
