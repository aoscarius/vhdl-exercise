-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar
-- 
-- Project Name: MAC Multiplier
-- Create Date:  28/12/2014
-- Module Name:  MAC.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione di una moltiplicatore a celle MAC
--
-- Dependencies: FA.vhd, MAC.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity MAC_Multiplier is
	generic ( N: natural := 8 );
    port ( x_in, y_in: in  STD_LOGIC_VECTOR((N-1) downto 0);
		   p_out : out  STD_LOGIC_VECTOR(((2*N)-1) downto 0)
	);
end MAC_Multiplier;

architecture Structural of MAC_Multiplier is
	
	type std_logic_matrix is array (N downto 0) of std_logic_vector (N downto 0);
	
	component  MAC is
		port ( s_in, x_in, y_in: in  STD_LOGIC;
			   c_in : in  STD_LOGIC;
			   s_out : out  STD_LOGIC;
			   c_out : out  STD_LOGIC
		);
	end component;
	
	signal s_line : std_logic_matrix := (others=> (others => '0'));
	signal c_line : std_logic_matrix := (others=> (others => '0'));

begin
	
	s_line(0) <= (others => '0');
	
	row: for i in 0 to (N-1) generate
	begin
		
		c_line(i)(0)<='0';
		
		column: for j in 1 to N generate
		begin
			MACCell: MAC port map (
				s_in => s_line(i)(j),
				x_in => x_in(j-1),
				y_in => y_in(i),
				c_in => c_line(i)(j-1),
				s_out => s_line(i+1)(j-1),
				c_out => c_line(i)(j)
			);
		end generate;
		
		s_line(i+1)(N)<= c_line(i)(N);
		p_out(i)<= s_line(i+1)(0);
				
	end generate;
	
	p_out(((2*N)-1) downto N) <= s_line(N)(N downto 1);
	
end Structural;

