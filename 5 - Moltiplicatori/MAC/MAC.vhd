-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar
-- 
-- Project Name: MAC Multiplier
-- Create Date:  28/12/2014
-- Module Name:  MAC.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione di una cella MAC
--
-- Dependencies: FA.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity MAC is
    port ( s_in, x_in, y_in: in  STD_LOGIC;
		   c_in : in  STD_LOGIC;
           s_out : out  STD_LOGIC;
		   c_out : out  STD_LOGIC
	);
end MAC;

architecture Structural of MAC is
	
	component FA is
	port ( a, b, cin: in std_logic;
		   sum, cout: out std_logic
	);
	end component;
	
	signal and_line: STD_LOGIC;

begin

	and_line <= x_in and y_in; 
	FACell: FA port map(s_in, and_line, c_in, s_out, c_out);
	
end Structural;

