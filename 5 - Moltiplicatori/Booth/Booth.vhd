-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Booth Multiplier
-- Create Date:  31/12/2014 
-- Module Name:  Booth.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Moltiplicatore di Booth che essegue la moltiplicazione
--               tra operandi interi relativi ad N bit.
--
-- Dependencies: RSRegister.vhd, DFF.vhd, BUSMux_2_1.vhd, Mux_2_1.vhd, RCA.vhd,
--				 FA.vhd, HitCounter.vhd, ControlUnit.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Booth is
	Generic ( bits : integer := 8 );
	Port (clk : in  STD_LOGIC;
		   reset : in  STD_LOGIC;
		   start : in STD_LOGIC;
         x : in  STD_LOGIC_VECTOR ((bits-1) downto 0);
         y : in  STD_LOGIC_VECTOR ((bits-1) downto 0);
		   stop : out STD_LOGIC;
         p_out : out STD_LOGIC_VECTOR (((2*bits)-1) downto 0));
end Booth;

architecture Structural of Booth is

	component RCA is
		generic ( N: natural := bits );
		port ( x_in, y_in: in  std_logic_vector ((N-1) downto 0);
				 c_in : in std_logic;
				 nadd_sub: in std_logic;
				 sum : out  std_logic_vector ((N-1) downto 0);
				 overflow : out std_logic; 
				 c_out : out std_logic);
	end component;

	component RSRegister is
		generic(N: integer := bits);
		port(	clk, load, reset, rshift: in std_logic;
				d_in: in std_logic;
				data_in: in std_logic_vector((N-1) downto 0);
				data_out: out std_logic_vector((N-1) downto 0)			
		);
	end component;

	component BUSMux_2_1 is
		generic ( N: natural := (bits-1));
		port ( din_0 : in std_logic_vector((N-1) downto 0);   
			    din_1 : in std_logic_vector((N-1) downto 0); 
			    sel : in std_logic; 
			    dout : out std_logic_vector((N-1) downto 0)
		);
	end component;

	component HitCounter is
		generic ( N : natural := bits );
		port ( clk, reset, count_inc : in std_logic;   
				 count_hit: out std_logic
		); 
	end component;

	component ControlUnit is
		port(	clk : in  std_logic;
				reset : in  std_logic;
				start : in std_logic;
				q_lsb : in std_logic_vector(1 downto 0);	-- q[0]q[-1]
				count_hit : in std_logic;						-- conteggio terminato
				nadd_sub : out  std_logic;						-- somma(0) sottrazione(1)
				a_reset : out  std_logic;						-- reset registro A
				q_reset : out  std_logic;						-- reset registro Q
				m_reset : out  std_logic;						-- reset registro M
				c_reset : out  std_logic;						-- reset contatore
				a_load : out  std_logic;						-- caricamento registro A
				q_load : out  std_logic;						-- caricamento registro Q
				m_load : out  std_logic;						-- caricamento registro M
				shift_en : out  std_logic;						-- abilitazione shift A e Q
				count_inc : out  std_logic;					-- incremento conteggio
				stop : out std_logic								-- sistema fermo
		);
	end component;

	signal x_extended, Q_Out : std_logic_vector(bits downto 0) := (others => '0');
	signal A_Out, M_Out : std_logic_vector((bits-1) downto 0) := (others => '0');
	signal sum_out : std_logic_vector((bits-1) downto 0) := (others => '0');
	
	signal cu_nadd_sub : std_logic;
	signal cu_a_reset, cu_q_reset, cu_m_reset, cu_c_reset : std_logic;
	signal cu_a_load, cu_q_load, cu_m_load: std_logic;
	signal cu_shift_en : std_logic;	
	signal cu_count_inc, cu_count_hit : std_logic;


begin

	-- L'ultimo bit[0] rappresenta il bit[-1]
	x_extended(0) <= '0'; 
	x_extended(bits downto 1) <= x; -- Composizione del valore in ingresso a Q 
	p_out <= A_Out & Q_Out(bits downto 1);	-- Composizione del prodotto finale 

	A: RSRegister -- Registro A
		port map(
			clk => clk,
			load => cu_a_load,
			reset => cu_a_reset,
			rshift => cu_shift_en,
			d_in => A_Out(bits-1),
			data_in => sum_out,
			data_out => A_Out			
		);

	Q: RSRegister -- Registro Q
		generic map ( N => (bits+1) )
		port map(
			clk => clk,
			load => cu_q_load,
			reset => cu_q_reset,
			rshift => cu_shift_en,
			d_in => A_Out(0),
			data_in => x_extended,
			data_out => Q_Out			
		);
				
	M: RSRegister -- Registro M
		port map(
			clk => clk,
			load => cu_m_load,
			reset => cu_m_reset,
			rshift => '0',
			d_in => '0',
			data_in => y,
			data_out => M_Out			
		);

	ParallelAdder: RCA -- Sommatore/Sottrattore
		port map( 
			x_in => A_Out,
			y_in => M_Out,
			c_in => '0',
			nadd_sub => cu_nadd_sub,
			sum => sum_out,
			overflow => open,
			c_out => open
		);

	Counter: HitCounter	-- Contatore Mod(bits)
		port map( 
			clk => clk,
			reset => cu_c_reset, 
			count_inc => cu_count_inc,
			count_hit => cu_count_hit
		); 

	CUnit: ControlUnit -- Unit� di Controllo
		port map(	
			clk => clk,
			reset => reset,
			start => start,
			q_lsb => Q_Out(1 downto 0),
			count_hit => cu_count_hit,
			nadd_sub => cu_nadd_sub,
			a_reset => cu_a_reset,
			q_reset => cu_q_reset,
			m_reset => cu_m_reset,
			c_reset => cu_c_reset,
			a_load => cu_a_load,
			q_load => cu_q_load,
			m_load => cu_m_load,
			shift_en => cu_shift_en,
			count_inc => cu_count_inc,
			stop => stop
		);

end Structural;

