-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Robertson Multiplier
-- Create Date:  31/12/2014 
-- Module Name:  Booth_tb.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Testbench per il moltiplicatore di Booth
--
-- Dependencies: Booth.vhd, RSRegister.vhd, DFF.vhd, BUSMux_2_1.vhd,
--				      Mux_2_1.vhd, RCA.vhd, FA.vhd, HitCounter.vhd, ControlUnit.vhd
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
 
entity Booth_tb is
end Booth_tb;
 
architecture behavior of Booth_tb is 
 
    -- component declaration for the unit under test (uut)
 
    component Booth
    port(
         clk : in  std_logic;
         reset : in  std_logic;
         start : in  std_logic;
         x : in  std_logic_vector(7 downto 0);
         y : in  std_logic_vector(7 downto 0);
         stop : out  std_logic;
         p_out : out  std_logic_vector(15 downto 0)
        );
    end component;
    

   --inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal start : std_logic := '0';
   signal x : std_logic_vector(7 downto 0) := (others => '0');
   signal y : std_logic_vector(7 downto 0) := (others => '0');

 	--outputs
   signal stop : std_logic;
   signal p_out : std_logic_vector(15 downto 0);

   -- clock period definitions
   constant clk_period : time := 20 ns;
 
begin
 
	-- instantiate the unit under test (uut)
   uut: Booth port map (
          clk => clk,
          reset => reset,
          start => start,
          x => x,
          y => y,
          stop => stop,
          p_out => p_out
        );

   -- clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		reset <= '1';
		
      wait for 10 ns;	
		
		reset <= '0';
		
		x <= "10110011";
		y <= "11010101";
		
		start <= '1';
		
      wait;
   end process;

end;
