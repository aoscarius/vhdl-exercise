-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Restoring Divisor
-- Create Date:  31/12/2014 
-- Module Name:  ControlUnit.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Unit� di controllo per il divisore con Restoring. Questo modulo
--               implementa una macchina a stati finiti che esegue l'algoritmo di 
--               divisione con Restoring per operandi interi senza segno.
--
-- Dependencies: none
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity ControlUnit is
	port(	clk : in  std_logic;
			reset : in  std_logic;
			start : in std_logic;
			sign : in std_logic;				-- sign of sum
			count_hit : in std_logic;		-- conteggio terminato
			nadd_sub : out  std_logic;		-- somma(0) sottrazione(1)
			a_reset : out  std_logic;		-- reset registro A
			q_reset : out  std_logic;		-- reset registro Q
			m_reset : out  std_logic;		-- reset registro M
			c_reset : out  std_logic;		-- reset contatore
			a_load : out  std_logic;		-- caricamento registro A
			q_load : out  std_logic;		-- caricamento registro Q
			m_load : out  std_logic;		-- caricamento registro M
			sel_mux : out  std_logic;		-- registri interni (0) precaricamento (1)
			shift_en : out  std_logic;		-- abilitazione shift A e Q
			count_inc : out  std_logic;	-- incremento conteggio
			stop : out std_logic				-- sistema fermo
	);
end ControlUnit;


architecture behavioral of ControlUnit is

type state_type is (s_idle, s_init, s_shift, s_sub, s_restoring, s_testcount, s_end);
signal s_current, s_next: state_type := s_idle;

begin

	next_state: process(clk, reset)
	begin
		if reset='1' then
			s_current <= s_idle;
		elsif (clk'event and clk='1') then
			s_current <= s_next;
		end if;
	end process;


	fsm_main: process (start, s_current, count_hit, sign)  -- gestisce la fsm
	begin

		-- reset generico ad ogni attivazione del process
		stop <= '0';			
		nadd_sub <= '1'; -- Data la prevalenza di ssottrazioni, mantengo attiva la funzione
		a_reset <= '0';
		q_reset <= '0';
		m_reset <= '0';
		c_reset <= '0';
		a_load <= '0';
		q_load <= '0';
		m_load <= '0';
		sel_mux <= '0';
		shift_en <= '0';
		count_inc <= '0';
				
		case s_current is

			-- stato di quiete, il sistema non fa niente	
			when s_idle => 	stop <= '1';				

									if start = '1' then
										s_next <= s_init;
									else 
										s_next <= s_idle;
									end if;
									
			-- stato di inizializzazione, il sistema si prepara a memorizzare gli operandi
			when s_init => 	sel_mux <= '1';
			
									a_load <= '1'; 
									q_load <= '1';
									m_load <= '1';
									
									c_reset <= '1';
									
									s_next <= s_shift;
									
			-- stato di shift a sinistra di a e q
			when s_shift => 	shift_en <='1';
									
									s_next <= s_sub;

			-- stato di sottrazione
			when s_sub => 		a_load <='1';
									q_load <='1';
									
									if sign = '0' then
										s_next <= s_testcount;
									else 
										s_next <= s_restoring; 
									end if;
									
			-- stato di restoring di A con M	
			when s_restoring =>	nadd_sub <= '0';
									a_load <= '1';
									
									s_next <= s_testcount;
									
									
			-- stato di controllo per il termine dell'algoritmo					
			when s_testcount =>									
									if count_hit = '1' then
										s_next <= s_end;
									else  
										count_inc <= '1';
										s_next <= s_shift;
									end if;

			-- stato finale, il sistema si ferma e restituisce il risultato finale
			when s_end =>		stop <= '1';
			
									if start ='0' then
										s_next <= s_idle;
									else 
										s_next <= s_end;
									end if;
									
			when others => 	s_next <= s_idle;
				
			end case;
		
	end process;

end behavioral;