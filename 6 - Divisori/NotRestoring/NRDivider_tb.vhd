-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Non Robertson Multiplier
-- Create Date:  31/12/2014 
-- Module Name:  NRDivider_tb.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Testbench del divisore ad N bits che implementa l'algoritmo 
--					  di non restoring
--
-- Dependencies: RDiveder.vhd, LSRegister.vhd, BUSMux_2_1.vhd, RCA.vhd,
--				     FA.vhd, HitCounter.vhd, ControlUnit.vhd
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY NRDivider_tb IS
END NRDivider_tb;
 
ARCHITECTURE behavior OF NRDivider_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT NRDivider
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         start : IN  std_logic;
         divider : IN  std_logic_vector(6 downto 0);
         divisor : IN  std_logic_vector(3 downto 0);
         stop : OUT  std_logic;
         reminder : OUT  std_logic_vector(3 downto 0);
         quotient : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal start : std_logic := '0';
   signal divider : std_logic_vector(6 downto 0) := (others => '0');
   signal divisor : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal stop : std_logic;
   signal reminder : std_logic_vector(3 downto 0);
   signal quotient : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: NRDivider PORT MAP (
          clk => clk,
          reset => reset,
          start => start,
          divider => divider,
          divisor => divisor,
          stop => stop,
          reminder => reminder,
          quotient => quotient
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		reset <= '1';
		
      wait for 100 ns;	
		
		reset <= '0';
		
		divider <= "1111011";
		divisor <= "1010";
		
		start <= '1';
		
      wait;
   end process;

END;
