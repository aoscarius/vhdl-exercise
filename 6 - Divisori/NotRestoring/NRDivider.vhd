-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Non Restoring Divisor
-- Create Date:  31/12/2014 
-- Module Name:  NRDivder.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Divisore ad N bits che implementa l'algoritmo di non restoring
--
-- Dependencies: LSRegister.vhd, DFF.vhd, BUSMux_2_1.vhd, RCA.vhd,
--				     FA.vhd, HitCounter.vhd, ControlUnit.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity NRDivider is
	Generic (bits : integer := 4;
			   log_bits : integer := 2);
	Port (clk : in  STD_LOGIC;
		   reset : in  STD_LOGIC;
		   start : in STD_LOGIC;
         divider : in  STD_LOGIC_VECTOR (((2*bits)-1-1) downto 0);
         divisor : in  STD_LOGIC_VECTOR ((bits-1) downto 0);
		   stop : out STD_LOGIC;
		   reminder : out STD_LOGIC_VECTOR ((bits-1) downto 0); 
         quotient : out STD_LOGIC_VECTOR ((bits-1) downto 0));
end NRDivider;

architecture Structural of NRDivider is

	component DFF_enable is
		port ( clk, reset: in std_logic;
				en : in std_logic;
				d : in std_logic;
				q : out std_logic
		);
	end component;

	component BUSMux_2_1 is
		generic ( N: natural := bits );
		port (din_0 : in std_logic_vector((N-1) downto 0);   
			  din_1 : in std_logic_vector((N-1) downto 0); 
			  sel : in std_logic; 
			  dout : out std_logic_vector((N-1) downto 0)
		);
	end component;
	
	component RCA is
		generic ( N: natural := bits );
		port ( x_in, y_in: in  std_logic_vector ((N-1) downto 0);
				 c_in : in std_logic;
				 nadd_sub: in std_logic;
				 sum : out  std_logic_vector ((N-1) downto 0);
				 overflow : out std_logic; 
				 c_out : out std_logic);
	end component;

	component LSRegister is
		generic(N: integer := bits);
		port(	clk, load, reset, lshift: in std_logic;
				d_in: in std_logic;
				data_in: in std_logic_vector((N-1) downto 0);
				data_out: out std_logic_vector((N-1) downto 0)			
		);
	end component;

	component HitCounter is
		generic (N : natural := (bits-1));
		port ( clk, reset, count_inc : in std_logic;
				 count_hit: out std_logic  
		); 
	end component;
	
	component ControlUnit is
		port(	clk : in  std_logic;
				reset : in  std_logic;
				start : in std_logic;
				sign : in std_logic;				-- sign of sum
				count_hit : in std_logic;		-- conteggio terminato
				nadd_sub : out  std_logic;		-- somma(0) sottrazione(1)
				s_reset : out  std_logic;		-- reset flip flop S
				a_reset : out  std_logic;		-- reset registro A
				q_reset : out  std_logic;		-- reset registro Q
				m_reset : out  std_logic;		-- reset registro M
				c_reset : out  std_logic;		-- reset contatore
				s_load : out  std_logic;		-- caricamento flip flop S
				a_load : out  std_logic;		-- caricamento registro A
				q_load : out  std_logic;		-- caricamento registro Q
				m_load : out  std_logic;		-- caricamento registro M
				sel_mux : out  std_logic;		-- registri interni (0) precaricamento (1)
				shift_en : out  std_logic;		-- abilitazione shift A e Q
				count_inc : out  std_logic;	-- incremento conteggio
				stop : out std_logic				-- sistema fermo
		);
	end component;

	signal mux_a_out, mux_a_in_1: std_logic_vector(bits downto 0) := (others => '0');
	signal mux_q_out, mux_q_in_0: std_logic_vector((bits-1) downto 0) := (others => '0');
	
	signal A_Out, sum_out, sum_in_y : std_logic_vector(bits downto 0) := (others => '0');
	signal M_Out, Q_Out : std_logic_vector((bits-1) downto 0) := (others => '0');
	
	signal S_Out : std_logic := '0';
	
	signal cu_s_load, cu_s_reset: std_logic;
	signal cu_nadd_sub : std_logic;
	signal cu_a_reset, cu_q_reset, cu_m_reset, cu_c_reset : std_logic;
	signal cu_a_load, cu_q_load, cu_m_load: std_logic;
	signal cu_sel_mux : std_logic;
	signal cu_shift_en : std_logic;	
	signal cu_count_inc, cu_count_hit : std_logic;

begin

	reminder <= A_Out((bits-1) downto 0);
	quotient <= Q_Out;
	
	-- La coppia di registra A.Q presenta due bit in piu rispetto al dividendo, posti inizialmente a 0
	mux_a_in_1 <= "00" & divider(((2*bits)-1-1) downto bits);
	-- Registro di supporto per il caricamento del quoziente
	mux_q_in_0 <= Q_Out((bits-1) downto 1) & (not sum_out(bits));
	-- Il sommatore ha un bit in piu come A, quindi si aggiunge uno 0 in testa al registro M
	sum_in_y <= '0' & M_Out;
	
	MUX_A: BUSMux_2_1 -- Seleziona l'uscita del sommatore (0) o l'MSB del dividendo (1)
		generic map (N => (bits+1))
		port map( 
			din_0 => sum_out,    -- A carica sum_out,
			din_1 => mux_a_in_1, -- A carica l'MSB di divider,
			sel => cu_sel_mux,
			dout => mux_a_out
		);
	
	MUX_Q: BUSMux_2_1 -- Seleziona il registro di precarica del quiziente (0) o l'LSB del dividendo (1)
		generic map (N => bits)
		port map( 
			din_0 => mux_q_in_0, 					 -- Q carica Q(n-1 downto 1) & Q0,
			din_1 => divider((bits-1) downto 0), -- Q carica l'LSB di D,
			sel => cu_sel_mux,
			dout => mux_q_out
		);

	S: DFF_enable
		port map( 
			clk => clk,
			reset => cu_s_reset,
			en => cu_s_load,
			d => sum_out(bits),
			q => S_out
		);
	
	A: LSRegister -- Registro A
		generic map (N => (bits+1))
		port map(
			clk => clk,
			load => cu_a_load,
			reset => cu_a_reset,
			lshift => cu_shift_en,
			d_in => Q_Out((bits-1)),
			data_in => mux_a_out,
			data_out => A_Out			
		);

	Q: LSRegister -- Registro Q
		port map(
			clk => clk,
			load => cu_q_load,
			reset => cu_q_reset,
			lshift => cu_shift_en,
			d_in => '-',
			data_in =>  mux_q_out,
			data_out => Q_Out			
		);
				
	M: LSRegister -- Registro M
		port map(
			clk => clk,
			load => cu_m_load,
			reset => cu_m_reset,
			lshift => '0',
			d_in => '0',
			data_in => divisor,
			data_out => M_Out			
		);

	ParallelAdder: RCA -- Sommatore/Sottrattore
		generic map (N => (bits+1))
		port map( 
			x_in => A_Out,
			y_in => sum_in_y,
			c_in => '0',
			nadd_sub => cu_nadd_sub,
			sum => sum_out,
			overflow => open,
			c_out => open
		);

	Counter: HitCounter	-- Contatore Mod(bits)
		port map( 
			clk => clk,
			reset => cu_c_reset, 
			count_inc => cu_count_inc,
			count_hit => cu_count_hit
		); 

	CUnit: ControlUnit -- Unit� di Controllo
		port map(	
			clk => clk,
			reset => reset,
			start => start,
			sign => S_Out,
			count_hit => cu_count_hit,
			nadd_sub => cu_nadd_sub,
			s_reset => cu_s_reset,
			a_reset => cu_a_reset,
			q_reset => cu_q_reset,
			m_reset => cu_m_reset,
			c_reset => cu_c_reset,
			s_load => cu_s_load,
			a_load => cu_a_load,
			q_load => cu_q_load,
			m_load => cu_m_load,
			sel_mux => cu_sel_mux,
			shift_en => cu_shift_en,
			count_inc => cu_count_inc,
			stop => stop
		);

end Structural;

