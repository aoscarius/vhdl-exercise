-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  26/12/2014 
-- Module Name:  LSRegister.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Registro ad N bit con funzione di shift a sinistra
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity LSRegister is
	generic(N: integer := 8);
	port(	clk, load, reset, lshift: in std_logic;
			d_in: in std_logic;
			data_in: in std_logic_vector((N-1) downto 0);
			data_out: out std_logic_vector((N-1) downto 0)			
	);
end LSRegister;

architecture Behavioral of LSRegister is
	signal data: std_logic_vector((N-1) downto 0) := (others => '0');
begin
	process(clk, reset)
	begin
		if (reset = '1') then
			data <= (others => '0');
		elsif (clk'event and clk='1') then
			if (load = '1') then
				data <= data_in;
			elsif (lshift = '1') then
			   data <= data((N-2) downto 0) & d_in;
			end if;
		end if; 
	end process;

	data_out <= data;
	
end Behavioral;

