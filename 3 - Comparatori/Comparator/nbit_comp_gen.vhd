-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Generic Nbit Comparator ver 0.2
-- Create Date:  06/11/2014 
-- Module Name:  nbit_comp_gen.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Il modulo implementa un generico comparatore ad Nbit esteso, con capacit� 
--               di connessione in cascata.
--
-- Dependencies: bit_comp_ext.vhd 
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Dichiarazione dell'intefaccia del modulo
entity nbit_comp_gen is
	 generic ( N: natural := 4);
    Port ( a : in  STD_LOGIC_VECTOR ((N-1) downto 0);
           b : in  STD_LOGIC_VECTOR ((N-1) downto 0);
			  inLT: in STD_LOGIC;
			  inEQ: in STD_LOGIC;
			  inGT: in STD_LOGIC;
           outLT : out  STD_LOGIC;
           outEQ : out  STD_LOGIC;
           outGT : out  STD_LOGIC);
end nbit_comp_gen;

-- Dichiarazione dell'architettura del modulo
-- Si � scelto di implementare il modulo con un architettura di tipo Structural, 
-- con l'ausilio del costrutto for ... generate. 
architecture struct of nbit_comp_gen is
-- Viene dichiarata l'interfaccia dei moduli utilizzati
	component bit_comp_ext is
	Port ( a : in  STD_LOGIC;
           b : in  STD_LOGIC;
           inLT : in  STD_LOGIC;
           inEQ : in  STD_LOGIC;
			  inGT: in STD_LOGIC;
           outLT : out  STD_LOGIC;
           outEQ : out  STD_LOGIC;
           outGT : out  STD_LOGIC);
	end component;

	signal lt_vect: STD_LOGIC_VECTOR ((N-1) downto 0);
	signal eq_vect: STD_LOGIC_VECTOR ((N-1) downto 0);
	signal gt_vect: STD_LOGIC_VECTOR ((N-1) downto 0);
 
-- Viene definita la configurazione dell'istanza bit_comp_ext, cos� da selezionare l'architettura da implementare
	for all: bit_comp_ext use entity work.bit_comp_ext(dtflow_v3); 
	
begin
-- L'ordine di generazione � discendente in quanto vengono collegati prima i bit piu significativi.
	BIT_GEN: for I in (N-1) downto 0 generate
-- La prima istanza generata � l'unica ad essere diversa dalle altre per i suoi collegamenti, in quanto � l'unica i cui valori
-- di ingresso alla cascata sono collegati agli ingressi della cascata.
		F: if I=(N-1) generate
			bcomp:bit_comp_ext port map(a(I), b(I), inLT, inEQ, inGT, lt_vect(I), eq_vect(I), gt_vect(I));  
		end generate F;
		O: if I<(N-1) generate	
			bcomp:bit_comp_ext port map(a(I), b(I), lt_vect(I+1), eq_vect(I+1), gt_vect(I+1), lt_vect(I), eq_vect(I), gt_vect(I));  
		end generate O;
	end generate BIT_GEN;
	
-- Alla fine l'uscita dell'ultimo bit comparator (quello meno significativo) 
-- � anche l'uscita del nostro modulo.
	outLT <= lt_vect(0);
	outEQ <= eq_vect(0);
	outGT <= gt_vect(0);
end struct;