-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Generic Nbit Comparator ver 0.2
-- Create Date:  06/11/2014 
-- Module Name:  bit_comp_ext.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Il modulo implementa un comparatore ad 1bit esteso, con capacit� 
--               di connessione in cascata. Esso rappresenta la cella logica nucleo 
--               di un generico comparatore ad Nbit.
--
-- Dependencies: none 
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Dichiarazione dell'intefaccia del modulo
-- Gli ingressi inLT, inEQ, inGT, rappresentano gli ingressi per la cascata
entity bit_comp_ext is
    Port ( a : in  STD_LOGIC;
           b : in  STD_LOGIC;
           inLT : in  STD_LOGIC;
           inEQ : in  STD_LOGIC;
			  inGT : in STD_LOGIC;
           outLT : out  STD_LOGIC;
           outEQ : out  STD_LOGIC;
			  outGT : out STD_LOGIC
	     );
end bit_comp_ext;

-- Dichiarazione dell'architettura del modulo
-- Di seguito tre versioni possibili di tipo dataflow, versione compatta con l'uso della macro xor
architecture dtflow_v1 of bit_comp_ext is
begin
	outLT <= (((not a) and b) and inEQ) or inLT;
	outEQ <= (not (a xor b)) and inEQ;
	outGT <= ((a and (not b)) and inEQ) or inGT;
end dtflow_v1;

-- versione senza l'uso della macro xor
architecture dtflow_v2 of bit_comp_ext is
begin
	outLT <= (((not a) and b) and inEQ) or inLT;
	outEQ <= (not (((not a) and b) or (a and (not b)))) and inEQ;
	outGT <= ((a and (not b)) and inEQ) or inGT;
end dtflow_v2;

-- e versione con signal per forzare una sintesi RTL estremamente compatta
architecture dtflow_v3 of bit_comp_ext is
	signal tmpLT, tmpGT : STD_LOGIC;
begin
	tmpLT <= (((not a) and b) and inEQ) or inLT;
	tmpGT <= ((a and (not b)) and inEQ) or inGT;
	outEQ <= (not tmpLT) and (not tmpGT) and inEQ;
	outLT <= tmpLT;
	outGT <= tmpGT;
end dtflow_v3;

-- Di seguito due versioni possibili di tipo behaviour con l'uso dei process e del costrutto
-- sequenziale if..then..else
architecture bhv_v1 of bit_comp_ext is
begin
    comp: process(a, b, inLT, inEQ, inGT)
    begin
        if (((a<b) and (inEQ='1')) or (inLT='1')) then
            outLT <= '1';
            outEQ <= '0';
            outGT <= '0';
        elsif ((a=b) and (inEQ='1')) then
            outEQ <= '1';
            outLT <= '0';
            outGT <= '0';
        else
            outGT <= '1';
            outLT <= '0';
            outEQ <= '0';
        end if;
    end process;
end bhv_v1;

-- e una versione piu snella, facente uso del costrutto di selezione condizionale when..else
architecture bhv_v2 of bit_comp_ext is
	signal output: STD_LOGIC_VECTOR (2 downto 0);
begin
	output <= "100" when (((a<b) and (inEQ='1')) or (inLT='1')) else
	          "010" when ((a=b) and (inEQ='1')) else
				 "001" when (((a>b) and (inEQ='1')) or (inGT='1')) else
				 "XXX";
	outLT <= output(2);
	outEQ <= output(1);
	outGT <= output(0);
end bhv_v2;