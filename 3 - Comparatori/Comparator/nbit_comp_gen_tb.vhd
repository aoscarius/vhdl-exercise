-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Generic Nbit Comparator ver 0.1
-- Create Date:  06/11/2014 
-- Module Name:  nbit_comp_gen_tb.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Semplice file di testbench per la simulazione del comparatore Nbit.
--
-- Dependencies: bit_comp_ext.vhd, nbit_comp_gen.vhd 
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
 
ENTITY nbit_comp_gen_tb IS
END nbit_comp_gen_tb;
 
ARCHITECTURE behavior OF nbit_comp_gen_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT nbit_comp_gen
	 GENERIC (N : natural := 32);
    PORT(
         a : IN  std_logic_vector((N-1) downto 0);
         b : IN  std_logic_vector((N-1) downto 0);
		   inLT: in STD_LOGIC;
		   inEQ: in STD_LOGIC;
		   inGT: in STD_LOGIC;
         outLT : OUT  std_logic;
         outEQ : OUT  std_logic;
         outGT : OUT  std_logic
        );
    END COMPONENT;
    
   --Inputs
   signal a : std_logic_vector(31 downto 0) := (others => '0');
   signal b : std_logic_vector(31 downto 0) := (others => '0');

 	--Outputs
   signal outLT : std_logic;
   signal outEQ : std_logic;
   signal outGT : std_logic;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: nbit_comp_gen 
		GENERIC MAP (N => 32)
		PORT MAP (
          a => a,
          b => b,
			 inLT => '0',
			 inEQ => '1',
			 inGT => '0',
          outLT => outLT,
          outEQ => outEQ,
          outGT => outGT
        );

   -- Clock process definitions
	stim_proc: process
   begin		
-- Viene utilizzata la funzione di conversione std_logic_vector,
-- per permettere l'ingresso di valori decimali negli ingressi a e b.
-- L'uso della funzione to_unsigned, della libreria numeric_std, 
-- permette di forzare la rappresentazione del numero ad un intero senza segno.
		a <= std_logic_vector(to_unsigned(0,a'length));
		b <= std_logic_vector(to_unsigned(0,b'length));
      wait for 100 ns;	
		a <= std_logic_vector(to_unsigned(15,a'length));
		b <= std_logic_vector(to_unsigned(45,b'length));
      wait for 100 ns;	
		a <= std_logic_vector(to_unsigned(47,a'length));
		b <= std_logic_vector(to_unsigned(13,b'length));
      wait for 100 ns;	
		a <= std_logic_vector(to_unsigned(5578,a'length));
		b <= std_logic_vector(to_unsigned(5578,b'length));
	  wait for 100 ns;
   end process;
END;
