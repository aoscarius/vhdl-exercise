-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar
--			  Ricciardi Roberta Maria
--
-- Project Name: Simulation Library
-- Create Date:  17:32:21 08/11/2014 
-- Module Name:  simulation_utility.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Package di utility contenente funzioni non sintetizzabili, utili 
--               per la simulazione dei componenti.
--
-- Dependencies: none
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all; -- function UNIFORM, TRUNC
use ieee.numeric_std.all; -- function TO_UNSIGNED

package simulation_utility is
	-- Shared Variable
	-- Queste variabili sono condivise ed usate come seme per la generazione dei 
	-- numeri casuali. Conservano il valore di seme di ogni generazione.
	shared variable seed1, seed2: positive;
	
	-- Functions
	-- Questa funzione restituisce un vettore STD_LOGIC casuale di dimensione N
	impure function RandStdLogicVector (N: natural) return STD_LOGIC_VECTOR;
	
end simulation_utility;

package body simulation_utility is

	impure function RandStdLogicVector (N: natural) return STD_LOGIC_VECTOR is
		variable rand: real;
		variable intrand: integer;
	begin
		-- Genero in rand un numero casuale compreso tra 0.0 e 1.0
		-- seed1 e seed2, se non esplicitamente inizializzati, vengono inizializzati
		-- a positive'low cio� 1
		UNIFORM(seed1, seed2, rand);
		-- Estendo il numero tra 0 e 2^N
		intrand := integer(TRUNC(rand*real(2**N)));
		-- Converto il numero ad un vettore di STD_LOGIC
		return std_logic_vector(to_unsigned(intrand, N));
	end function;
end simulation_utility;
