-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar
--			  Ricciardi Roberta Maria
--
-- Project Name: TI7485 4bit Comparator
-- Create Date:  09:45:10 06/11/2014 
-- Module Name:  bit_comp_ext.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Il modulo implementa un comparatore ad 1bit esteso, con capacit� 
--               di connessione in cascata. Esso rappresenta la cella logica nucleo 
--               di un generico comparatore ad Nbit.
--               Il comparatore lavora in logica attiva alta. In particolare viene 
--               implementata la legge secondo cui:
--
--               * outEQ � attivo solo se i due bit correnti e i bit precedenti sono uguali;
--               * outLT(outGT) � attivo solo se i due bit correnti sono LT(GT) e i precendenti 
--                 sono EQ, oppure se i precedenti sono LT(GT);
--
-- Dependencies: none 
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity bit_comp_ext is
    Port ( a :     in STD_LOGIC;
           b :     in STD_LOGIC;
           inLT :  in STD_LOGIC;
           inEQ :  in STD_LOGIC;
		   inGT :  in STD_LOGIC;
           outLT : out STD_LOGIC;
           outEQ : out STD_LOGIC;
		   outGT : out STD_LOGIC
	      );
end bit_comp_ext;

architecture dataflow of bit_comp_ext is
	signal tmpLT, tmpGT : STD_LOGIC;
begin
	tmpLT <= (((not a) and b) and inEQ) or inLT;
	tmpGT <= ((a and (not b)) and inEQ) or inGT;
	outEQ <= (not tmpLT) and (not tmpGT) and inEQ;
	outLT <= tmpLT;
	outGT <= tmpGT;
end dataflow;

