-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar
--			  Ricciardi Roberta Maria
--
-- Project Name: TI7485 4bit Comparator
-- Create Date:  09:55:12 06/11/2014 
-- Module Name:  nbit_comp_7485_tb.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Semplice file di testbench per la simulazione del comparatore Nbit
--               basato sull'IC 7485. Il testbench � parametrizzato per effettuare 
--               test variabili e fa uso di generatori di numeri casuali.
--
-- Dependencies: bit_comp_ext.vhd, TI7485.vhd, nbit_comp_7485.vhd
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use work.simulation_utility.all;
 
ENTITY nbit_comp_7485_tb IS
END nbit_comp_7485_tb;
 
ARCHITECTURE behavior OF nbit_comp_7485_tb IS 
	-- Parametri variabili per il testbench, definirli prima di simulare
	constant NumberOfStimulus : natural := 8;
	constant NumberOfBits: natural := 8;

	COMPONENT nbit_comp_7485
		GENERIC ( N: natural := NumberOfBits);
		PORT(
			a : IN  std_logic_vector((NumberOfBits-1) downto 0);
			b : IN  std_logic_vector((NumberOfBits-1) downto 0);
			outLT : OUT  std_logic;
			outEQ : OUT  std_logic;
			outGT : OUT  std_logic
		);
	END COMPONENT;
    
   --Inputs
   signal a, b : std_logic_vector((NumberOfBits-1) downto 0) := (others => '0');

 	--Outputs
   signal outLT, outEQ, outGT : std_logic;

BEGIN
   -- Instantiate the Unit Under Test (UUT)
   uut: nbit_comp_7485 
		  PORT MAP (
          a => a,
          b => b,
          outLT => outLT,
          outEQ => outEQ,
          outGT => outGT
        );

   -- Stimulus process
	stim_proc: process
	begin	
		-- Genero NumberOfStimulus, numeri casulai in ingresso al 
		-- comparatore per verificarne il funzionamento
		for i in 0 to NumberOfStimulus loop
			a <= RandStdLogicVector(a'length);
			b <= RandStdLogicVector(b'length);
			wait for 100 ns;	
		end loop;	
	end process;
	
END;
