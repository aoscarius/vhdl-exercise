-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar
--			  Ricciardi Roberta Maria
--
-- Project Name: TI7485 4bit Comparator
-- Create Date:  09:52:53 06/11/2014 
-- Module Name:  nbit_comp_7485.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Il modulo implementa un comparatore generico ad Nbit. 
--               Il nucleo alla base del comparatore � il modulo che implementa l'IC 7485. 
--               Per poter essere utilizzato correttamente, il comparatore necessita che N 
--               (numero dei bit) sia un multiplo di 4 (numero dei bit di base di cui � 
--               composto l'IC 7485). Se tale valore non � rispettato, il comparatore generato
--	              avr� un numero di bit pari al primo multiplo di 4 piu piccolo del numero 
--               di bit scelto.
--
-- Dependencies: bit_comp_ext.vhd, TI7485.vhd 
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity nbit_comp_7485 is
	generic ( N: natural := 8);
   Port ( a : in  STD_LOGIC_VECTOR ((N-1) downto 0);
          b : in  STD_LOGIC_VECTOR ((N-1) downto 0);
          outLT : out  STD_LOGIC;
          outEQ : out  STD_LOGIC;
          outGT : out  STD_LOGIC
			);
end nbit_comp_7485;

architecture struct of nbit_comp_7485 is
	component TI_7485 is
    Port ( a : in  STD_LOGIC_VECTOR (3 downto 0);
           b : in  STD_LOGIC_VECTOR (3 downto 0);
			  inLT: in STD_LOGIC;
			  inEQ: in STD_LOGIC;
			  inGT: in STD_LOGIC;
           outLT : out  STD_LOGIC;
           outEQ : out  STD_LOGIC;
           outGT : out  STD_LOGIC);
	end component;

	-- Questa costante � di supporto nel calcolo del numero di comparatori
	-- 7485 da utilizzare per generare il comparatore Nbit.
	constant N_TI7485: natural := natural(N/4);

	-- Signal per il collegamento delle istanze del nucleo in cascata
	signal lt_vect, eq_vect, gt_vect: STD_LOGIC_VECTOR (N_TI7485 downto 0);

begin
	-- Se N non � un multiplo di 4 allora viene generato un errore in fase di sintesi che avverte dell'errata configurazione dei bit (bloccante)
	assert ((N mod 4)=0) report "Attention N=" & integer'image(N) & " is not a multiple of 4. Correct the bit depth before the synthesize process." severity error;
	
	lt_vect(0) <= '0'; --
	eq_vect(0) <= '1'; -- Questa condizione iniziale (0,1,0) � necessaria per il corretto funzionamento
	gt_vect(0) <= '0'; --

	BIT_GEN: for I in 0 to (N_TI7485-1) generate
		TI7485: TI_7485 port map(a((N-((I*4)+1)) downto (N-((I+1)*4))), b((N-((I*4)+1)) downto (N-((I+1)*4))), lt_vect(I), eq_vect(I), gt_vect(I), lt_vect(I+1), eq_vect(I+1), gt_vect(I+1));  
	end generate BIT_GEN;
	
	-- L'uscita dell'ultimo 7485 � anche l'uscita del modulo
	outLT <= lt_vect(N_TI7485);
	outEQ <= eq_vect(N_TI7485);
	outGT <= gt_vect(N_TI7485);
	
end struct;

