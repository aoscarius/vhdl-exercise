-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar
--			  Ricciardi Roberta Maria
--
-- Project Name: TI7485 4bit Comparator
-- Create Date:  09:48:27 06/11/2014 
-- Module Name:  TI_7485.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Il modulo implementa l'IC 7485 della Texas Instrument, un comparatore 
--               a 4bit esteso, con capacit� di connessione in cascata.
--
-- Dependencies: bit_comp_ext.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TI_7485 is
    Port ( a : in  STD_LOGIC_VECTOR (3 downto 0);
           b : in  STD_LOGIC_VECTOR (3 downto 0);
			  inLT: in STD_LOGIC;
			  inEQ: in STD_LOGIC;
			  inGT: in STD_LOGIC;
           outLT : out  STD_LOGIC;
           outEQ : out  STD_LOGIC;
           outGT : out  STD_LOGIC);
end TI_7485;

architecture struct of TI_7485 is
	component bit_comp_ext is
	Port ( a : in  STD_LOGIC;
           b : in  STD_LOGIC;
           inLT : in  STD_LOGIC;
           inEQ : in  STD_LOGIC;
			  inGT: in STD_LOGIC;
           outLT : out  STD_LOGIC;
           outEQ : out  STD_LOGIC;
			  outGT : out  STD_LOGIC);
	end component;

	-- Signal per il collegamento delle istanze del nucleo in cascata
	signal lt_vect, eq_vect, gt_vect: STD_LOGIC_VECTOR (4 downto 0);

begin

	lt_vect(4) <= inLT; 
	eq_vect(4) <= inEQ;
	gt_vect(4) <= inGT;

	-- L'ordine di generazione � discendente in quanto vengono collegati prima i bit piu significativi.
	BIT_GEN: for I in 3 downto 0 generate
		bcomp:bit_comp_ext port map(a(I), b(I), lt_vect(I+1), eq_vect(I+1), gt_vect(I+1), lt_vect(I), eq_vect(I), gt_vect(I));  
	end generate;
	
	outLT <= lt_vect(0);
	outEQ <= eq_vect(0);
	outGT <= gt_vect(0);
	
end struct;

