-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Generic Adder
-- Create Date:  27/11/2014
-- Module Name:  RPC.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione di un Ripple Carry con cella 
--               elementare di tipo Full Adder.
--
-- Dependencies: RCAComparator.vhd, RCA.vhd, FA.vhd
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY RCAComparator_tb IS
END RCAComparator_tb;
 
ARCHITECTURE behavior OF RCAComparator_tb IS 
 
   -- Component Declaration for the Unit Under Test (UUT)
   COMPONENT RCAComparator
		generic ( NBit: natural := 8 );
		port ( a, b: in std_logic_vector((NBit-1) downto 0);
				outLT, outEQ, outGT: out std_logic
		);
   END COMPONENT;  

   --Inputs
   signal a : std_logic_vector(7 downto 0) := (others => '0');
   signal b : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal outLT : std_logic;
   signal outEQ : std_logic;
   signal outGT : std_logic;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: RCAComparator PORT MAP (
          a => a,
          b => b,
          outLT => outLT,
          outEQ => outEQ,
          outGT => outGT
        );

   -- Stimulus process
   stim_proc: process
   begin		
		a <= "00000000";
		b <= "11110000";
		wait for 10 ns;
		a <= "10100000";
		b <= "10010000";
		wait for 10 ns;
		a <= "11100100";
		b <= "11100100";
		wait for 10 ns;
		a <= "11000000";
		b <= "00110000";
		wait for 10 ns;
   end process;

END;
