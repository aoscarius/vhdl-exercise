-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Generic Adder
-- Create Date:  27/11/2014
-- Module Name:  RPC.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione di un Ripple Carry con cella 
--               elementare di tipo Full Adder.
--
-- Dependencies: RCA.vhd, FA.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity RCAComparator is
	generic ( NBit: natural := 8 );
	port ( a, b: in std_logic_vector((NBit-1) downto 0);
		   outLT, outEQ, outGT: out std_logic
	);
end RCAComparator;

architecture Structural of RCAComparator is
	component RCA
		generic ( N: natural := NBit );
		port ( a, b: in std_logic_vector((N-1) downto 0);
			   c_in: in std_logic;
			   nadd_sub : in std_logic;
			   sum: out std_logic_vector((N-1) downto 0);
			   overflow: out std_logic;
			   c_out: out std_logic
		);
	end component;

	signal sub_out : std_logic_vector((NBit-1) downto 0);
	
begin
	-- Calcolo C = A - B
	sub: RCA port map (a, b, '0', '1', sub_out, open, open);
	
	comp_0: process (sub_out)
	begin
		-- IF(C < 0) than A < B
		if (signed(sub_out)<0) then
			outLT <= '1';
			outEQ <= '0';
			outGT <= '0';
		-- IF(C > 0) than A > B
		elsif (signed(sub_out)>0) then
			outLT <= '0';
			outEQ <= '0';
			outGT <= '1';
		-- IF(C = 0) than A = B
		else
			outLT <= '0';
			outEQ <= '1';
			outGT <= '0';
		end if;
	end process;
	
end Structural;