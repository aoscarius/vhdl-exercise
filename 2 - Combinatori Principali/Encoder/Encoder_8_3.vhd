-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar


-- 
-- Project Name: ASE Component Library
-- Create Date:  22/11/2014
-- Module Name:  Encoder_8_1.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Questo modulo implementa un encoder 8:3. Sebbene siano possibili
--               diversi tipi di implementazione per questo scopo, si � scelta
--               una a caso, in quanto tutte equivalenti e tutte tradotte dal 
--               compilatore XST come ROM mappate.
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Encoder_8_3 is
	port ( din: in std_logic_vector(7 downto 0);
		   dout : out std_logic_vector(2 downto 0)
	);
end Encoder_8_3;

architecture Behavioral of Encoder_8_3 is
begin
	enc:process(din)
	begin
		case din is
			when "10000000" => dout <= "000";
			when "01000000" => dout <= "001";
			when "00100000" => dout <= "010";
			when "00010000" => dout <= "011";
			when "00001000" => dout <= "100";
			when "00000100" => dout <= "101";
			when "00000010" => dout <= "110";
			when "00000001" => dout <= "111";
			when others => dout <= (others => '0');
		end case;
	end process;
end Behavioral;

