-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  22/11/2014
-- Module Name:  FADecoder.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Questo modulo implementa un full adder, realizzato a partire da
--               un decoder 3:8. Questo � un tipico esempio di rete combinatoria.
--               Tale implementazione � comprensibile andando ad osservare la 
--               tabella di verit� del decoder.
--
--							a|b|cin  7|6|5|4|3|2|1|0   sum|cout
--							-------------------------------------
--							0 0  0   1 0 0 0 0 0 0 0    0    0
--							0 0  1   0 1 0 0 0 0 0 0    1    0 
--							0 1  0   0 0 1 0 0 0 0 0    1    0
--							0 1  1   0 0 0 1 0 0 0 0    0    1
--							1 0  0   0 0 0 0 1 0 0 0    1    0
--							1 0  1   0 0 0 0 0 1 0 0    0    1
--							1 1  0   0 0 0 0 0 0 1 0    0    1
--							1 1  1   0 0 0 0 0 0 0 1    1    1
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FADecoder is
	port ( a, b, cin: in std_logic;
		   sum, cout: out std_logic
	);
end FADecoder;

architecture Behavioral of FADecoder is
	component Decoder_3_8
		port ( din : in std_logic_vector(2 downto 0);
			   dout: out std_logic_vector(7 downto 0)
		);
	end component;
	signal inb : std_logic_vector(2 downto 0);
	signal outb : std_logic_vector(7 downto 0); 
begin
	-- Concateno i segnali in ingresso per il decoder
	inb <= a & b & cin;
	-- Instanzio il decoder con i segnali ricavati
	FA: Decoder_3_8 port map(inb, outb);
	-- Genero le uscite come combinazione dei risultati
	sum <= outb(6) or outb(5) or outb(3) or outb(0);
	cout <= outb(4) or outb(2) or outb(1) or outb(0);
end Behavioral;
