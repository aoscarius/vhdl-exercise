-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar


-- 
-- Project Name: ASE Component Library
-- Create Date:  22/11/2014
-- Module Name:  DeMux_1_4.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx isE 14.x WebPack Free
--
-- Description:  Questo modulo rappresenta l'implementazione di un demux 1:4
--               Vengono proposte varie implementazioni, tutte equivalenti tra
--               loro e riconosciute in maniera automatica come demux dall'XST
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity DeMux_1_4 is
	port ( din : in std_logic;
			 sel : in std_logic_vector(1 downto 0);
			 dout: out std_logic_vector(3 downto 0)
	);
end DeMux_1_4;

-------------------------------------------------------------------------------
-- Implementazione con process e if .. then .. else
-------------------------------------------------------------------------------
architecture bhv_if OF DeMux_1_4 is
begin 
	mux:process(sel, din)
	begin
		IF (sel="00") THEN
			dout <= (0=>din, others=>'0');
		ELSIF (sel="01") THEN
			dout <= (1=>din, others=>'0');
		ELSIF (sel="10") THEN
			dout <= (2=>din, others=>'0');
		ELSIF (sel="11") THEN
			dout <= (3=>din, others=>'0');
		ELSE      
			dout <= (others=>'X');
		END IF;
	end process mux;
end bhv_if;

-------------------------------------------------------------------------------
-- Implementazione con process e case .. is
-------------------------------------------------------------------------------
architecture bhv_case OF DeMux_1_4 is
begin 
	mux:process(sel, din)
	begin
		CASE sel is
			WHEN  "00"  =>  dout <= (0=>din, others=>'0');
			WHEN  "01"  =>  dout <= (1=>din, others=>'0');
			WHEN  "10"  =>  dout <= (2=>din, others=>'0');
			WHEN  "11"  =>  dout <= (3=>din, others=>'0');
			WHEN OTHERS =>  dout <= (others=>'X');
		end CASE;
	end process mux;
end bhv_case;

-------------------------------------------------------------------------------
-- Implementazione con with .. select (assegnazione selettiva)
-------------------------------------------------------------------------------
architecture bhv_with OF DeMux_1_4 is
begin 
	WITH sel SELECT
	dout <= (0=>din, others=>'0') WHEN "00",
			  (1=>din, others=>'0') WHEN "01",
			  (2=>din, others=>'0') WHEN "10",
			  (3=>din, others=>'0') WHEN "11",
			  (others=>'X') WHEN OTHERS; 
end bhv_with;

-------------------------------------------------------------------------------
-- Implementazione con when .. else (assegnazione condizionale)
-------------------------------------------------------------------------------
architecture bhv_when OF DeMux_1_4 is
begin
    dout <= (0=>din, others=>'0') WHEN sel="00" ELSE
            (1=>din, others=>'0') WHEN sel="01" ELSE
            (2=>din, others=>'0') WHEN sel="10" ELSE
            (3=>din, others=>'0') WHEN sel="11" ELSE
            (others=>'0');
end bhv_when;


























