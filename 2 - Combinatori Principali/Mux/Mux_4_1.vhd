-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar


-- 
-- Project Name: ASE Component Library
-- Create Date:  22/11/2014
-- Module Name:  Mux_4_1.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx isE 14.x WebPack Free
--
-- Description:  Questo modulo rappresenta l'implementazione di un mux 4:1
--               Vengono proposte varie implementazioni, tutte equivalenti tra
--               loro e riconosciute in maniera automatica come mux dall'XST
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Mux_4_1 is
	port ( din : in std_logic_vector(3 downto 0);
			 sel : in std_logic_vector(1 downto 0);
			 dout: out std_logic
	);
end Mux_4_1;

-------------------------------------------------------------------------------
-- Implementazione con process e if .. then .. else
-------------------------------------------------------------------------------
architecture bhv_if OF Mux_4_1 is
begin 
	mux:process(sel, din)
	begin
		IF (sel="00") THEN
			dout <= din(0);
		ELSIF (sel="01") THEN
			dout <= din(1);
		ELSIF (sel="10") THEN
			dout <= din(2);
		ELSIF (sel="11") THEN
			dout <= din(3);
		ELSE      
			dout <= 'X';
		END IF;
	end process mux;
end bhv_if;

-------------------------------------------------------------------------------
-- Implementazione con process e case .. is
-------------------------------------------------------------------------------
architecture bhv_case OF mux4_1 is
begin 
	mux:process(sel, din)
	begin
		CASE sel is
			WHEN  "00"  =>  dout <= din(0);
			WHEN  "01"  =>  dout <= din(1);
			WHEN  "10"  =>  dout <= din(2);
			WHEN  "11"  =>  dout <= din(3);
			WHEN OTHERS =>  dout <= 'X';
		end CASE;
	end process mux;
end bhv_case;

-------------------------------------------------------------------------------
-- Implementazione con with .. select (assegnazione selettiva)
-------------------------------------------------------------------------------
architecture bhv_with OF mux4_1 is
begin 
	WITH sel SELECT
	dout <= din(0) WHEN "00",
			  din(1) WHEN "01",
			  din(2) WHEN "10",
			  din(3) WHEN "11",
			  'X' WHEN OTHERS; 
end bhv_with;

-------------------------------------------------------------------------------
-- Implementazione con when .. else (assegnazione condizionale)
-------------------------------------------------------------------------------
architecture bhv_when OF mux4_1 is
begin
    dout <= din(0) WHEN sel="00" ELSE
            din(1) WHEN sel="01" ELSE
            din(2) WHEN sel="10" ELSE
            din(3) WHEN sel="11" ELSE
            'X';
end bhv_when;


























