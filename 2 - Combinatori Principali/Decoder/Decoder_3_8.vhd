-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar


-- 
-- Project Name: ASE Component Library
-- Create Date:  22/11/2014
-- Module Name:  Decoder_1_8.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Questo modulo implementa un decoder 3:8. Sebbene siano possibili
--               diversi tipi di implementazione per questo scopo, si � scelta
--               una a caso, in quanto tutte equivalenti e tutte tradotte dal 
--               compilatore XST come ROM mappate.
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Decoder_3_8 is
	port ( din: in std_logic_vector(2 downto 0);
		   dout: out std_logic_vector(7 downto 0)
	);
end Decoder_3_8;

architecture Behavioral of Decoder_3_8 is
begin
	dec:process(din)
	begin
		case din is
			when "000" => dout <= "10000000";
			when "001" => dout <= "01000000";
			when "010" => dout <= "00100000";
			when "011" => dout <= "00010000";
			when "100" => dout <= "00001000";
			when "101" => dout <= "00000100";
			when "110" => dout <= "00000010";
			when "111" => dout <= "00000001";
			when others => dout <= (others => '0');
		end case;
	end process;
end Behavioral;

