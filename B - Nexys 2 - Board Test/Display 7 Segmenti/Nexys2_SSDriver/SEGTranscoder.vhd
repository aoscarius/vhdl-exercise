-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: SevSegment Driver
-- Create Date:  15/11/2014 
-- Module Name:  SEGTranscoder.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Decoder per display 7 Segmenti a logica attiva bassa
--
-- Dependencies: none
-------------------------------------------------------------------------------
-- Common cathodes - negative logics ( '0'=> on, '1' => off )
--         a
--       =====
--    f || g || b
--       =====
--    e ||   || c
--       ===== .dp
--         d


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SEGTranscoder is
	port ( sel : in std_logic_vector(3 downto 0);		   
		   dp_in : in std_logic;
		   seg : out std_logic_vector(0 to 6); 
		   dp_out : out std_logic
    ); 
end SEGTranscoder;

architecture bhv_BCD of SEGTranscoder is
	signal dp_buff : std_logic;
begin
	dp_buff <= dp_in;
	
	decode:process(sel)
	begin
		case sel is ---------- abcdefg
		when "0000" => seg <= "0000001"; -- Digit 0
		when "0001" => seg <= "1001111"; -- Digit 1
		when "0010" => seg <= "0010010"; -- Digit 2
		when "0011" => seg <= "0000110"; -- Digit 3
		when "0100" => seg <= "1001100"; -- Digit 4
		when "0101" => seg <= "0100100"; -- Digit 5
		when "0110" => seg <= "0100000"; -- Digit 6
		when "0111" => seg <= "0001111"; -- Digit 7
		when "1000" => seg <= "0000000"; -- Digit 8
		when "1001" => seg <= "0000100"; -- Digit 9
		when others => seg <= "1111110"; -- Digit -
		end case; 
	end process;
	
	dp_out <= dp_buff;
end bhv_BCD;

architecture bhv_HEX of SEGTranscoder is
	signal dp_buff : std_logic;
begin
	dp_buff <= dp_in;
	
	decode:process(sel)
	begin
		case sel is ---------- abcdefg
		when "0000" => seg <= "0000001"; -- Digit 0
		when "0001" => seg <= "1001111"; -- Digit 1
		when "0010" => seg <= "0010010"; -- Digit 2
		when "0011" => seg <= "0000110"; -- Digit 3
		when "0100" => seg <= "1001100"; -- Digit 4
		when "0101" => seg <= "0100100"; -- Digit 5
		when "0110" => seg <= "0100000"; -- Digit 6
		when "0111" => seg <= "0001111"; -- Digit 7
		when "1000" => seg <= "0000000"; -- Digit 8
		when "1001" => seg <= "0000100"; -- Digit 9
		when "1010" => seg <= "0001000"; -- Digit A
		when "1011" => seg <= "1100000"; -- Digit b
		when "1100" => seg <= "0110001"; -- Digit C
		when "1101" => seg <= "1000010"; -- Digit d
		when "1110" => seg <= "0110000"; -- Digit E
		when "1111" => seg <= "0111000"; -- Digit F
		when others => seg <= "1111110"; -- Digit -
		end case; 
	end process;
	
	dp_out <= dp_buff;
end bhv_HEX;