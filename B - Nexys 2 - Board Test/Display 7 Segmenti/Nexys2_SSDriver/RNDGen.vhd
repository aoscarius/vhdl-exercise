-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  15/11/2014 
-- Module Name:  RNDGen.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Generatore hardware di numeri pseudo-casuali, facente uso
--               dell'operatore XOR e di uno shift-register retroazionato.
--				 In sintesi il generatore fa uso di un registro interno.
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity RNDGen is
	generic ( bitdepth: natural := 4);
	port ( clk : in STD_LOGIC;
		   rndOut : out STD_LOGIC_VECTOR ((bitdepth-1) downto 0)
	);
end RNDGen;

architecture Behavioral of RNDGen is
begin
	rnd_gen:process(clk)
		variable rnd_tmp : std_logic_vector((bitdepth-1) downto 0):=(0 => '1', others => '0');
		variable tmp : std_logic := '0';
	begin
		if(clk'event and clk='1') then
			-- Eseguo lo xor dei primi 3 bit meno significativi
			tmp := rnd_tmp(1) xor rnd_tmp(0);
			rnd_tmp((bitdepth-2) downto 0) := rnd_tmp((bitdepth-1) downto 1);
			-- E piazzo il risultato in testa dopo aver shiftato tutti i bit di una posizione verso sinistra
			rnd_tmp(bitdepth-1) := tmp;
		end if;
		rndOut <= rnd_tmp;
	end process;
end Behavioral; 