-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: SevSegment Driver
-- Create Date:  15/11/2014 
-- Module Name:  SSDriver.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Driver per display a 7 segmenti a 4 cifre
--
-- Dependencies: SEGTranscoder.vhd, ANDecoder.vhd, BUSMux_4_1.vhd, NCounter.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Nexys2_SSDisplay is
	port (clk: in std_logic;
			sw : in std_logic_vector (3 downto 0);
			btn : in std_logic_vector (3 downto 0);
		   seg : out std_logic_vector(0 to 6);
		   dp : out std_logic;
		   an : out std_logic_vector(3 downto 0)
	);
end Nexys2_SSDisplay;

--========================================================================================================
-- Architettura per il test con generatore di numeri casuali senza controllo
--========================================================================================================
architecture Mixed_RND of Nexys2_SSDisplay is
	component clkGen is
			generic ( FHzIn : integer; FHzOut : integer);
			port ( clk_in : in  STD_LOGIC;
				   clk_out: out STD_LOGIC
			);
	end component;

	component RNDGen is
		generic ( bitdepth: natural := 16);
		port ( clk : in STD_LOGIC;
			   rndOut : out STD_LOGIC_VECTOR ((bitdepth-1) downto 0)
		);
	end component;

	component SSDriver
		port ( d3 : in std_logic_vector(3 downto 0);
			   d2 : in std_logic_vector(3 downto 0);
			   d1 : in std_logic_vector(3 downto 0);
			   d0 : in std_logic_vector(3 downto 0);
			   clk_ref: in std_logic;
			   seg : out std_logic_vector(0 to 6);
			   dp : out std_logic;
			   an : out std_logic_vector(3 downto 0)
		);
	end component;

	-- Internal buffer for digit storage
	signal digits : std_logic_vector(15 downto 0) := (others => '0');
	alias digit_3 is digits(15 downto 12);
   alias digit_2 is digits(11 downto 8);
	alias digit_1 is digits(7 downto 4);
	alias digit_0 is digits(3 downto 0);
	
	-- Segnali di clock
	signal clk_generated : std_logic_vector(1 downto 0);
	
begin
	-- Genero i nuovi clock
	clk_refresh: clkGen generic map (FHzIn => 50000000, FHzOut => 60000) port map (clk, clk_generated(0));
	clk_newdigits: clkGen generic map (FHzIn => 50000000, FHzOut => 4) port map (clk, clk_generated(1));
	-- Visualizzo numeri casuali ogni secondo
	rnd_generated: RNDGen port map(clk_generated(1), digits);
	sseg_display: SSDriver port map(digit_3, digit_2, digit_1, digit_0, clk_generated(0), seg, dp, an);
end Mixed_RND;

--========================================================================================================
-- Architettura per il test con generatore di numeri casuali con controllo manuale del tempo di refresh
--========================================================================================================
architecture Mixed_DIVSEL of Nexys2_SSDisplay is
	component clkGen is
			generic ( FHzIn : integer; FHzOut : integer);
			port ( clk_in : in  STD_LOGIC;
				   clk_out: out STD_LOGIC
			);
	end component;

	component clkDivSel
		port ( sel : in std_logic_vector(3 downto 0);
				reset : in std_logic;
				clk_in : in std_logic;   
				clk_out : out std_logic
		); 
	end component;

	component RNDGen is
		generic ( bitdepth: natural := 16);
		port ( clk : in STD_LOGIC;
			   rndOut : out STD_LOGIC_VECTOR ((bitdepth-1) downto 0)
		);
	end component;

	component SSDriver
		port ( d3 : in std_logic_vector(3 downto 0);
			   d2 : in std_logic_vector(3 downto 0);
			   d1 : in std_logic_vector(3 downto 0);
			   d0 : in std_logic_vector(3 downto 0);
			   clk_ref: in std_logic;
			   seg : out std_logic_vector(0 to 6);
			   dp : out std_logic;
			   an : out std_logic_vector(3 downto 0)
		);
	end component;

	-- Internal buffer for digit storage
	signal digits : std_logic_vector(15 downto 0) := (others => '0');
	alias digit_3 is digits(15 downto 12);
   alias digit_2 is digits(11 downto 8);
	alias digit_1 is digits(7 downto 4);
	alias digit_0 is digits(3 downto 0);
	
	-- Segnali di clock
	signal clk_generated : std_logic_vector(1 downto 0);

	
begin
	-- Genero i nuovi clock
	clk_refresh: clkDivSel port map (sw, btn(0), clk, clk_generated(0));
	clk_newdigits: clkGen generic map (FHzIn => 50000000, FHzOut => 4) port map (clk, clk_generated(1));
	-- Visualizzo numeri casuali ogni secondo
	rnd_generated: RNDGen port map(clk_generated(1), digits);
	sseg_display: SSDriver port map(digit_3, digit_2, digit_1, digit_0, clk_generated(0), seg, dp, an);
end Mixed_DIVSEL;

--========================================================================================================
-- Architettura per il test con registri a caricamento manuale controllo manuale del tempo di refresh
--========================================================================================================
architecture Mixed_REG of Nexys2_SSDisplay is
	component DFFRegister
		generic ( N : natural := 4 );
		port ( clk, reset, en: in std_logic;
				d : in std_logic_vector(N-1 downto 0);
				q : out std_logic_vector(N-1 downto 0)
		);
	end component ;

	component clkGen is
			generic ( FHzIn : integer; FHzOut : integer);
			port ( clk_in : in  STD_LOGIC;
				   clk_out: out STD_LOGIC
			);
	end component;

	component SSDriver
		port ( d3 : in std_logic_vector(3 downto 0);
			   d2 : in std_logic_vector(3 downto 0);
			   d1 : in std_logic_vector(3 downto 0);
			   d0 : in std_logic_vector(3 downto 0);
			   clk_ref: in std_logic;
			   seg : out std_logic_vector(0 to 6);
			   dp : out std_logic;
			   an : out std_logic_vector(3 downto 0)
		);
	end component;

	-- Internal buffer for digit storage
	signal digit_3 : std_logic_vector(3 downto 0) := (others => '0');
   signal digit_2 : std_logic_vector(3 downto 0) := (others => '0');
	signal digit_1 : std_logic_vector(3 downto 0) := (others => '0');
	signal digit_0 : std_logic_vector(3 downto 0) := (others => '0');
	
	-- Segnali di clock
	signal clk_generated : std_logic;
	
begin
	-- Genero i nuovi clock
	clk_refresh: clkGen generic map (FHzIn => 50000000, FHzOut => 60000) port map (clk, clk_generated);
	-- Carico i numeri in registri e li visualizzo
	dg3: DFFRegister port map (clk, '0', btn(3), sw, digit_3);
	dg2: DFFRegister port map (clk, '0', btn(2), sw, digit_2);
	dg1: DFFRegister port map (clk, '0', btn(1), sw, digit_1);
	dg0: DFFRegister port map (clk, '0', btn(0), sw, digit_0);
	sseg_display: SSDriver port map(digit_3, digit_2, digit_1, digit_0, clk_generated, seg, dp, an);
end Mixed_REG;