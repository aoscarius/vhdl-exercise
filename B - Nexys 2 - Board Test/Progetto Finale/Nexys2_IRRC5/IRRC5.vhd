-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: IR-RC5 Module
-- Create Date:  9/01/2015
-- Module Name:  CUDec.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Modulo che implementa la control unit, impiegata nel modulo di 
--               ricezione IR-RC5. Questo modulo si occupa della generazione corretta
--               dei sincronismi e della supervisione sul dato ricevuto
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity CUDec is
	generic ( sys_clock : integer := 50_000_000 );
	port (sys_clk	  : in std_logic;	 -- Clock di sistema
			reset		  : in std_logic;	 -- Resetta il sistema
			ir_mod_in  : in std_logic;	 -- Segnale di ingresso dal modulo rx
			toggle_in  : in std_logic;	 -- Segnale di toggle decodificato
			dec_end_rx : in std_logic;	 -- Segnale di ricezione completata		
			dec_err_rx : in std_logic;	 -- Segnale di errore durante la ricezione
			all_reset  : out std_logic; -- Segnale di reset del decodificatore
			load_data  : out std_logic; -- Abilita la memorizzazione del dato decodificato
			gen_err   : out std_logic;  -- Alto se vi � un errore nella trasmissione
			gen_irq	  : out std_logic  -- Attiva la linea di interruzione
	);
end CUDec;

architecture Behavioral of CUDec is
	
	constant half_cycle_ticks : natural := sys_clock/1124; -- Numero di sys_ticks in mezzo periodo di dati

	type state_type is (s_reset, s_idle, s_sync, s_start, s_stop);
	signal state: state_type := s_idle;
	
	-- Segnali di controllo per il sync della decodifica
	signal sync_counter : natural range 0 to half_cycle_ticks := 0; 
	signal sync_reset : std_logic := '0';
	signal sync_start : std_logic := '0';
	signal is_sync : std_logic := '0';

	-- Segnale di memorizzazione del toogle bit
	signal reg_toggle : std_logic := '0';

begin

	-- Questo processo implementa un contatore, impiegato per 
	-- avviare la decodifica ad inizio ricezione. Permette di 
	-- sincronizzare il decodificatore con i dati ricevuti
	halt_sync_count: process(sys_clk, sync_reset, sync_start)
	begin
		if sync_reset='1' then
			is_sync <= '0';
			sync_counter <= half_cycle_ticks;
		elsif (sys_clk'event and sys_clk='1') then
			if (sync_start = '1') then
				sync_counter <= sync_counter - 1;
				if (sync_counter = 0) then
					is_sync <= '1';
				end if;
			end if;
		end if;
	end process;

	-- Macchina a stati finiti di controllo
	fsm_main: process (sys_clk, reset, ir_mod_in, is_sync, dec_end_rx, dec_err_rx) 
	begin

		if reset='1' then
		
			gen_err <= '0'; -- Questo reset viene effettuato fisicamente solo al reset manuale
			state <= s_reset;
			
		elsif (sys_clk'event and sys_clk='1') then

			case state is
				
				-- Stato di reset dove, tutti i parametri vengoni inizializzati
				when s_reset => 
					all_reset <= '0';
					load_data <= '0';
					gen_irq <= '0';
					
					reg_toggle <= toggle_in;
					
					sync_reset <= '1';
					sync_start <= '0';
					state <= s_idle;

				-- Stato di idle, dove la macchina attende la prima variazione da
				-- alto a basso del segnale di ricezione, per poter avviare il sincronismo
				-- e quindi la decodifica del dato
				when s_idle => 	
					sync_reset <= '0';
					if (ir_mod_in='0') then
						sync_start <= '1';
						state <= s_sync;
					end if;
				
				-- Stato di sync in cui si attende il termine della fase di sync per avviare 
				-- la decodifica
				when s_sync =>
					gen_err <= '0';
					all_reset <= '1';
					if (is_sync='1') then
						sync_start <= '0';
						state <= s_start;
					end if;
					
				-- Stato di decodifica al termine del quale, se vi � stato un errore, 
				-- il sistema si autoresetta in attesa di un nuovo dato, altrimenti
				-- passa alla fase di controllo del dato ricevuto
				when  s_start =>
					all_reset <= '0';
					if (dec_err_rx='1') then
						gen_err <= '1';
						state <= s_reset;
					elsif (dec_end_rx='1') then
						state <= s_stop;
					end if;
										
				-- Stato di termine trasmissione e controllo. Se il dato ricevuto
				-- presenta un toggle bit differente da quello precedente allora � 
				-- stato ricevuto un nuovo dato, come da specifiche del protocollo
				-- e viene quindi alzata la linea di interrupt e viene abilitata la
				-- memorizzazione del dato
				when s_stop => 
					load_data <= '1';
					if (reg_toggle /= toggle_in) then
						gen_irq <= '1'; 
					end if;
					state <= s_stop;

				when others =>  
					state <= s_reset;
			
				end case;
			end if;
	end process;
end Behavioral;

-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: IR-RC5 Module
-- Create Date:  9/01/2015
-- Module Name:  CUEnc.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Modulo che implementa la control unit, impiegata nel modulo di 
--               trasmissione IR-RC5. Questo modulo si occupa della generazione corretta
--               dei sincronismi e della supervisione sulla codifica del dato da trasmettere
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity CUEnc is
	generic ( sys_clock    : integer := 50_000_000 );
	port (sys_clk	        : in std_logic;	 -- Clock di sistema
			reset				  : in std_logic;  -- Resetta il sistema
			send		        : in std_logic;	 -- Avvia il sistema
			end_tx			  : in std_logic;  -- Segnale di fine trasmissione pacchetto
			toggle  	        : out std_logic; -- Segnale di toggle da codificare
			load_data        : out std_logic; -- Abilita la memorizzazione del dato da codificare
			all_reset        : out std_logic  -- Segnale di reset del codificatore
	);
end CUEnc;

architecture Behavioral of CUEnc is
	
	constant wdog_ticks : natural := (sys_clock/10); -- Numero di sys_ticks di pausa tra una trasmissione ed un altra

	type state_type is (s_reset, s_idle, s_toogle, s_load, s_start_reset, s_start_rx, s_wdog);
	signal state: state_type := s_idle;
	
	-- Segnali di controllo per il sync della decodifica
	signal wdog_counter : natural range 0 to wdog_ticks := 0; 
	signal wdog_reset : std_logic := '0';
	signal wdog_start : std_logic := '0';
	signal wdog_timeout : std_logic := '0';

	-- Segnale di memorizzazione del toogle bit
	signal reg_toggle : std_logic := '1';

begin

	-- Questo processo implementa un contatore, impiegato per 
	-- avviare la il reset automatico e il reinvio del pachetto,
	-- ogni wdog_ticks se il tasto send resta premuto
	watchdog_count: process(sys_clk, wdog_reset, wdog_start)
	begin
		if wdog_reset='1' then
			wdog_timeout <= '0';
			wdog_counter <= wdog_ticks;
		elsif (sys_clk'event and sys_clk='1') then
			if (wdog_start = '1') then
				wdog_counter <= wdog_counter - 1;
				if (wdog_counter = 0) then
					wdog_timeout <= '1';
				end if;
			end if;
		end if;
	end process;

	-- Macchina a stati finiti di controllo
	fsm_main: process (sys_clk, send, end_tx, wdog_timeout) 
	begin

		if reset='1' then
		
			state <= s_reset;
			
		elsif (sys_clk'event and sys_clk='1') then
			
			case state is
			
				-- Stato di reset dove, tutti i parametri vengoni inizializzati
				when s_reset => 
					load_data <= '0';
					all_reset <= '1';
					
					wdog_reset <= '1';
					wdog_start <= '0';
					
					state <= s_idle;
				
				-- Stato di idle, dove la macchina attende l'attivazione del segnale di send
				-- per iniziare il processo di memorizzazione, codifica ed invio del dato
				when s_idle => 	
					if (send='1') then
						state <= s_toogle;
					end if;
				
				-- Stato in cui viene togglato il bit di toggle da inserire nel dato da inviare
				when s_toogle =>
					reg_toggle <= not reg_toggle;
					state <= s_load;
					
				-- Stato in cui viene abilitata la memorizzazione del dato da inviare
				when s_load =>
					load_data <= '1';
					state <= s_start_reset;

				-- Stage 1 dello stato di start in cui vengono effettuate le operazioni di reset
				-- del contatore watchdog. In realt� questo stato � ridondante ma � stato inserito 
				-- per ritardare di un ciclo di clock la fase di memorizzazione del dato, prima di
				-- iniziare l'invio
				when s_start_reset =>
					wdog_reset <= '1';
					wdog_start <= '0';
					state <= s_start_rx;

				-- Stage 2 dello stato di start dove tutti i segnali di reset vengono abbassati 
				-- e viene iniziata effettivamente la codifica e trasmissione. Il sistema, resta
				-- in questo stato finch� non � terminata la trasmissione, dopodich�, se il segnale
				-- di send � ancora alto allora avvia il watchdog per inviare un nuovo pachetto,
				-- altrimenti ritorna in fase di reset
				when s_start_rx =>
					load_data <= '0';
					all_reset <= '0';
					wdog_reset <= '0';
					if (end_tx='1') then
						if (send='1') then
							wdog_start <= '1';
							state <= s_wdog;
						else
							state <= s_reset;
						end if;
					end if;
										
				-- Stato di controllo del watchdog. Lo stato resta in attesa del segnale di timeout
				-- del contatore watchdog per poter riavviare la codifica e trasmissione del dato
				-- gia memorizzato, purch� il segnale di send sia ancora alto, altrimenti resetta il sistema
				when s_wdog => 
					if (send = '1') then
						if (wdog_timeout = '1') then
							all_reset <= '1';
							state <= s_start_reset;
						end if;
					else
						state <= s_reset;
					end if;

				when others =>  
					state <= s_reset;
			
				end case;
			end if;
	end process;
	
	toggle <= reg_toggle;
	
end Behavioral;

-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: IR-RC5 Module
-- Create Date:  07/01/2015
-- Module Name: RC5_Rx.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Modulo per la ricezione seriale su infrarossi, secondo il 
--               protocollo RC5 della Philips.
--
-- Dependencies: sPWM.vhd, RC5Dec.vhd, CUDec.vhd
-------------------------------------------------------------------------------
-- Il comando � composto da 14 bits:
--
-- | 1 | 1 | T | A | A | A | A | A | C | C | C | C | C | C |
-- |  ACG  |CHK|      ADDRESS      |        COMMAND        |
--
-- Il valore del singolo bit (0 o 1) � determinato dal tipo di fronte salita o 
-- discesa che veniamo a trovare:
-- 
-- rising_edge = 0 "01" nella cella bit (in ricezione)
-- falling_edge = 1 "10" nella cella bit (in ricezione)
-- 
-- ACG: Rappresenta la sequenza di start ed � composta dallo start bit sempre 1
--      e dal field bit utilizzato solo nella versione extended altrimenti 1.
-- CHK o TOGGLE: Questo bit cambia di stato logico ad ogni ritrasmissione. Ad 
--               esempio se premo il tasto 1 del mio telecomando fintanto che 
--               lo tengo premuto il bit TOGGLE rimarr� sempre uguale, mentre 
--               se rilascio il tasto e lo ripremo cambier� stato logico. 
--               Questo fornisce informazione al micropc in ricezione che ho 
--               appunto rilasciato e premuto un tasto.
-- ADDRESS: Qui troviamo in sequenza 5 bit utilizzati come indirizzo per poter 
--          comandare fino a 32 apparecchi diversi simultaneamente. Normalmente
--          troviamo "00000" come indirizzo per il comando del TV.
-- COMMAND: Qui troviamo in sequenza 6 bit utilizzati per la trasmissione del
--          comando vero e proprio. Che quindi nella versione base di RC5 
--          permette 2^6 = 64 comandi. Ad esempio "000000" corrisponde al tasto 
--          programma 0, mentre "000001" al tasto 1 e cos� via.
-- 
-- Sia per ADDRESS che per COMMAND la sequenza dei bit va dal pi� significatico
-- al meno significativo.
-- 
-- La larghezza di ciascun stato logico, che possiamo chiamare step, � di 889 usec.
-- Un bit � formato da 2 step.
-- Quindi la lunghezza totale � 889 x 2 x 14 = 24,9 msec
-- La pausa tra un treno impulsi e il successivo � di 105 msec

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity RC5_Rx is
	port (sys_clk   : in std_logic;							  -- Clock di sistema
			reset		 : in std_logic;							  -- Resetta il sistema e lo prepara ad una nuova ricezione
			ir_mod_in : in std_logic;               		  -- Direttamente collegabile ad un modulo rx ad infrarossi
			no_signal : out std_logic;							  -- Segnale di errore sul mezzo trasmissivo
			irq_ir	 : out std_logic;							  -- Richiesta di interrupt: se � attiva � disponibile una nuova ricezione
			address   : out std_logic_vector(4 downto 0);  -- Indirizzo del dispositivo 
			command   : out std_logic_vector(6 downto 0)); -- Comando per il dispositivo
end RC5_Rx;

architecture mixed of RC5_Rx is
	component sPWM is
		generic(
			mode		: std_logic := '0';		 --Tipo di forma d'onda ..|''|(0) |''|..(1) 
			sys_clk  : integer := 50_000_000; --Clock di sistema in hz
			pwm_freq : integer := 36_000;     --Frequenza del PWM in hz
			duty     : integer := 30);			 --Duty cycle in % (ampiezza livello logico alto)
		port(
			clk      : in  std_logic;         --Clock di sistema
			reset    : in  std_logic;         --Reset del modulo
			pwm_out  : out std_logic);        --Outputs PWM
	end component;

	component RC5Dec is
		 generic ( N: natural := 13 );
		 port ( clk : in std_logic;									     -- Clock di campionamento
				  reset : in std_logic;                              -- Reset dei registri interni
				  in_data : in std_logic;                            -- Dato seriale codificato in ingresso
				  end_rx : out std_logic;									  -- Attivo quando tutti gli N bit sono stati ricevuti
				  error_rx : out std_logic;								  -- Attivo quando si � presentato un errore nella decodifica
				  data_bus : out std_logic_vector ((N-1) downto 0)); -- Dato ricevuto
	end component;

	component CUDec is
		port (sys_clk	  : in std_logic;	 -- Clock di sistema
				reset		  : in std_logic;	 -- Resetta il sistema
				ir_mod_in  : in std_logic;	 -- Segnale di ingresso dal modulo rx
				toggle_in  : in std_logic;	 -- Segnale di toggle decodificato
				dec_end_rx : in std_logic;	 -- Segnale di ricezione completata		
				dec_err_rx : in std_logic;	 -- Segnale di errore durante la ricezione
				all_reset  : out std_logic; -- Segnale di reset del decodificatore
				load_data  : out std_logic; -- Abilita la memorizzazione del dato decodificato
				gen_err   : out std_logic;  -- Alto se vi � un errore nella trasmissione
				gen_irq	  : out std_logic  -- Attiva la linea di interruzione
		);
	end component;
	

	signal sample_clk : std_logic := '0';
	signal reg_data : std_logic_vector(12 downto 0) := (others => '0');

	signal cu_dec_end_rx : std_logic := '0';
	signal cu_dec_err_rx : std_logic := '0';
	signal cu_all_reset : std_logic := '0';
	signal cu_load_data : std_logic := '0';
	
begin
	
	rx_successfull: process (sys_clk, cu_load_data, reg_data)
	begin
		if (sys_clk'event and sys_clk='1') then
			if (cu_load_data = '1') then
				address <= reg_data(10 downto 6);
				-- Il bit C6, del command va interpretato negato (supporto a RC5-EX)
				command <= (not reg_data(12)) & reg_data(5 downto 0);
			else 
				address <= (others => '0');
				command <= (others => '0');
			end if;
		end if;
	end process;

	sample_clk_gen : sPWM 
		generic map (
			pwm_freq => 1124, -- Frequenza di bit rate (frequenza delle celle bit)
			duty => 25    
		)
		port map (
			clk => sys_clk,
			reset => cu_all_reset,
			pwm_out => sample_clk
		);

	rc5_decoder : RC5Dec
		port map (
			clk => sample_clk,
			reset => cu_all_reset,
			in_data => ir_mod_in,
			end_rx => cu_dec_end_rx,
			error_rx => cu_dec_err_rx,
			data_bus => reg_data
		);

	rc5_control_unit_rx : CUDec
		port map (
			sys_clk => sys_clk,
			reset => reset,
			ir_mod_in => ir_mod_in,
			toggle_in => reg_data(11),
			dec_end_rx => cu_dec_end_rx,
			dec_err_rx => cu_dec_err_rx,
			all_reset => cu_all_reset,
			load_data => cu_load_data,
			gen_err => no_signal,
			gen_irq => irq_ir
		);

end mixed;

-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: IR-RC5 Module
-- Create Date:  06/01/2015
-- Module Name: RC5_Tx.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Modulo per la trasmissione seriale su infrarossi, secondo il 
--               protocollo RC5 della Philips.
--
-- Dependencies: sPWM.vhd, RC5Enc.vhd, CUEnc.vhd
-------------------------------------------------------------------------------
-- Il comando � composto da 14 bits:
--
-- | 1 | 1 | T | A | A | A | A | A | C | C | C | C | C | C |
-- |  ACG  |CHK|      ADDRESS      |        COMMAND        |
--
-- Il valore del singolo bit (0 o 1) � determinato dal tipo di fronte salita o 
-- discesa che veniamo a trovare:
-- 
-- rising_edge = 1 "01" nella cella bit (in trasmissione)
-- falling_edge = 0 "10" nella cella bit (in trasmissione)
-- 
-- ACG: Rappresenta la sequenza di start ed � composta dallo start bit sempre 1
--      e dal field bit utilizzato solo nella versione extended altrimenti 1.
-- CHK o TOGGLE: Questo bit cambia di stato logico ad ogni ritrasmissione. Ad 
--               esempio se premo il tasto 1 del mio telecomando fintanto che 
--               lo tengo premuto il bit TOGGLE rimarr� sempre uguale, mentre 
--               se rilascio il tasto e lo ripremo cambier� stato logico. 
--               Questo fornisce informazione al micropc in ricezione che ho 
--               appunto rilasciato e premuto un tasto.
-- ADDRESS: Qui troviamo in sequenza 5 bit utilizzati come indirizzo per poter 
--          comandare fino a 32 apparecchi diversi simultaneamente. Normalmente
--          troviamo "00000" come indirizzo per il comando del TV.
-- COMMAND: Qui troviamo in sequenza 6 bit utilizzati per la trasmissione del
--          comando vero e proprio. Che quindi nella versione base di RC5 
--          permette 2^6 = 64 comandi. Ad esempio "000000" corrisponde al tasto 
--          programma 0, mentre "000001" al tasto 1 e cos� via.
-- 
-- Sia per ADDRESS che per COMMAND la sequenza dei bit va dal pi� significatico
-- al meno significativo.
-- 
-- La larghezza di ciascun stato logico, che possiamo chiamare step, � di 889 usec.
-- Un bit � formato da 2 step.
-- Quindi la lunghezza totale � 889 x 2 x 14 = 24,9 msec
-- La pausa tra un treno impulsi e il successivo � di 105 msec

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity RC5_Tx is
	port (sys_clk : in std_logic;							 -- Clock di sistema
			reset   : in std_logic;							 -- Reset di sistema
			send    : in std_logic;							 -- Finch� � attivo invia la trasmissione
			address : in std_logic_vector(4 downto 0); -- Indirizzo del dispositivo 
			command : in std_logic_vector(6 downto 0); -- Comando per il dispositivo
			ir_led_out : out std_logic;					 -- Direttamente collegabile ad un led ad infrarossi
			ir_mod_out : out std_logic);               -- Direttamente collegabile in tx con filo (loop function)
end RC5_Tx;

architecture mixed of RC5_Tx is
	component sPWM is
		generic(
			mode		: std_logic := '1';		 --Tipo di forma d'onda ..|''|(0) |''|..(1) 
			sys_clk  : integer := 50_000_000; --Clock di sistema in hz
			pwm_freq : integer := 36_000;     --Frequenza del PWM in hz
			duty     : integer := 30);			 --Duty cycle in % (ampiezza livello logico alto)
		port(
			clk      : in  std_logic;         --Clock di sistema
			reset    : in  std_logic;         --Reset del modulo
			pwm_out  : out std_logic);        --Outputs PWM
	end component;

	component RC5Enc is
		 generic ( N: natural := 14);
		 port ( clk : in std_logic;									    -- Clock di trasmissione
				  reset : in std_logic;                             -- Reset e caricamento del dato
				  data_bus : in  std_logic_vector ((N-1) downto 0); -- Dato da trasmettere
				  end_tx : out  std_logic;                          -- Attivo a fine trasmissione
				  out_data : out  std_logic);                       -- Dato codificato
	end component;

	component CUEnc is
		port (sys_clk	        : in std_logic;	 -- Clock di sistema
				reset				  : in std_logic;  -- Resetta il sistema
				send		        : in std_logic;	 -- Avvia il sistema
				end_tx			  : in std_logic;  -- Segnale di fine trasmissione pacchetto
				toggle  	        : out std_logic; -- Segnale di toggle da codificare
				load_data        : out std_logic; -- Abilita la memorizzazione del dato da codificare
				all_reset        : out std_logic  -- Segnale di reset del codificatore
		);
	end component;

	signal carrier_clk : std_logic := '0';
	signal data_clk : std_logic := '0';

	signal reg_data : std_logic_vector(13 downto 0) := (others => '0');
	signal reg_out : std_logic := '0';
	
	signal cu_end_tx : std_logic := '0';

	signal cu_toggle : std_logic := '0';
	signal cu_load_data :std_logic := '0';
	signal cu_all_reset : std_logic := '0'; 
	
begin
	
	tx_start: process (sys_clk, cu_load_data, reg_data)
	begin
		if (sys_clk'event and sys_clk='1') then
			if (cu_load_data = '1') then
				-- Nella versione RC5-EX il bit C6 di command ha significato negato
				-- mentre nella RC5, questo bit � sempre 1
				reg_data <= '1' & (not command(6)) & cu_toggle & address & command(5 downto 0);
			end if;
		end if;
	end process;

	carrier_clk_gen : sPWM 
		generic map (
			pwm_freq => 36_000, -- Frequenza della portante
			duty => 30    
		)
		port map (
			clk => sys_clk,
			reset => cu_all_reset,
			pwm_out => carrier_clk
		);

	data_clk_gen : sPWM 
		generic map (
			pwm_freq => 1124, -- Frequenza di bit rate (frequenza delle celle bit)
			duty => 50    
		)
		port map (
			clk => sys_clk,
			reset => cu_all_reset,
			pwm_out => data_clk
		);
	
	rc5_encoder : RC5Enc
		port map (
			clk => data_clk,
			reset => cu_all_reset,
			data_bus => reg_data,
			end_tx => cu_end_tx,
			out_data => reg_out
		);

	rc5_control_unit_tx : CUEnc
		port map (
			sys_clk => sys_clk,
			reset => reset,
			send => send,
			end_tx => cu_end_tx,
			toggle => cu_toggle,
			load_data => cu_load_data,
			all_reset => cu_all_reset
		);
		
	ir_led_out <= reg_out and carrier_clk; -- Modulazione della portante con il segnale codificato
	ir_mod_out <= not reg_out; -- Segnale codificato negato (utile per le connessioni con filo)

end mixed;

-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: IR-RC5 Module
-- Create Date:  05/01/2015
-- Module Name:  RC5Enc.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Modulo per la decodifica Manchester, nota anche come codifica 
--					  bi-fase, di un dato ad N bit. Il modulo necessita di essere 
--               allineato ad inizio trasmissione per poter effettuare una corretta
--               decodifica.
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity RC5Dec is
	 generic ( N: natural := 14 );
    port ( clk : in std_logic;									     -- Clock di campionamento
			  reset : in std_logic;                              -- Reset dei registri interni
           in_data : in std_logic;                            -- Dato seriale codificato in ingresso
			  end_rx : out std_logic;									  -- Attivo quando tutti gli N bit sono stati ricevuti
			  error_rx : out std_logic;								  -- Attivo quando si � presentato un errore nella decodifica
			  data_bus : out std_logic_vector ((N-1) downto 0)); -- Dato ricevuto
end RC5Dec;

architecture behavioral of RC5Dec is
	
	signal reg_sample : std_logic := '0';
	signal ff_sample : std_logic := '0';

	signal rx_enabled : std_logic := '0';
	signal bit_counter : integer range 0 to N := N; -- Contatore di bit interno
	signal reg_data : std_logic_vector ((N-1) downto 0) := (others => '0');
	
begin
	-- Il duty cycle del clock, determina il centraggio del campionamento
	-- mentre l'allineamento � dato dal rilascio del reset
	process(clk, reset)
	begin
		if (reset = '1') then
			reg_sample <= '0';
			ff_sample <= '0';
			
			error_rx <= '0';
			
			rx_enabled <= '1';
			reg_data <= (others => '0');
			bit_counter <= N;
			
			data_bus <= (others => '0');
		else
			if (rx_enabled='1') then
				if (clk'event and clk = '1') then 
				   -- Attraverso un FF-T
					ff_sample <= not ff_sample;
					-- Ogni due colpi di clock, campiono il bit in ingresso
					if (ff_sample = '0') then 
						reg_sample <= in_data;
					else
					-- Al successivo colpo di clock, confronto il bit campionato
					-- con l'attuale e quindi decodifico il dato
						if ((reg_sample = '0') and (in_data = '1')) then
							reg_data <= reg_data((N-2) downto 0) & '0';
							bit_counter <= bit_counter - 1;
						elsif ((reg_sample = '1') and (in_data = '0')) then
							reg_data <= reg_data((N-2) downto 0) & '1';
							bit_counter <= bit_counter - 1;
						elsif (reg_sample = in_data) then
							error_rx <= '1';
						end if;
					end if;
					-- Se il contatore di bit ha raggiunto lo 0 termino la ricezione
					if (bit_counter = 0) then
						rx_enabled <= '0';
						data_bus <= reg_data;
					else
						data_bus <= (others => '0');
					end if;
				end if;
			end if;
		end if;
	end process ;
	
	end_rx <= not rx_enabled;

end behavioral;

-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: IR-RC5 Module
-- Create Date:  02/01/2015
-- Module Name:  RC5Enc.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Modulo per la codifica Manchester, nota anche come codifica 
--					  bi-fase, di un dato ad N bit. Il modulo pone in uscita in 
--               modalit� sequenziale, il dato contenuto in data_bus.
--
-- Dependencies: none
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity RC5Enc is
	 generic ( N: natural := 14);
    port ( clk : in std_logic;									    -- Clock di trasmissione
			  reset : in std_logic;                             -- Reset e caricamento del dato
			  data_bus : in  std_logic_vector ((N-1) downto 0); -- Dato da trasmettere
			  end_tx : out std_logic;									 -- Attivo quando tutti gli N bit sono stati trasmessi
           out_data : out  std_logic);                       -- Dato serial codificato in uscita
end RC5Enc;

architecture behavioral of RC5Enc is
	signal ff_clk : std_logic := '0'; -- FlipFlop T utilizzato per dimezzare la frequenza di clock

	signal tx_enabled : std_logic := '0';
	signal bit_counter : integer range 0 to N := N; -- Contatore di bit interno (+1 bit di sync)
	signal reg_data : std_logic_vector ((N-1) downto 0) := (others => '0');
begin
	process(clk, reset, data_bus)
	begin
		-- Se � attivo il reset
		if (reset = '1') then
			ff_clk <= '0';
			
			-- Carico il dato da trasferire in un registro buffer interno
			reg_data <= data_bus;

			tx_enabled <= '1';
			bit_counter <= N;
			out_data <= '0';
		else
			-- Finch� non ho completato la trasmissione di tutti i bit
			if (tx_enabled = '1') then
				-- Ad ogni ciclo di clock
				if (clk'event and clk='1') then
					-- Togglo il flip-flop usato come riferimento per il posizionamento del dato
					ff_clk <= not ff_clk;
					-- Se l'FF-T � alto
					if (ff_clk = '1') then 
						-- Allora shifto il registro ad anello e decremento il contatore di bit
						reg_data <= reg_data((N-2) downto 0) & reg_data(N-1);
						bit_counter <= bit_counter - 1;
					end if;
					if (bit_counter = 0) then
						tx_enabled <= '0';
					end if;
				-- La codifica in uscita secondo manchester � dato dallo xor tra il bit piu 
				-- significativo da trasmettere e il clock del dato (negato a causa della natura 
				-- della forma d'onda del clock ..|''|)
					out_data <= reg_data(N-1) xor (not ff_clk);
				end if;
			else
			-- Se la trasmissione � disabilitata all'ora l'uscita � nulla
				out_data <= '0';
			end if;		
		end if;
	end process;
	
	end_tx <= not tx_enabled;
	
end behavioral;

-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: IR-RC5 Module
-- Create Date:  06/01/2015
-- Module Name:  sPWM.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Modulo per la generazione di un segnale PWM, a frequenza prefissata e 
--					  duty cycle fisso e prefissato in fase di istanziazione.
--
-- Dependencies: none
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity sPWM is
  generic(
      mode		: std_logic := '0';		 --Tipo di forma d'onda ..|''|(0) |''|..(1) 
		sys_clk  : integer := 50_000_000; --Clock di sistema in hz
      pwm_freq : integer := 36_000;     --Frequenza del PWM in hz
		duty     : integer := 30);			 --Duty cycle in % (ampiezza livello logico alto)
  port(
      clk      : in  std_logic;         --Clock di sistema
      reset    : in  std_logic;         --Reset del modulo
      pwm_out  : out std_logic);        --Outputs PWM
end sPWM;

architecture behavioral of sPWM is
	constant period   : integer := sys_clk/pwm_freq;         --Numero di clocks in un periodo
	constant reg_duty : integer := duty*period/100;          --Numero di clocks in un duty cycle
	signal reg_count  : integer range 0 to period - 1 := 0;  --Contatore di clocks
begin
	process(clk, reset)
	begin
		-- Reset asincrono
		if(reset = '1') then
			reg_count <= 0;
			pwm_out <= '0';
	   -- Ad ogni ciclo di clock
		elsif(clk'event and clk = '1') then                      
			-- Se il contatore ha raggiunto il numero di clocks per periodo
			-- allora resetto il contatore
			if(reg_count = (period-1)) then
				reg_count <= 0;
			else
				reg_count <= reg_count + 1;
			end if;
			-- Nel mode = '0' dove la forma d'onda � del tipo ....|''''|
			if	(mode = '0') then
				-- Se il contatore ha superato il numero di clock per duty cycle
				-- allora porto l'uscita al suo valore logico basso
				if(reg_count > (period-reg_duty-1)) then
					pwm_out <= '1';
				else
					pwm_out <= '0';
				end if;
			
			-- Altrimenti nel mode = '1' dove la forma d'onda � del tipo |''''|....
			else
				-- Se il contatore ha superato il numero di clock per duty cycle
				-- allora porto l'uscita al suo valore logico basso
				if(reg_count > (reg_duty-1)) then
					pwm_out <= '0';
				else
					pwm_out <= '1';
				end if;
			end if;
		end if;
	end process;
end behavioral;

