-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: SevSegment Driver
-- Create Date:  15/11/2014 
-- Module Name:  ANDecoder.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Decoder per led timing division a logica attiva bassa
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ANDecoder is
	port ( sel : in std_logic_vector(1 downto 0);
		   an : out std_logic_vector(3 downto 0)
	);
end ANDecoder;

architecture Behavioral of ANDecoder is
begin
process(sel)
	begin
		case sel is
			when "00" => an <= "0111";
			when "01" => an <= "1011";
			when "10" => an <= "1101";
			when "11" => an <= "1110";
			when others => an <= "1111";
		end case; 
	end process;
end Behavioral;

-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  15/11/2014 
-- Module Name:  BUSMux_4_1.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Multiplexer 4:1 per BUS a N bit
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity BUSMux_4_1 is
	generic ( N: natural := 4 );
	port (din_0 : in std_logic_vector((N-1) downto 0);   
		  din_1 : in std_logic_vector((N-1) downto 0); 
		  din_2 : in std_logic_vector((N-1) downto 0);
		  din_3 : in std_logic_vector((N-1) downto 0);
		  sel : in std_logic_vector(1 downto 0); 
		  dout : out std_logic_vector((N-1) downto 0)
	);
end BUSMux_4_1;

architecture Behavioral of BUSMux_4_1 is
begin
	process(sel, din_0, din_1, din_2, din_3)
	begin
		case sel is
			when "00" => dout <= din_0;
			when "01" => dout <= din_1;
			when "10" => dout <= din_2;
			when "11" => dout <= din_3; 
			when others => dout <= (others => 'X');
		end case; 
	end process;
end Behavioral;

-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  15/11/2014 
-- Module Name:  NCounter.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Contatore generico ad N bit, che conta per incrementi in modulo
--               di 2^N
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity NCounter is
	generic ( N : natural := 8 );
	port ( clk, reset : in std_logic;   
		   cout : out std_logic_vector((N-1) downto 0)
	); 
end NCounter;

architecture Behavioral of NCounter is
	signal tmp_cnt : std_logic_vector((N-1) downto 0) := (others => '0');
begin
	process(clk, reset)
	begin
		if (reset='1') then
			tmp_cnt <= (others => '0');
		elsif (clk'event and clk='1') then
			tmp_cnt <= tmp_cnt + 1;
		end if;
	end process;
	cout <= tmp_cnt;
end Behavioral;

-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: SevSegment Driver
-- Create Date:  15/11/2014 
-- Module Name:  SEGTranscoder.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Transcoder per display 7 Segmenti a logica attiva bassa
--
-- Dependencies: none
-------------------------------------------------------------------------------
-- Common cathodes - negative logics ( '0'=> on, '1' => off )
--         a
--       =====
--    f || g || b
--       =====
--    e ||   || c
--       ===== .dp
--         d


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SEGTranscoder is
	port ( sel : in std_logic_vector(3 downto 0);		   
		   dp_in : in std_logic;
		   seg : out std_logic_vector(0 to 6); 
		   dp_out : out std_logic
    ); 
end SEGTranscoder;

architecture bhv_BCD of SEGTranscoder is
	signal dp_buff : std_logic;
begin
	dp_buff <= dp_in;
	
	decode:process(sel)
	begin
		case sel is ---------- abcdefg
		when "0000" => seg <= "0000001"; -- Digit 0
		when "0001" => seg <= "1001111"; -- Digit 1
		when "0010" => seg <= "0010010"; -- Digit 2
		when "0011" => seg <= "0000110"; -- Digit 3
		when "0100" => seg <= "1001100"; -- Digit 4
		when "0101" => seg <= "0100100"; -- Digit 5
		when "0110" => seg <= "0100000"; -- Digit 6
		when "0111" => seg <= "0001111"; -- Digit 7
		when "1000" => seg <= "0000000"; -- Digit 8
		when "1001" => seg <= "0000100"; -- Digit 9
		when others => seg <= "1111110"; -- Digit -
		end case; 
	end process;
	
	dp_out <= dp_buff;
end bhv_BCD;

architecture bhv_HEX of SEGTranscoder is
	signal dp_buff : std_logic;
begin
	dp_buff <= dp_in;
	
	decode:process(sel)
	begin
		case sel is ---------- abcdefg
		when "0000" => seg <= "0000001"; -- Digit 0
		when "0001" => seg <= "1001111"; -- Digit 1
		when "0010" => seg <= "0010010"; -- Digit 2
		when "0011" => seg <= "0000110"; -- Digit 3
		when "0100" => seg <= "1001100"; -- Digit 4
		when "0101" => seg <= "0100100"; -- Digit 5
		when "0110" => seg <= "0100000"; -- Digit 6
		when "0111" => seg <= "0001111"; -- Digit 7
		when "1000" => seg <= "0000000"; -- Digit 8
		when "1001" => seg <= "0000100"; -- Digit 9
		when "1010" => seg <= "0001000"; -- Digit A
		when "1011" => seg <= "1100000"; -- Digit b
		when "1100" => seg <= "0110001"; -- Digit C
		when "1101" => seg <= "1000010"; -- Digit d
		when "1110" => seg <= "0110000"; -- Digit E
		when "1111" => seg <= "0111000"; -- Digit F
		when others => seg <= "1111110"; -- Digit -
		end case; 
	end process;
	
	dp_out <= dp_buff;
end bhv_HEX;

-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: SevSegment Driver
-- Create Date:  15/11/2014 
-- Module Name:  SSDriver.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Driver per display a 7 segmenti a 4 cifre
--
-- Dependencies: SEGTranscoder.vhd, ANDecoder.vhd, BUSMux_4_1.vhd, NCounter.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SSDriver is
	port ( d3 : in std_logic_vector(3 downto 0);
		   d2 : in std_logic_vector(3 downto 0);
		   d1 : in std_logic_vector(3 downto 0);
		   d0 : in std_logic_vector(3 downto 0);
		   clk_ref: in std_logic;
		   seg : out std_logic_vector(0 to 6);
		   dp : out std_logic;
		   an : out std_logic_vector(3 downto 0)
	);
end SSDriver;

architecture Mixed of SSDriver is
	component SEGTranscoder
		port ( sel : in std_logic_vector(3 downto 0);		   
		       dp_in : in std_logic;
		       seg : out std_logic_vector(0 to 6); 
		       dp_out : out std_logic
		); 
	end component;

	component ANDecoder
		port ( sel : in std_logic_vector(1 downto 0);
		       an : out std_logic_vector(3 downto 0)
		);
	end component;

	component BUSMux_4_1
		generic ( N: natural := 4 );
		port (din_0 : in std_logic_vector((N-1) downto 0);   
			  din_1 : in std_logic_vector((N-1) downto 0); 
			  din_2 : in std_logic_vector((N-1) downto 0);
			  din_3 : in std_logic_vector((N-1) downto 0);
			  sel : in std_logic_vector(1 downto 0); 
			  dout : out std_logic_vector((N-1) downto 0)
		);
	end component;

	component NCounter
		generic ( N : natural := 2 );
		port ( clk, reset : in std_logic;   
			   cout : out std_logic_vector((N-1) downto 0)
		); 
	end component;

	-- Internal buffer for digit storage
	signal digit_3, digit_2, digit_1, digit_0 : std_logic_vector(3 downto 0) := (others => '0');
	-- Segnali di connessione
	signal sseg_data : std_logic_vector(3 downto 0);
	signal digit_sel : std_logic_vector(1 downto 0);
	
	for all: SEGTranscoder use entity work.SEGTranscoder(bhv_HEX);
	
begin
	-- Inserisco i segnali di ingresso
	digit_3 <= d3; digit_2 <= d2; digit_1 <= d1; digit_0 <= d0;
	
	bus_multiplexer: BUSMux_4_1 port map (digit_3, digit_2, digit_1, digit_0, digit_sel, sseg_data);
	SS_SEGTranscoder: SEGTranscoder port map (sseg_data, '1', seg, dp);
	SS_ANDecoder: ANDecoder port map (digit_sel, an);
	refresh_counter: NCounter port map (clk_ref, '0', digit_sel);

end Mixed;