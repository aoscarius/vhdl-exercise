-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: IR-RC5 Module
-- Create Date:  10/01/2015 
-- Module Name:  Bin2BCD.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione della top level architecture per il testing
--					  su board dei moduli di trasmissione ad infrarossi su protocollo
--					  RC5-Extended della Philips.
--
-- Dependencies: Nexys2_IRRC5.ucf, ASELibrary.vhd, SSDriver.vhd, IRRC5.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Nexys2_IRRC5 is
	port (clk: in std_logic;
			-- Switch
			add : in std_logic_vector (1 downto 0);
			cmd : in std_logic_vector (5 downto 0);
			-- Buttons
			btn_reset : in std_logic;
			btn_send : in std_logic;
			btn_rc5ex : in std_logic;
			-- Leds 
			led_tx : out std_logic;
			led_rx : out std_logic;
			led_nosig_rx : out std_logic;
			led_irq_rx : out std_logic;
			-- TX pin
			JC_ir_tx : out std_logic;
			JC_loop_tx : out std_logic;
			-- RX pin
			JC_rx : in std_logic;
			-- Seven Segment Display
			seg : out std_logic_vector(0 to 6);
			dp : out std_logic;
			an : out std_logic_vector(3 downto 0)
	);
end Nexys2_IRRC5;

architecture Behavioral of Nexys2_IRRC5 is
	
	component RC5_Tx is
		port (sys_clk : in std_logic;							 -- Clock di sistema
				reset   : in std_logic;							 -- Reset di sistema
				send    : in std_logic;							 -- Finch� � attivo invia la trasmissione
				address : in std_logic_vector(4 downto 0); -- Indirizzo del dispositivo 
				command : in std_logic_vector(6 downto 0); -- Comando per il dispositivo
				ir_led_out : out std_logic;					 -- Direttamente collegabile ad un led ad infrarossi
				ir_mod_out : out std_logic);               -- Direttamente collegabile ad un modulo tx ad infrarossi
	end component;
	
	signal address_tx : std_logic_vector(4 downto 0);
	signal command_tx : std_logic_vector(6 downto 0);
	signal ir_tx : std_logic := '0';

	component RC5_Rx is
		port (sys_clk   : in std_logic;							  -- Clock di sistema
				reset		 : in std_logic;							  -- Resetta il sistema e lo prepara ad una nuova ricezione
				ir_mod_in : in std_logic;               		  -- Direttamente collegabile ad un modulo rx ad infrarossi
				no_signal : out std_logic;							  -- Segnale di errore sul mezzo trasmissivo
				irq_ir	 : out std_logic;							  -- Richiesta di interrupt: se � attiva � disponibile una nuova ricezione
				address   : out std_logic_vector(4 downto 0);  -- Indirizzo del dispositivo 
				command   : out std_logic_vector(6 downto 0)); -- Comando per il dispositivo
	end component;

	signal address_rx : std_logic_vector(4 downto 0);
	signal command_rx : std_logic_vector(6 downto 0);
	signal ir_rx : std_logic := '0';

---------------------------------------------------------------------------
-- Driver per il Display a 7 Segmenti della board Nexys 2
---------------------------------------------------------------------------
	component clkGen is
			generic ( FHzIn : integer := 50000000; FHzOut : integer := 60000);
			port ( clk_in : in  STD_LOGIC;
					clk_out: out STD_LOGIC
			 );
	end component;
	
	component SSDriver is
		port ( d3 : in std_logic_vector(3 downto 0);
				 d2 : in std_logic_vector(3 downto 0);
				 d1 : in std_logic_vector(3 downto 0);
				 d0 : in std_logic_vector(3 downto 0);
				 clk_ref: in std_logic;
				 seg : out std_logic_vector(0 to 6);
				 dp : out std_logic;
				 an : out std_logic_vector(3 downto 0)
		);
	end component;

	-- Segnali per il display a 7 Segmenti
	signal clk_gen_refresh : std_logic := '0';
	signal digits_210_out : std_logic_vector(11 downto 0) := (others => '0');
	signal digit_3 : std_logic_vector(3 downto 0) := (others => '0');
   alias digit_2 is digits_210_out(11 downto 8);
	alias digit_1 is digits_210_out(7 downto 4);
	alias digit_0 is digits_210_out(3 downto 0);
---------------------------------------------------------------------------

	component Bin2BCD is
		port ( 
			binary_in : in  std_logic_vector (7 downto 0);
			bcd_out   : out std_logic_vector (11 downto 0)
		);
	end component;

	signal digits_210_in : std_logic_vector(7 downto 0) := (others => '0');

begin

	decData: Bin2BCD port map( 
		binary_in => digits_210_in,
		bcd_out => digits_210_out
	);

---------------------------------------------------------------------------
-- Driver per il Display a 7 Segmenti della board Nexys 2
---------------------------------------------------------------------------
	clk_refresh: clkGen generic map (FHzOut=> 400) port map (clk, clk_gen_refresh);
	SSDisplay : SSDriver port map (digit_3, digit_2, digit_1, digit_0, clk_gen_refresh, seg, dp, an);
---------------------------------------------------------------------------

---------------------------------------------------------------------------
-- IR-RC5 RX Module Instance
---------------------------------------------------------------------------
	address_tx <= "000" & add;
	command_tx <= btn_rc5ex & cmd;
	
	led_tx <= ir_tx;
	JC_ir_tx <= ir_tx;

	tx_inst: RC5_Tx port map(
		sys_clk => clk,						
		reset => btn_reset,
		send => btn_send,				
		address => address_tx,
		command => command_tx,
		ir_led_out	=> ir_tx,
		ir_mod_out => JC_loop_tx -- Usato per il loop
	);
---------------------------------------------------------------------------

---------------------------------------------------------------------------
-- IR-RC5 RX Module Instance
---------------------------------------------------------------------------
	ir_rx <= JC_rx;
	led_rx <= not ir_rx;

	rx_inst: RC5_Rx port map(
		sys_clk => clk,
		reset => btn_reset,
		ir_mod_in => ir_rx,
		no_signal => led_nosig_rx,
		irq_ir => led_irq_rx,
		address => address_rx,
		command => command_rx
	);
---------------------------------------------------------------------------

	digit_3 <= "00" & address_rx(1 downto 0);
	digits_210_in <= '0' & command_rx;

end Behavioral;

