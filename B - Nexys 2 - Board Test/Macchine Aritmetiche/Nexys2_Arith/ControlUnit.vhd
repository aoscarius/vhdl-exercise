-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Macchine Aritmetiche
-- Create Date:  22/01/2015 
-- Module Name:  ControlUnit.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Control Unit per il test su board delle macchine aritmetiche
--
-- Dependencies: none
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity ControlUnit is
	port(	clk : in  std_logic;
			reset : in  std_logic;
			sig_x_in : in  std_logic;		-- Segnale di memorizzazione di X
			sig_y_in : in  std_logic;		-- Segnale di memorizzazione di Y
			sig_res : in  std_logic;		-- Segnale di scelta del risultato
			x_in_en : out  std_logic;		-- Abilita la memorizzazione di X
			x_loaded : out  std_logic;		-- Segnala che X � stato memorizzato
			y_in_en : out  std_logic;		-- Abilita la memorizzazione di Y
			y_loaded : out  std_logic;		-- Segnala che Y � stato memorizzato
			sel_disp : out std_logic;		-- Segnale di controllo MuxDisplay
			op_disp : out std_logic_vector(1 downto 0);	-- //    MuxRisultato
			nadd_sub : out  std_logic;		-- Abilitazione somma o sottrazione
			mul_start : out  std_logic;	-- Segnale avvio moltiplicazione
			mul_stop : in  std_logic;		-- Segnale moltiplicazione pronta
			div_start : out  std_logic;	-- Segnale avvio divisione
			div_stop : in  std_logic		-- Segnale divisione pronta
	);
end ControlUnit;


architecture behavioral of ControlUnit is

type state_type is (s_reset, s_x_load, s_y_load, s_md_start, s_md_pause, s_md_stop, s_res_press, s_res_release);
signal state: state_type := s_reset;

-- Shif Register circolare interno per la selezione dell'operazione da eseguire
signal op_selected: std_logic_vector(3 downto 0) := (others => '0');

begin

	fsm_main: process (clk, reset, state, sig_x_in, sig_y_in, sig_res, mul_stop, div_stop) -- gestisce la fsm
	begin
		if reset='1' then
			
			state <= s_reset;
			
		elsif (clk'event and clk='1') then
			
			-- Reset ciclici
			x_in_en <= '0';
			y_in_en <= '0';
			mul_start <= '0';
			div_start <= '0';
			
			case state is
				
				-- Stato iniziale di reset della macchina in cui la macchina si prepara
				-- alla memorizzazione di nuovi dati e a nuove operazioni aritmetiche su essi
				when s_reset => 	
						x_loaded <= '0';
						y_loaded <= '0';
						sel_disp <= '0';
						nadd_sub <= '0';
						
						-- Inizializzo lo Shift Register circolare con un 1 in testa (segnale di add attivo)
						op_selected <= "1000";
						op_disp <= "00";
							
						state <= s_x_load;
				
				--	Stato di attesa del segnale di memorizzazione di X e relativa abilitazione
				-- delle linee di controllo per la memorizzazione dell'operando
				when s_x_load => 
						if (sig_x_in='1') then
							x_in_en <= '1';
							x_loaded <= '1';
						
							state <= s_y_load;
						end if;

				--	Stato di attesa del segnale di memorizzazione di Y e relativa abilitazione
				-- delle linee di controllo per la memorizzazione dell'operando
				when s_y_load =>
						if (sig_y_in='1') then
							y_in_en <= '1';
							y_loaded <= '1';
						
							state <= s_md_start;
						end if;

				--	Stato di abilitazione dei segnali di avvio delle macchine moltiplicatrici 
				-- e divisorie che richiedono un certo numero di cicli macchina per poter
				-- visualizzare un risultato corretto
				when s_md_start =>
						mul_start <= '1';
						div_start <= '1';
				
						state <= s_md_pause;
						
				--	Stato di salto di un ciclo macchina per allineare la control unit all'inizio
				-- operazioni da parte delle macchine moltiplicatrici e divisorie (patch di ritardo)
				when s_md_pause =>
						state <= s_md_stop;

				--	Stato di attesa del segnale di stop delle macchine moltiplicatrici e divisorie
				-- per abilitare la funzione di visualizzazione dei risultati
				when s_md_stop =>
						if (mul_stop='1' and div_stop='1') then
							state <= s_res_press;
						end if;	
						
				--	Stage 1 dello stato di controllo del segnale di switch tra i risultati
				-- All'abilitazione del segnale viene effettuato lo switch e la macchina passa allo 
				-- Stage 2 in cui attende la disabilitazione del segnale prima del prossimo switch
				when s_res_press =>
						sel_disp <= '1';
						
						nadd_sub <= op_selected(2);
												
						case op_selected is
							when "1000" => op_disp <= "00";
							when "0100" => op_disp <= "01";
							when "0010" => op_disp <= "10";
							when "0001" => op_disp <= "11";
							when others => op_disp <= "00";
						end case;
						
						if (sig_res='1') then
							-- Lo shift register circolare fa girare il segnale di abilitazione
							-- tra le 4 linne disponibili cos� da scegliere l'operazione da visualizzare
							-- In effetti si comporta con un contatore ed un decoder messi insieme
							op_selected <= op_selected(0) & op_selected(3 downto 1);
							state <= s_res_release;
						end if;
										
				--	Stage 2 dello stato di controllo del segnale di switch tra i risultati
				-- Attraverso questo stato � possibile identificare i fronti del segnale di controllo
				when s_res_release => 
						if (sig_res='0') then
							state <= s_res_press;
						end if;

				when others => 	
							state <= s_reset;
					
				end case;
				
			end if;
	end process;

end behavioral;