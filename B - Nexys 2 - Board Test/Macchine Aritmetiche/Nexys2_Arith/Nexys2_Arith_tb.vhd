LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY Nexys2_Arith_tb IS
END Nexys2_Arith_tb;
 
ARCHITECTURE behavior OF Nexys2_Arith_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Nexys2_Arith
    PORT(
         clk : IN  std_logic;
         operand : IN  std_logic_vector(7 downto 0);
         btn_x_in : IN  std_logic;
         btn_y_in : IN  std_logic;
         btn_reset : IN  std_logic;
         btn_result : IN  std_logic;
         led_operation : OUT  std_logic_vector(3 downto 0);
         led_x_loaded : OUT  std_logic;
         led_y_loaded : OUT  std_logic;
         seg : OUT  std_logic_vector(0 to 6);
         dp : OUT  std_logic;
         an : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal operand : std_logic_vector(7 downto 0) := (others => '0');
   signal btn_x_in : std_logic := '0';
   signal btn_y_in : std_logic := '0';
   signal btn_reset : std_logic := '0';
   signal btn_result : std_logic := '0';

 	--Outputs
   signal led_operation : std_logic_vector(3 downto 0);
   signal led_x_loaded : std_logic;
   signal led_y_loaded : std_logic;
   signal seg : std_logic_vector(0 to 6);
   signal dp : std_logic;
   signal an : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Nexys2_Arith PORT MAP (
          clk => clk,
          operand => operand,
          btn_x_in => btn_x_in,
          btn_y_in => btn_y_in,
          btn_reset => btn_reset,
          btn_result => btn_result,
          led_operation => led_operation,
          led_x_loaded => led_x_loaded,
          led_y_loaded => led_y_loaded,
          seg => seg,
          dp => dp,
          an => an
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		btn_reset <= '1';
      wait for 100 ns;	
		btn_reset <= '0';

      wait for clk_period*10;

		-- Carico X
		operand <= "00001110";
		btn_x_in <= '1';
      wait for clk_period*10;
		btn_x_in <= '0';
 		
		-- Carico Y
		operand <= "00000111";
		btn_y_in <= '1';
      wait for clk_period*10;
		btn_y_in <= '0';

		wait for clk_period*10;
		
		wait for clk_period*10;
		btn_result <= '1';
		wait for clk_period;
		btn_result <= '0';

		wait for clk_period*10;
		btn_result <= '1';
		wait for clk_period;
		btn_result <= '0';

		wait for clk_period*10;
		btn_result <= '1';
		wait for clk_period;
		btn_result <= '0';

		wait for clk_period*10;
		btn_result <= '1';
		wait for clk_period;
		btn_result <= '0';

		wait for clk_period*10;
		btn_result <= '1';
		wait for clk_period;
		btn_result <= '0';
		
		wait for clk_period*10;
		btn_result <= '1';
		wait for clk_period;
		btn_result <= '0';
		
		wait for clk_period*10;

   end process;

END;
