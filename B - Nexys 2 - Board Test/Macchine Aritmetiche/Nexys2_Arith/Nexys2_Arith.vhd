-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Macchine Aritmetiche
-- Create Date:  22/01/2015 
-- Module Name:  Nexys2_Arith.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Test su board delle macchine aritmetiche
--
-- Dependencies: Nexys2_Arith.ucf, ASELibrary.vhd, ArithLib.vhd, SSDriver.vhd, ControlUnit.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Nexys2_Arith is
	port (clk: in std_logic;
			-- Switch
			operand : in std_logic_vector (7 downto 0);
			-- Buttons
			btn_x_in : in std_logic;
			btn_y_in : in std_logic;
			btn_reset : in std_logic;
			btn_result : in std_logic;
			-- Leds 
			led_operation : out std_logic_vector (3 downto 0);
			led_x_loaded : out std_logic;
			led_y_loaded : out std_logic;
			-- Seven Segment Display
			seg : out std_logic_vector(0 to 6);
			dp : out std_logic;
			an : out std_logic_vector(3 downto 0)
	);
end Nexys2_Arith;

architecture Behavioral of Nexys2_Arith is

---------------------------------------------------------------
-- Macchine Aritmetiche
---------------------------------------------------------------
	-- Sommatore\Sottrattore: Ripple Carry
	component RCA is
		generic ( N: natural := 8 );
		port ( x_in, y_in: in  std_logic_vector ((N-1) downto 0);
				 c_in : in std_logic;
				 nadd_sub: in std_logic;
				 sum : out  std_logic_vector ((N-1) downto 0);
				 overflow : out std_logic; 
				 c_out : out std_logic);
	end component;

	-- Moltiplicatore: Booth
	component Booth is
		Generic ( bits : integer := 8 );
		Port (clk : in  STD_LOGIC;
				reset : in  STD_LOGIC;
				start : in STD_LOGIC;
				x : in  STD_LOGIC_VECTOR ((bits-1) downto 0);
				y : in  STD_LOGIC_VECTOR ((bits-1) downto 0);
				stop : out STD_LOGIC;
				p_out : out STD_LOGIC_VECTOR (((2*bits)-1) downto 0));
	end component;

	-- Divisore: Restoring
	component RDivider is
		Generic (bits : integer := 8;
					log_bits : integer := 3);
		Port (clk : in  STD_LOGIC;
				reset : in  STD_LOGIC;
				start : in STD_LOGIC;
				divider : in  STD_LOGIC_VECTOR (((2*bits)-1-1) downto 0);
				divisor : in  STD_LOGIC_VECTOR ((bits-1) downto 0);
				stop : out STD_LOGIC;
			   reminder : out STD_LOGIC_VECTOR ((bits-1) downto 0); 
				quotient : out STD_LOGIC_VECTOR ((bits-1) downto 0));
	end component;
---------------------------------------------------------------

---------------------------------------------------------------
-- Interfaccia di controllo delle macchine
---------------------------------------------------------------
	-- Registri di memorizzazione dati
	component DFFRegister is
		generic ( N : natural := 8 );
		port ( clk, reset, en: in std_logic;
				d : in std_logic_vector(N-1 downto 0);
				q : out std_logic_vector(N-1 downto 0)
		);
	end component ;
	
	-- BUSMux per la selezione del risultato
	component BUSMux_4_1 is
		generic ( N: natural := 16 );
		port (din_0 : in std_logic_vector((N-1) downto 0);   
			  din_1 : in std_logic_vector((N-1) downto 0); 
			  din_2 : in std_logic_vector((N-1) downto 0);
			  din_3 : in std_logic_vector((N-1) downto 0);
			  sel : in std_logic_vector(1 downto 0); 
			  dout : out std_logic_vector((N-1) downto 0)
		);
	end component;

	-- ControlUnit per il controllo della scheda
	component ControlUnit is
		port(	clk : in  std_logic;
				reset : in  std_logic;
				sig_x_in : in  std_logic;		-- Segnale di memorizzazione di X
				sig_y_in : in  std_logic;		-- Segnale di memorizzazione di Y
				sig_res : in  std_logic;		-- Segnale di scelta del risultato
				x_in_en : out  std_logic;		-- Abilita la memorizzazione di X
				x_loaded : out  std_logic;		-- Segnala che X � stato memorizzato
				y_in_en : out  std_logic;		-- Abilita la memorizzazione di Y
				y_loaded : out  std_logic;		-- Segnala che Y � stato memorizzato
				sel_disp : out std_logic;		-- Segnale di controllo MuxDisplay
				op_disp : out std_logic_vector(1 downto 0);	-- //    MuxRisultato
				nadd_sub : out  std_logic;		-- Abilitazione somma o sottrazione
				mul_start : out  std_logic;	-- Segnale avvio moltiplicazione
				mul_stop : in  std_logic;		-- Segnale moltiplicazione pronta
				div_start : out  std_logic;	-- Segnale avvio divisione
				div_stop : in  std_logic		-- Segnale divisione pronta
		);
	end component;
---------------------------------------------------------------

---------------------------------------------------------------------------
-- Mux di supporto al display per la visualizzazione ingressi, risultato
---------------------------------------------------------------------------
	component BUSMux_2_1 is
		generic ( N: natural := 16 );
		port (din_0 : in std_logic_vector((N-1) downto 0);   
			   din_1 : in std_logic_vector((N-1) downto 0); 
			   sel : in std_logic; 
			   dout : out std_logic_vector((N-1) downto 0)
		);
	end component;

	signal disp_select : std_logic := '0';
	signal disp_buffer_op : std_logic_vector(15 downto 0) := (others => '0');
	signal disp_buffer_res : std_logic_vector(15 downto 0) := (others => '0');
	
---------------------------------------------------------------------------
-- Driver per il Display a 7 Segmenti della board Nexys 2
---------------------------------------------------------------------------
	component clkGen is
			generic ( FHzIn : integer := 50000000; FHzOut : integer := 60000);
			port ( clk_in : in  STD_LOGIC;
					clk_out: out STD_LOGIC
			 );
	end component;
	
	component SSDriver is
		port ( d3 : in std_logic_vector(3 downto 0);
				 d2 : in std_logic_vector(3 downto 0);
				 d1 : in std_logic_vector(3 downto 0);
				 d0 : in std_logic_vector(3 downto 0);
				 clk_ref: in std_logic;
				 seg : out std_logic_vector(0 to 6);
				 dp : out std_logic;
				 an : out std_logic_vector(3 downto 0)
		);
	end component;

	-- Segnali per il display a 7 Segmenti
	signal clk_gen_refresh : std_logic := '0';
	signal digits_out : std_logic_vector(15 downto 0) := (others => '0');
	alias digit_3 is digits_out(15 downto 12);
   alias digit_2 is digits_out(11 downto 8);
	alias digit_1 is digits_out(7 downto 4);
	alias digit_0 is digits_out(3 downto 0);
---------------------------------------------------------------------------
		
---------------------------------------------------------------------------
-- Segnali di controllo
---------------------------------------------------------------------------
	signal reset : std_logic := '0';
	
	-- Segnali dei registri per gli operandi
	signal num_x_out, num_y_out : std_logic_vector(7 downto 0) := (others => '0');

	-- Segnali somma-sottrazione
	signal result_ss : std_logic_vector(15 downto 0) := (others => '0');
	-- Segnali moltiplicazione
	signal result_mul : std_logic_vector(15 downto 0) := (others => '0');
	-- Segnali divisione
	signal divider_temp : std_logic_vector(14 downto 0) := (others => '0');
	signal result_div : std_logic_vector(15 downto 0) := (others => '0');
	-- Display (0) Somma (1) Sottrazione (2) Moltiplicazione (3) Divisione
	signal operation_display : std_logic_vector(1 downto 0) := (others => '0');
	
	-- Segnali della control unit
	signal cu_x_in_en, cu_y_in_en : std_logic := '0';
	signal cu_nadd_sub, cu_mul_start, cu_mul_stop, cu_div_start, cu_div_stop : std_logic := '0';
---------------------------------------------------------------------------

begin

	reset <= btn_reset;
	
	-- Processo per la segnalazione su led dell'operazione visualizzata
	mux_opdisp_prox: process (clk, reset)
	begin
		if (reset='1') then
			led_operation <= "0000";
		elsif (clk'event and clk='1') then
			case operation_display is
				when "00" => led_operation <= "1000";
				when "01" => led_operation <= "0100";
				when "10" => led_operation <= "0010";
				when "11" => led_operation <= "0001";
				when others => led_operation <= "0000";
			end case;
		end if;
	end process;
	
---------------------------------------------------------------------------
-- Mux per la visualizzazione su display tra operandi e risultato
---------------------------------------------------------------------------
	disp_buffer_op <= "00000000" & operand;	
	disp_mux: BUSMux_2_1 port map (
			din_0 => disp_buffer_op, 
			din_1 => disp_buffer_res, 
			sel => disp_select, 
			dout => digits_out 
	);
---------------------------------------------------------------------------
	
---------------------------------------------------------------------------
-- Driver per il Display a 7 Segmenti della board Nexys 2
---------------------------------------------------------------------------
	clk_refresh: clkGen generic map (FHzOut=> 400) port map (clk, clk_gen_refresh);
	SSDisplay : SSDriver port map (digit_3, digit_2, digit_1, digit_0, clk_gen_refresh, seg, dp, an);
---------------------------------------------------------------------------
	
	opXIN: DFFRegister port map(clk, reset, cu_x_in_en, operand, num_x_out);
	opYIN: DFFRegister port map(clk, reset, cu_y_in_en, operand, num_y_out);

	result_ss(15 downto 8) <= (others => '0');
	-- Somma-Sottrazione: RCA
	RCA_SS_inst: RCA port Map ( 
			x_in => num_x_out,
			y_in => num_y_out,
			c_in => '0',
			nadd_sub => cu_nadd_sub,
			sum => result_ss(7 downto 0),
			overflow => open,
			c_out => open
	);

	-- Moltiplicatore: Booth
	BOOTH_inst: Booth Port Map (
			clk => clk, 
			reset => reset, 
			start => cu_mul_start, 
			x => num_x_out, 
			y => num_y_out, 
			stop => cu_mul_stop, 
			p_out => result_mul 
	);

	-- Divisore: Restoring
	divider_temp <= "0000000" & num_x_out;
	RDIV_inst: RDivider Port Map (
		clk => clk,
		reset => reset,
		start => cu_div_start,
		divider => divider_temp, 
		divisor => num_y_out, 
		stop => cu_div_stop,
		reminder => result_div(7 downto 0), 
		quotient => result_div(15 downto 8) 
	);


	CUInst: ControlUnit port map(
		clk => clk,
		reset => reset,
		sig_x_in => btn_x_in,
		sig_y_in => btn_y_in,
		sig_res => btn_result,
		x_in_en => cu_x_in_en,
		x_loaded => led_x_loaded, 
		y_in_en => cu_y_in_en,
		y_loaded => led_y_loaded, 
		sel_disp => disp_select,
		op_disp => operation_display,
		nadd_sub => cu_nadd_sub,
		mul_start => cu_mul_start,
		mul_stop => cu_mul_stop ,
		div_start => cu_div_start,
		div_stop => cu_div_stop

	);
	
	-- Mux per la selezione su display del tipo di risultato
	result_mux: BUSMux_4_1 port map (
			din_0 => result_ss, 
			din_1 => result_ss, 
			din_2 => result_mul, 
			din_3 => result_div, 
			sel => operation_display, 
			dout => disp_buffer_res 
	);
	
end Behavioral;

