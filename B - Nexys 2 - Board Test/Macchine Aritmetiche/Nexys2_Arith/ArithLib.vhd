-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Generic Adder
-- Create Date:  24/11/2014
-- Module Name:  FA.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione di un Full Adder
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FA is
	port ( a, b, cin: in std_logic;
		   sum, cout: out std_logic
	);
end FA;

architecture Dataflow of FA is
begin
	sum <= a xor b xor cin;
	cout <= (a and b) or (cin and a) or (cin and b);
end Dataflow;

-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Generic Adder
-- Create Date:  27/11/2014
-- Module Name:  RPC.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Implementazione di un Ripple Carry con cella 
--               elementare di tipo Full Adder.
--
-- Dependencies: FA.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity RCA is
	generic ( N: natural := 8 );
    port ( x_in, y_in: in  std_logic_vector ((N-1) downto 0);
		   c_in : in std_logic;
		   nadd_sub: in std_logic;
           sum : out  std_logic_vector ((N-1) downto 0);
		   overflow : out std_logic; 
		   c_out : out std_logic);
end  RCA;

architecture Structural of  RCA is
	component FA
		port ( a, b, cin: in std_logic;
			   sum, cout: out std_logic
		);
	end component;
	
	-- Vettore di segnali di propagazione del carry
	signal c_vect : std_logic_vector(N downto 0);
	
	-- Segnali per la rete di complementazione ----------------------------------
	signal yc_in: STD_LOGIC_VECTOR ((N-1) downto 0) := (others => '0');
	signal cc_in: STD_LOGIC := '0';
	constant xones: STD_LOGIC_VECTOR ((N-1) downto 0) := (others => '1');
	-----------------------------------------------------------------------------
	
begin
	-- Rete di complementazione a 2 di b ------------------
	yc_in <= (y_in xor xones) when (nadd_sub='1') else y_in;
	cc_in <= (not c_in) when (nadd_sub='1') else c_in;
	-------------------------------------------------------

	c_vect(0) <= cc_in;
	
	-- Genero la catena di Full Adder in cascata
	rpc_gen: for i in (N-1) downto 0 generate 
		facell: FA port map(x_in(i), yc_in(i), c_vect(i), sum(i), c_vect(i+1));
	end generate;
	
	-- Calcolo dell'overflow
	overflow <= c_vect(N) xor c_vect(N-1);
	-- E riporto in uscita l'ultimo carry della cascata
	c_out <= c_vect(N);
	
end Structural;

-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  26/12/2014 
-- Module Name:  RSRegister.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Registro ad N bit con funzione di shift a destra
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity RSRegister is
	generic(N: integer := 8);
	port(	clk, load, reset, rshift: in std_logic;
			d_in: in std_logic;
			data_in: in std_logic_vector((N-1) downto 0);
			data_out: out std_logic_vector((N-1) downto 0)			
	);
end RSRegister;

architecture Behavioral of RSRegister is
	signal data: std_logic_vector((N-1) downto 0) := (others => '0');
begin
	process(clk, reset)
	begin
		if (reset = '1') then
			data <= (others => '0');
		elsif (clk'event and clk='1') then
			if (load = '1') then
				data <= data_in;
			elsif (rshift = '1') then
			   data <= d_in & data((N-1) downto 1);
			end if;
		end if; 
	end process;

	data_out <= data;
	
end Behavioral;

-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  15/11/2014 
-- Module Name:  HitCounter.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Contatore generico modulo N, con full range hit
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity HitCounter is
	generic (N : natural := 8);
	port ( clk, reset, count_inc: in std_logic;
		   count_hit: out std_logic  
	); 
end HitCounter;

architecture Behavioral of HitCounter is
	signal reg_count : natural range 0 to N := 0;
begin
	process(clk, reset, reg_count)
	begin
		if (reset='1') then
			reg_count <= 0;
		elsif (clk'event and clk='1') then
			if (count_inc='1') then
				reg_count <= reg_count + 1;
			end if;
		end if;	
		if (reg_count<N) then
			count_hit <= '0';
		else
			count_hit <= '1';
		end if;
	end process;
end Behavioral;

-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Booth Multiplier
-- Create Date:  31/12/2014 
-- Module Name:  ControlUnit.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Unit� di controllo per il moltiplicatore Booth. Questo modulo
--               implementa una macchina a stati finiti che esegue l'algoritmo di 
--               moltiplicazione di Booth per operandi interi relativi.
--
-- Dependencies: none
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity BControlUnit is
	port(	clk : in  std_logic;
			reset : in  std_logic;
			start : in std_logic;
			q_lsb : in std_logic_vector(1 downto 0);	-- q[0]q[-1]
         count_hit : in std_logic;						-- conteggio terminato
			nadd_sub : out  std_logic;						-- somma(0) sottrazione(1)
			a_reset : out  std_logic;						-- reset registro A
			q_reset : out  std_logic;						-- reset registro Q
			m_reset : out  std_logic;						-- reset registro M
			c_reset : out  std_logic;						-- reset contatore
			a_load : out  std_logic;						-- caricamento registro A
			q_load : out  std_logic;						-- caricamento registro Q
			m_load : out  std_logic;						-- caricamento registro M
			shift_en : out  std_logic;						-- abilitazione shift A e Q
			count_inc : out  std_logic;					-- incremento conteggio
			stop : out std_logic								-- sistema fermo
	);
end BControlUnit;


architecture behavioral of BControlUnit is

type state_type is (s_idle, s_init, s_scan, s_shift, s_end);
signal s_current, s_next: state_type := s_idle;

begin

	next_state: process(clk, reset)
	begin
		if reset='1' then
			s_current <= s_idle;
		elsif (clk'event and clk='1') then
			s_current <= s_next;
		end if;
	end process;


	fsm_main: process (start, s_current, count_hit, q_lsb) 		-- gestisce la fsm
	begin

		-- reset generico ad ogni attivazione del process
		stop <= '0';			
		nadd_sub <= '0';
		a_reset <= '0';
		q_reset <= '0';
		m_reset <= '0';
		c_reset <= '0';
		a_load <= '0';
      q_load <= '0';
      m_load <= '0';
		shift_en <= '0';
		count_inc <= '0';
				
		case s_current is

			-- stato di quiete, il sistema non fa niente	
			when s_idle => 	stop <= '1';				

									if start = '1' then
										s_next <= s_init;
									else 
										s_next <= s_idle;
									end if;
									
			-- stato di inizializzazione, il sistema si prepara a memorizzare gli operandi
			when s_init => 	a_reset <= '1';  -- BEGIN
									c_reset <= '1';
									
									q_load <= '1';	--INPUT
									m_load <= '1';
									
									s_next <= s_scan;
									
			-- stato di scan che attiva la somma la sottrazione o la somma con zero					
			when s_scan => 	if q_lsb = "01" then
										a_load <='1';
										nadd_sub <= '0';
									elsif q_lsb = "10" then 
										a_load <='1';
										nadd_sub <= '1';
									end if;
									
									count_inc <= '1';
									
									s_next <= s_shift;
			
			-- stato di shift a destra di a e q
			when s_shift => 	shift_en <='1';
																		
									if count_hit = '1' then
										s_next <= s_end;
									else 	
										s_next <= s_scan;
									end if;
									
				
			-- stato finale, il sistema si ferma e restituisce il prodotto finale
			when s_end =>		stop <= '1';
			
									if start ='0' then
										s_next <= s_idle;
									else 
										s_next <= s_end;
									end if;
									
			
			when others => 	s_next <= s_idle;
				
			end case;
	
		
	end process;

end behavioral;

-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Booth Multiplier
-- Create Date:  31/12/2014 
-- Module Name:  Robertson.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Moltiplicatore di Booth che essegue la moltiplicazione
--               tra operandi interi relativi ad N bit.
--
-- Dependencies: RSRegister.vhd, DFF.vhd, BUSMux_2_1.vhd, Mux_2_1.vhd, RCA.vhd,
--				 FA.vhd, NCounter.vhd, ControlUnit.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Booth is
	Generic ( bits : integer := 8 );
	Port (clk : in  STD_LOGIC;
		   reset : in  STD_LOGIC;
		   start : in STD_LOGIC;
         x : in  STD_LOGIC_VECTOR ((bits-1) downto 0);
         y : in  STD_LOGIC_VECTOR ((bits-1) downto 0);
		   stop : out STD_LOGIC;
         p_out : out STD_LOGIC_VECTOR (((2*bits)-1) downto 0));
end Booth;

architecture Structural of Booth is

	component RCA is
		generic ( N: natural := bits );
		port ( x_in, y_in: in  std_logic_vector ((N-1) downto 0);
				 c_in : in std_logic;
				 nadd_sub: in std_logic;
				 sum : out  std_logic_vector ((N-1) downto 0);
				 overflow : out std_logic; 
				 c_out : out std_logic);
	end component;

	component RSRegister is
		generic(N: integer := bits);
		port(	clk, load, reset, rshift: in std_logic;
				d_in: in std_logic;
				data_in: in std_logic_vector((N-1) downto 0);
				data_out: out std_logic_vector((N-1) downto 0)			
		);
	end component;

	component BUSMux_2_1 is
		generic ( N: natural := (bits-1));
		port ( din_0 : in std_logic_vector((N-1) downto 0);   
			    din_1 : in std_logic_vector((N-1) downto 0); 
			    sel : in std_logic; 
			    dout : out std_logic_vector((N-1) downto 0)
		);
	end component;

	component HitCounter is
		generic ( N : natural := bits );
		port ( clk, reset, count_inc : in std_logic;   
				 count_hit: out std_logic
		); 
	end component;

	component BControlUnit is
		port(	clk : in  std_logic;
				reset : in  std_logic;
				start : in std_logic;
				q_lsb : in std_logic_vector(1 downto 0);	-- q[0]q[-1]
				count_hit : in std_logic;						-- conteggio terminato
				nadd_sub : out  std_logic;						-- somma(0) sottrazione(1)
				a_reset : out  std_logic;						-- reset registro A
				q_reset : out  std_logic;						-- reset registro Q
				m_reset : out  std_logic;						-- reset registro M
				c_reset : out  std_logic;						-- reset contatore
				a_load : out  std_logic;						-- caricamento registro A
				q_load : out  std_logic;						-- caricamento registro Q
				m_load : out  std_logic;						-- caricamento registro M
				shift_en : out  std_logic;						-- abilitazione shift A e Q
				count_inc : out  std_logic;					-- incremento conteggio
				stop : out std_logic								-- sistema fermo
		);
	end component;

	signal x_extended, Q_Out : std_logic_vector(bits downto 0) := (others => '0');
	signal A_Out, M_Out : std_logic_vector((bits-1) downto 0) := (others => '0');
	signal sum_out : std_logic_vector((bits-1) downto 0) := (others => '0');
	
	signal cu_nadd_sub : std_logic;
	signal cu_a_reset, cu_q_reset, cu_m_reset, cu_c_reset : std_logic;
	signal cu_a_load, cu_q_load, cu_m_load: std_logic;
	signal cu_shift_en : std_logic;	
	signal cu_count_inc, cu_count_hit : std_logic;


begin

	-- L'ultimo bit[0] rappresenta il bit[-1]
	x_extended(0) <= '0'; 
	x_extended(bits downto 1) <= x; -- Composizione del valore in ingresso a Q 
	p_out <= A_Out & Q_Out(bits downto 1);	-- Composizione del prodotto finale 

	A: RSRegister -- Registro A
		port map(
			clk => clk,
			load => cu_a_load,
			reset => cu_a_reset,
			rshift => cu_shift_en,
			d_in => A_Out(bits-1),
			data_in => sum_out,
			data_out => A_Out			
		);

	Q: RSRegister -- Registro Q
		generic map ( N => (bits+1) )
		port map(
			clk => clk,
			load => cu_q_load,
			reset => cu_q_reset,
			rshift => cu_shift_en,
			d_in => A_Out(0),
			data_in => x_extended,
			data_out => Q_Out			
		);
				
	M: RSRegister -- Registro M
		port map(
			clk => clk,
			load => cu_m_load,
			reset => cu_m_reset,
			rshift => '0',
			d_in => '0',
			data_in => y,
			data_out => M_Out			
		);

	ParallelAdder: RCA -- Sommatore/Sottrattore
		port map( 
			x_in => A_Out,
			y_in => M_Out,
			c_in => cu_nadd_sub,
			nadd_sub => cu_nadd_sub,
			sum => sum_out,
			overflow => open,
			c_out => open
		);

	Counter: HitCounter	-- Contatore Mod(bits)
		port map( 
			clk => clk,
			reset => cu_c_reset, 
			count_inc => cu_count_inc,
			count_hit => cu_count_hit
		); 

	CUnit: BControlUnit -- Unit� di Controllo
		port map(	
			clk => clk,
			reset => reset,
			start => start,
			q_lsb => Q_Out(1 downto 0),
			count_hit => cu_count_hit,
			nadd_sub => cu_nadd_sub,
			a_reset => cu_a_reset,
			q_reset => cu_q_reset,
			m_reset => cu_m_reset,
			c_reset => cu_c_reset,
			a_load => cu_a_load,
			q_load => cu_q_load,
			m_load => cu_m_load,
			shift_en => cu_shift_en,
			count_inc => cu_count_inc,
			stop => stop
		);

end Structural;

-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  15/11/2014 
-- Module Name:  BUSMux_2_1.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Multiplexer 2:1 per BUS a N bit
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity BUSMux_2_1 is
	generic ( N: natural := 8 );
	port (din_0 : in std_logic_vector((N-1) downto 0);   
		  din_1 : in std_logic_vector((N-1) downto 0); 
		  sel : in std_logic; 
		  dout : out std_logic_vector((N-1) downto 0)
	);
end BUSMux_2_1;

architecture Behavioral of BUSMux_2_1 is
begin
	process(sel, din_0, din_1)
	begin
		case sel is
			when '0' => dout <= din_0;
			when '1' => dout <= din_1;
			when others => dout <= (others => 'X');
		end case; 
	end process;
end Behavioral;

-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: ASE Component Library
-- Create Date:  26/12/2014 
-- Module Name:  LSRegister.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Registro ad N bit con funzione di shift a sinistra
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity LSRegister is
	generic(N: integer := 8);
	port(	clk, load, reset, lshift: in std_logic;
			d_in: in std_logic;
			data_in: in std_logic_vector((N-1) downto 0);
			data_out: out std_logic_vector((N-1) downto 0)			
	);
end LSRegister;

architecture Behavioral of LSRegister is
	signal data: std_logic_vector((N-1) downto 0) := (others => '0');
begin
	process(clk, reset)
	begin
		if (reset = '1') then
			data <= (others => '0');
		elsif (clk'event and clk='1') then
			if (load = '1') then
				data <= data_in;
			elsif (lshift = '1') then
			   data <= data((N-2) downto 0) & d_in;
			end if;
		end if; 
	end process;

	data_out <= data;
	
end Behavioral;

-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Restoring Divisor
-- Create Date:  31/12/2014 
-- Module Name:  ControlUnit.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Unit� di controllo per il divisore con Restoring. Questo modulo
--               implementa una macchina a stati finiti che esegue l'algoritmo di 
--               divisione con Restoring per operandi interi senza segno.
--
-- Dependencies: none
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity RControlUnit is
	port(	clk : in  std_logic;
			reset : in  std_logic;
			start : in std_logic;
			sign : in std_logic;				-- sign of sum
			count_hit : in std_logic;		-- conteggio terminato
			nadd_sub : out  std_logic;		-- somma(0) sottrazione(1)
			a_reset : out  std_logic;		-- reset registro A
			q_reset : out  std_logic;		-- reset registro Q
			m_reset : out  std_logic;		-- reset registro M
			c_reset : out  std_logic;		-- reset contatore
			a_load : out  std_logic;		-- caricamento registro A
			q_load : out  std_logic;		-- caricamento registro Q
			m_load : out  std_logic;		-- caricamento registro M
			sel_mux : out  std_logic;		-- registri interni (0) precaricamento (1)
			shift_en : out  std_logic;		-- abilitazione shift A e Q
			count_inc : out  std_logic;	-- incremento conteggio
			stop : out std_logic				-- sistema fermo
	);
end RControlUnit;


architecture behavioral of RControlUnit is

type state_type is (s_idle, s_init, s_shift, s_sub, s_restoring, s_testcount, s_end);
signal s_current, s_next: state_type := s_idle;

begin

	next_state: process(clk, reset)
	begin
		if reset='1' then
			s_current <= s_idle;
		elsif (clk'event and clk='1') then
			s_current <= s_next;
		end if;
	end process;


	fsm_main: process (start, s_current, count_hit, sign)  -- gestisce la fsm
	begin

		-- reset generico ad ogni attivazione del process
		stop <= '0';			
		nadd_sub <= '1'; -- Data la prevalenza di ssottrazioni, mantengo attiva la funzione
		a_reset <= '0';
		q_reset <= '0';
		m_reset <= '0';
		c_reset <= '0';
		a_load <= '0';
		q_load <= '0';
		m_load <= '0';
		sel_mux <= '0';
		shift_en <= '0';
		count_inc <= '0';
				
		case s_current is

			-- stato di quiete, il sistema non fa niente	
			when s_idle => 	stop <= '1';				

									if start = '1' then
										s_next <= s_init;
									else 
										s_next <= s_idle;
									end if;
									
			-- stato di inizializzazione, il sistema si prepara a memorizzare gli operandi
			when s_init => 	sel_mux <= '1';
			
									a_load <= '1'; 
									q_load <= '1';
									m_load <= '1';
									
									c_reset <= '1';
									
									s_next <= s_shift;
									
			-- stato di shift a sinistra di a e q
			when s_shift => 	shift_en <='1';
									
									s_next <= s_sub;

			-- stato di sottrazione
			when s_sub => 		a_load <='1';
									q_load <='1';
									
									if sign = '0' then
										s_next <= s_testcount;
									else 
										s_next <= s_restoring; 
									end if;
									
			-- stato di restoring di A con M	
			when s_restoring =>	nadd_sub <= '0';
									a_load <= '1';
									
									s_next <= s_testcount;
									
									
			-- stato di controllo per il termine dell'algoritmo					
			when s_testcount =>									
									if count_hit = '1' then
										s_next <= s_end;
									else  
										count_inc <= '1';
										s_next <= s_shift;
									end if;

			-- stato finale, il sistema si ferma e restituisce il risultato finale
			when s_end =>		stop <= '1';
			
									if start ='0' then
										s_next <= s_idle;
									else 
										s_next <= s_end;
									end if;
									
			when others => 	s_next <= s_idle;
				
			end case;
		
	end process;

end behavioral;

-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: Restoring Divisor
-- Create Date:  31/12/2014 
-- Module Name:  RDivder.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Divisore ad N bits che implementa l'algoritmo di restoring
--
-- Dependencies: SRegister.vhd, BUSMux_2_1.vhd, RCA.vhd,
--				     FA.vhd, HitCounter.vhd, ControlUnit.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity RDivider is
	Generic (bits : integer := 4;
			   log_bits : integer := 2);
	Port (clk : in  STD_LOGIC;
		   reset : in  STD_LOGIC;
		   start : in STD_LOGIC;
         divider : in  STD_LOGIC_VECTOR (((2*bits)-1-1) downto 0);
         divisor : in  STD_LOGIC_VECTOR ((bits-1) downto 0);
		   stop : out STD_LOGIC;
		  reminder : out STD_LOGIC_VECTOR ((bits-1) downto 0); 
         quotient : out STD_LOGIC_VECTOR ((bits-1) downto 0));
end RDivider;

architecture Structural of RDivider is

	component BUSMux_2_1 is
		generic ( N: natural := bits );
		port (din_0 : in std_logic_vector((N-1) downto 0);   
			  din_1 : in std_logic_vector((N-1) downto 0); 
			  sel : in std_logic; 
			  dout : out std_logic_vector((N-1) downto 0)
		);
	end component;
	
	component RCA is
		generic ( N: natural := bits );
		port ( x_in, y_in: in  std_logic_vector ((N-1) downto 0);
				 c_in : in std_logic;
				 nadd_sub: in std_logic;
				 sum : out  std_logic_vector ((N-1) downto 0);
				 overflow : out std_logic; 
				 c_out : out std_logic);
	end component;

	component LSRegister is
		generic(N: integer := bits);
		port(	clk, load, reset, lshift: in std_logic;
				d_in: in std_logic;
				data_in: in std_logic_vector((N-1) downto 0);
				data_out: out std_logic_vector((N-1) downto 0)			
		);
	end component;

	component HitCounter is
		generic (N : natural := (bits-1));
		port ( clk, reset, count_inc : in std_logic;
				 count_hit: out std_logic  
		); 
	end component;
	
	component RControlUnit is
		port(	clk : in  std_logic;
				reset : in  std_logic;
				start : in std_logic;
				sign : in std_logic;				-- sign of sum
				count_hit : in std_logic;		-- conteggio terminato
				nadd_sub : out  std_logic;		-- somma(0) sottrazione(1)
				a_reset : out  std_logic;		-- reset registro A
				q_reset : out  std_logic;		-- reset registro Q
				m_reset : out  std_logic;		-- reset registro M
				c_reset : out  std_logic;		-- reset contatore
				a_load : out  std_logic;		-- caricamento registro A
				q_load : out  std_logic;		-- caricamento registro Q
				m_load : out  std_logic;		-- caricamento registro M
				sel_mux : out  std_logic;		-- registri interni (0) precaricamento (1)
				shift_en : out  std_logic;		-- abilitazione shift A e Q
				count_inc : out  std_logic;	-- incremento conteggio
				stop : out std_logic				-- sistema fermo
		);
	end component;

	signal mux_a_out, mux_a_in_1: std_logic_vector(bits downto 0) := (others => '0');
	signal mux_q_out, mux_q_in_0: std_logic_vector((bits-1) downto 0) := (others => '0');
	
	signal A_Out, sum_out, sum_in_y : std_logic_vector(bits downto 0) := (others => '0');
	signal M_Out, Q_Out : std_logic_vector((bits-1) downto 0) := (others => '0');
	
	signal cu_nadd_sub : std_logic;
	signal cu_a_reset, cu_q_reset, cu_m_reset, cu_c_reset : std_logic;
	signal cu_a_load, cu_q_load, cu_m_load: std_logic;
	signal cu_sel_mux : std_logic;
	signal cu_shift_en : std_logic;	
	signal cu_count_inc, cu_count_hit : std_logic;

begin

	reminder <= A_Out((bits-1) downto 0);
	quotient <= Q_Out;
	
	-- La coppia di registra A.Q presenta due bit in piu rispetto al dividendo, posti inizialmente a 0
	mux_a_in_1 <= "00" & divider(((2*bits)-1-1) downto bits);
	-- Registro di supporto per il caricamento del quoziente
	mux_q_in_0 <= Q_Out((bits-1) downto 1) & (not sum_out(bits));
	-- Il sommatore ha un bit in piu come A, quindi si aggiunge uno 0 in testa al registro M
	sum_in_y <= '0' & M_Out;
	
	MUX_A: BUSMux_2_1 -- Seleziona l'uscita del sommatore (0) o l'MSB del dividendo (1)
		generic map (N => (bits+1))
		port map( 
			din_0 => sum_out,    -- A carica sum_out,
			din_1 => mux_a_in_1, -- A carica l'MSB di divider,
			sel => cu_sel_mux,
			dout => mux_a_out
		);
	
	MUX_Q: BUSMux_2_1 -- Seleziona il registro di precarica del quiziente (0) o l'LSB del dividendo (1)
		generic map (N => bits)
		port map( 
			din_0 => mux_q_in_0, 					 -- Q carica Q(n-1 downto 1) & Q0,
			din_1 => divider((bits-1) downto 0), -- Q carica l'LSB di D,
			sel => cu_sel_mux,
			dout => mux_q_out
		);

	A: LSRegister -- Registro A
		generic map (N => (bits+1))
		port map(
			clk => clk,
			load => cu_a_load,
			reset => cu_a_reset,
			lshift => cu_shift_en,
			d_in => Q_Out((bits-1)),
			data_in => mux_a_out,
			data_out => A_Out			
		);

	Q: LSRegister -- Registro Q
		port map(
			clk => clk,
			load => cu_q_load,
			reset => cu_q_reset,
			lshift => cu_shift_en,
			d_in => '-',
			data_in =>  mux_q_out,
			data_out => Q_Out			
		);
				
	M: LSRegister -- Registro M
		port map(
			clk => clk,
			load => cu_m_load,
			reset => cu_m_reset,
			lshift => '0',
			d_in => '0',
			data_in => divisor,
			data_out => M_Out			
		);

	ParallelAdder: RCA -- Sommatore/Sottrattore
		generic map (N => (bits+1))
		port map( 
			x_in => A_Out,
			y_in => sum_in_y,
			c_in => cu_nadd_sub,
			nadd_sub => cu_nadd_sub,
			sum => sum_out,
			overflow => open,
			c_out => open
		);

	Counter: HitCounter	-- Contatore Mod(bits)
		port map( 
			clk => clk,
			reset => cu_c_reset, 
			count_inc =>  cu_count_inc,
			count_hit => cu_count_hit
		); 

	CUnit: RControlUnit -- Unit� di Controllo
		port map(	
			clk => clk,
			reset => reset,
			start => start,
			sign => sum_out(bits),
			count_hit => cu_count_hit,
			nadd_sub => cu_nadd_sub,
			a_reset => cu_a_reset,
			q_reset => cu_q_reset,
			m_reset => cu_m_reset,
			c_reset => cu_c_reset,
			a_load => cu_a_load,
			q_load => cu_q_load,
			m_load => cu_m_load,
			sel_mux => cu_sel_mux,
			shift_en => cu_shift_en,
			count_inc => cu_count_inc,
			stop => stop
		);

end Structural;



