-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar


-- 
-- Project Name: IR-RC5 Module
-- Create Date:  10/01/2015
-- Module Name:  RC5EncDec_tb.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Testbench dei soli moduli di codifica e decodifica Manchster
--               impiegati nel progetto
--
-- Dependencies: RC5Enc.vhd, RC5Dec.vhd
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY RC5EncDec_tb IS
END RC5EncDec_tb;
 
ARCHITECTURE behavior OF RC5EncDec_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT RC5Enc
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         data_bus : IN  std_logic_vector(13 downto 0);
			end_tx : OUT std_logic;
         out_data : OUT  std_logic
        );
    END COMPONENT;
    
	COMPONENT RC5Dec
	PORT ( 
			clk2x : in std_logic;	
			reset : in std_logic; 
			in_data : in std_logic; 
			end_rx : out std_logic;	
			error_rx : out std_logic;
			data_bus : out std_logic_vector (13 downto 0));
	END COMPONENT;

   signal tx_clk : std_logic := '0';
   signal rx_clk : std_logic := '0';
 
   signal reset : std_logic := '0';
 
	signal tx_data_bus : std_logic_vector(13 downto 0) := (others => '0');
   signal rx_data_bus : std_logic_vector(13 downto 0) := (others => '0');

   signal encoded_data_line : std_logic;
   signal received_data_line : std_logic;
 
	signal end_tx : std_logic;	
	signal end_rx : std_logic;	
	signal error_rx : std_logic;

   -- Clock period definitions
   constant tx_clk_period : time := 10 ns;
   constant rx_clk_period : time := 10 ns;
 
BEGIN
 
	received_data_line <= not encoded_data_line;
	
	-- Instantiate the Unit Under Test (UUT)
   uut_tx: RC5Enc PORT MAP (
          clk => tx_clk,
          reset => reset,
          data_bus => tx_data_bus,
			 end_tx => end_tx,
          out_data => encoded_data_line
        );

	uut_rx: RC5Dec PORT MAP( 
			 clk2x => rx_clk,
			 reset => reset,
			 in_data => received_data_line,
			 end_rx => end_rx,	
			 error_rx => error_rx,
			 data_bus => rx_data_bus
        );

   -- Clock process definitions
   tx_clk_process :process
   begin
		tx_clk <= '0';
		wait for tx_clk_period/2;
		tx_clk <= '1';
		wait for tx_clk_period/2;
   end process;

   rx_clk_process :process
   begin
		rx_clk <= '0';
		wait for rx_clk_period/2;
		rx_clk <= '1';
		wait for rx_clk_period/2;
   end process;

   -- Stimulus process
   stim_proc: process
   begin		
 		reset  <= '1';
		tx_data_bus <= "11000000000001";
		wait for tx_clk_period*10;
		reset  <= '0';
     
	   wait for tx_clk_period*100;
	  
   end process;

END;
