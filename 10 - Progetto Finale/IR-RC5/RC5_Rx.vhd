-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: IR-RC5 Module
-- Create Date:  07/01/2015
-- Module Name: RC5_Rx.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Modulo per la ricezione seriale su infrarossi, secondo il 
--               protocollo RC5 della Philips.
--
-- Dependencies: sPWM.vhd, RC5Dec.vhd, CUDec.vhd
-------------------------------------------------------------------------------
-- Il comando � composto da 14 bits:
--
-- | 1 | 1 | T | A | A | A | A | A | C | C | C | C | C | C |
-- |  ACG  |CHK|      ADDRESS      |        COMMAND        |
--
-- Il valore del singolo bit (0 o 1) � determinato dal tipo di fronte salita o 
-- discesa che veniamo a trovare:
-- 
-- rising_edge = 0 "01" nella cella bit (in ricezione)
-- falling_edge = 1 "10" nella cella bit (in ricezione)
-- 
-- ACG: Rappresenta la sequenza di start ed � composta dallo start bit sempre 1
--      e dal field bit utilizzato solo nella versione extended altrimenti 1.
-- CHK o TOGGLE: Questo bit cambia di stato logico ad ogni ritrasmissione. Ad 
--               esempio se premo il tasto 1 del mio telecomando fintanto che 
--               lo tengo premuto il bit TOGGLE rimarr� sempre uguale, mentre 
--               se rilascio il tasto e lo ripremo cambier� stato logico. 
--               Questo fornisce informazione al micropc in ricezione che ho 
--               appunto rilasciato e premuto un tasto.
-- ADDRESS: Qui troviamo in sequenza 5 bit utilizzati come indirizzo per poter 
--          comandare fino a 32 apparecchi diversi simultaneamente. Normalmente
--          troviamo "00000" come indirizzo per il comando del TV.
-- COMMAND: Qui troviamo in sequenza 6 bit utilizzati per la trasmissione del
--          comando vero e proprio. Che quindi nella versione base di RC5 
--          permette 2^6 = 64 comandi. Ad esempio "000000" corrisponde al tasto 
--          programma 0, mentre "000001" al tasto 1 e cos� via.
-- 
-- Sia per ADDRESS che per COMMAND la sequenza dei bit va dal pi� significatico
-- al meno significativo.
-- 
-- La larghezza di ciascun stato logico, che possiamo chiamare step, � di 889 usec.
-- Un bit � formato da 2 step.
-- Quindi la lunghezza totale � 889 x 2 x 14 = 24,9 msec
-- La pausa tra un treno impulsi e il successivo � di 105 msec

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity RC5_Rx is
	port (sys_clk   : in std_logic;							  -- Clock di sistema
			reset		 : in std_logic;							  -- Resetta il sistema e lo prepara ad una nuova ricezione
			ir_mod_in : in std_logic;               		  -- Direttamente collegabile ad un modulo rx ad infrarossi
			no_signal : out std_logic;							  -- Segnale di errore sul mezzo trasmissivo
			irq_ir	 : out std_logic;							  -- Richiesta di interrupt: se � attiva � disponibile una nuova ricezione
			address   : out std_logic_vector(4 downto 0);  -- Indirizzo del dispositivo 
			command   : out std_logic_vector(6 downto 0)); -- Comando per il dispositivo
end RC5_Rx;

architecture mixed of RC5_Rx is
	component sPWM is
		generic(
			mode		: std_logic := '0';		 --Tipo di forma d'onda ..|''|(0) |''|..(1) 
			sys_clk  : integer := 50_000_000; --Clock di sistema in hz
			pwm_freq : integer := 36_000;     --Frequenza del PWM in hz
			duty     : integer := 30);			 --Duty cycle in % (ampiezza livello logico alto)
		port(
			clk      : in  std_logic;         --Clock di sistema
			reset    : in  std_logic;         --Reset del modulo
			pwm_out  : out std_logic);        --Outputs PWM
	end component;

	component RC5Dec is
		 generic ( N: natural := 13 );
		 port ( clk : in std_logic;									     -- Clock di campionamento
				  reset : in std_logic;                              -- Reset dei registri interni
				  in_data : in std_logic;                            -- Dato seriale codificato in ingresso
				  end_rx : out std_logic;									  -- Attivo quando tutti gli N bit sono stati ricevuti
				  error_rx : out std_logic;								  -- Attivo quando si � presentato un errore nella decodifica
				  data_bus : out std_logic_vector ((N-1) downto 0)); -- Dato ricevuto
	end component;

	component CUDec is
		port (sys_clk	  : in std_logic;	 -- Clock di sistema
				reset		  : in std_logic;	 -- Resetta il sistema
				ir_mod_in  : in std_logic;	 -- Segnale di ingresso dal modulo rx
				toggle_in  : in std_logic;	 -- Segnale di toggle decodificato
				dec_end_rx : in std_logic;	 -- Segnale di ricezione completata		
				dec_err_rx : in std_logic;	 -- Segnale di errore durante la ricezione
				all_reset  : out std_logic; -- Segnale di reset del decodificatore
				load_data  : out std_logic; -- Abilita la memorizzazione del dato decodificato
				gen_err   : out std_logic;  -- Alto se vi � un errore nella trasmissione
				gen_irq	  : out std_logic  -- Attiva la linea di interruzione
		);
	end component;
	

	signal sample_clk : std_logic := '0';
	signal reg_data : std_logic_vector(12 downto 0) := (others => '0');

	signal cu_dec_end_rx : std_logic := '0';
	signal cu_dec_err_rx : std_logic := '0';
	signal cu_all_reset : std_logic := '0';
	signal cu_load_data : std_logic := '0';
	
begin
	
	rx_successfull: process (sys_clk, cu_load_data, reg_data)
	begin
		if (sys_clk'event and sys_clk='1') then
			if (cu_load_data = '1') then
				address <= reg_data(10 downto 6);
				-- Il bit C6, del command va interpretato negato (supporto a RC5-EX)
				command <= (not reg_data(12)) & reg_data(5 downto 0);
			else 
				address <= (others => '0');
				command <= (others => '0');
			end if;
		end if;
	end process;

	sample_clk_gen : sPWM 
		generic map (
			pwm_freq => 1124, -- Frequenza doppia di bit-rate
			duty => 25    
		)
		port map (
			clk => sys_clk,
			reset => cu_all_reset,
			pwm_out => sample_clk
		);

	rc5_decoder : RC5Dec
		port map (
			clk => sample_clk,
			reset => cu_all_reset,
			in_data => ir_mod_in,
			end_rx => cu_dec_end_rx,
			error_rx => cu_dec_err_rx,
			data_bus => reg_data
		);

	rc5_control_unit_rx : CUDec
		port map (
			sys_clk => sys_clk,
			reset => reset,
			ir_mod_in => ir_mod_in,
			toggle_in => reg_data(11),
			dec_end_rx => cu_dec_end_rx,
			dec_err_rx => cu_dec_err_rx,
			all_reset => cu_all_reset,
			load_data => cu_load_data,
			gen_err => no_signal,
			gen_irq => irq_ir
		);

end mixed;

