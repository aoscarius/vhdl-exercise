-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar


-- 
-- Project Name: IR-RC5 Module
-- Create Date:  9/01/2015
-- Module Name:  CUDec.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Modulo che implementa la control unit, impiegata nel modulo di 
--               ricezione IR-RC5. Questo modulo si occupa della generazione corretta
--               dei sincronismi e della supervisione sul dato ricevuto
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity CUDec is
	generic ( sys_clock : integer := 50_000_000 );
	port (sys_clk	  : in std_logic;	 -- Clock di sistema
			reset		  : in std_logic;	 -- Resetta il sistema
			ir_mod_in  : in std_logic;	 -- Segnale di ingresso dal modulo rx
			toggle_in  : in std_logic;	 -- Segnale di toggle decodificato
			dec_end_rx : in std_logic;	 -- Segnale di ricezione completata		
			dec_err_rx : in std_logic;	 -- Segnale di errore durante la ricezione
			all_reset  : out std_logic; -- Segnale di reset del decodificatore
			load_data  : out std_logic; -- Abilita la memorizzazione del dato decodificato
			gen_err   : out std_logic;  -- Alto se vi � un errore nella trasmissione
			gen_irq	  : out std_logic  -- Attiva la linea di interruzione
	);
end CUDec;

architecture Behavioral of CUDec is
	
	constant half_cycle_ticks : natural := sys_clock/1124; -- Numero di sys_ticks in mezzo periodo di dati

	type state_type is (s_reset, s_idle, s_sync, s_start, s_stop);
	signal state: state_type := s_idle;
	
	-- Segnali di controllo per il sync della decodifica
	signal sync_counter : natural range 0 to half_cycle_ticks := 0; 
	signal sync_reset : std_logic := '0';
	signal sync_start : std_logic := '0';
	signal is_sync : std_logic := '0';

	-- Segnale di memorizzazione del toogle bit
	signal reg_toggle : std_logic := '0';

begin

	-- Questo processo implementa un contatore, impiegato per 
	-- avviare la decodifica ad inizio ricezione. Permette di 
	-- sincronizzare il decodificatore con i dati ricevuti
	halt_sync_count: process(sys_clk, sync_reset, sync_start)
	begin
		if sync_reset='1' then
			is_sync <= '0';
			sync_counter <= half_cycle_ticks;
		elsif (sys_clk'event and sys_clk='1') then
			if (sync_start = '1') then
				sync_counter <= sync_counter - 1;
				if (sync_counter = 0) then
					is_sync <= '1';
				end if;
			end if;
		end if;
	end process;

	-- Macchina a stati finiti di controllo
	fsm_main: process (sys_clk, reset, ir_mod_in, is_sync, dec_end_rx, dec_err_rx) 
	begin

		if reset='1' then
		
			gen_err <= '0'; -- Questo reset viene effettuato fisicamente solo al reset manuale
			state <= s_reset;
			
		elsif (sys_clk'event and sys_clk='1') then

			case state is
				
				-- Stato di reset dove, tutti i parametri vengoni inizializzati
				when s_reset => 
					all_reset <= '0';
					load_data <= '0';
					gen_irq <= '0';
					
					reg_toggle <= toggle_in;
					
					sync_reset <= '1';
					sync_start <= '0';
					state <= s_idle;

				-- Stato di idle, dove la macchina attende la prima variazione da
				-- alto a basso del segnale di ricezione, per poter avviare il sincronismo
				-- e quindi la decodifica del dato
				when s_idle => 	
					sync_reset <= '0';
					if (ir_mod_in='0') then
						sync_start <= '1';
						state <= s_sync;
					end if;
				
				-- Stato di sync in cui si attende il termine della fase di sync per avviare 
				-- la decodifica
				when s_sync =>
					gen_err <= '0';
					all_reset <= '1';
					if (is_sync='1') then
						sync_start <= '0';
						state <= s_start;
					end if;
					
				-- Stato di decodifica al termine del quale, se vi � stato un errore, 
				-- il sistema si autoresetta in attesa di un nuovo dato, altrimenti
				-- passa alla fase di controllo del dato ricevuto
				when  s_start =>
					all_reset <= '0';
					if (dec_err_rx='1') then
						gen_err <= '1';
						state <= s_reset;
					elsif (dec_end_rx='1') then
						state <= s_stop;
					end if;
										
				-- Stato di termine trasmissione e controllo. Se il dato ricevuto
				-- presenta un toggle bit differente da quello precedente allora � 
				-- stato ricevuto un nuovo dato, come da specifiche del protocollo
				-- e viene quindi alzata la linea di interrupt e viene abilitata la
				-- memorizzazione del dato
				when s_stop => 
					load_data <= '1';
					if (reg_toggle /= toggle_in) then
						gen_irq <= '1'; 
					end if;
					state <= s_stop;

				when others =>  
					state <= s_reset;
			
				end case;
			end if;
	end process;
end Behavioral;

