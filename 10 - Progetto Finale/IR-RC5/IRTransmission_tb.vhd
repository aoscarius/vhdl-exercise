-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar


-- 
-- Project Name: IR-RC5 Module
-- Create Date:  10/01/2015
-- Module Name:  IRTransmission_tb.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Testbench per i moduli di trasmissione e ricezione IR su protocollo
--					  RC5 della Philips. Il test simula un intera fase di comunicazione
--               con test di invio e ricezione dello stesso messaggio con toggle 
--					  alto e basso
--
-- Dependencies: RC5_Tx.vhd, RC5_Rx.vhd, sPWM.vhd, sWatchdog.vhd, RC5Enc.vhd, 
--               RC5Dec.vhd, CUDec.vhd
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

ENTITY IRTransmission_tb IS
END IRTransmission_tb;
 
ARCHITECTURE behavior OF IRTransmission_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
	COMPONENT RC5_Tx
		PORT(
			sys_clk : IN  std_logic;
			reset : IN  std_logic;
			send : IN  std_logic;
			address : IN  std_logic_vector(4 downto 0);
			command : IN  std_logic_vector(6 downto 0);
			ir_led_out : OUT  std_logic;
			ir_mod_out : OUT  std_logic
		);
	END COMPONENT;
		 
	COMPONENT RC5_Rx is
		PORT (
			sys_clk   : in std_logic;							  
			reset     : in std_logic;							  
			ir_mod_in : in std_logic;               		  
			no_signal : out std_logic;
			irq_ir 	 : out std_logic;  				        
			address   : out std_logic_vector(4 downto 0);  
			command   : out std_logic_vector(6 downto 0)
		); 
	END COMPONENT;

   signal sys_clk : std_logic := '0';
   
	signal tx_send : std_logic := '0';

   signal tx_reset : std_logic := '0';
   signal rx_reset : std_logic := '0';
 
	signal tx_address : std_logic_vector(4 downto 0) := (others => '0');
   signal tx_command : std_logic_vector(6 downto 0) := (others => '0');

   signal ir_led : std_logic;
   signal ir_mod : std_logic;
	signal ir_rx : std_logic := '0';
	signal ir_rx_select : std_logic := '0';

   signal rx_address : std_logic_vector(4 downto 0) := (others => '0');
   signal rx_command : std_logic_vector(6 downto 0) := (others => '0');

	signal led_error : std_logic := '0';
	signal rx_irq : std_logic := '0';
 
   -- Clock period definitions
   constant sys_clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uutTX: RC5_Tx PORT MAP (
          sys_clk => sys_clk,
          reset => tx_reset,
          send => tx_send,
          address => tx_address,
          command => tx_command,
          ir_led_out => ir_led,
          ir_mod_out => ir_mod
        );

   uutRX: RC5_Rx PORT MAP (
			 sys_clk => sys_clk,							  
			 reset => rx_reset,					  
			 ir_mod_in => ir_rx,         		  
			 no_signal => led_error,
			 irq_ir => rx_irq,				        
			 address => rx_address,  
			 command => rx_command
        );

   -- Clock process definitions
   sys_clk_process :process
   begin
		sys_clk <= '1';
		wait for sys_clk_period/2;
		sys_clk <= '0';
		wait for sys_clk_period/2;
   end process;
 

   -- Stimulus process
	mux_rx_proc: process (sys_clk, ir_rx_select)
   begin		
		case ir_rx_select is
			when '0' => ir_rx <= ir_mod;
			when '1' => ir_rx <= '1';
			when others => ir_rx <= '0';
		end case;
	end process;
   
	tx_proc: process
   begin		
 		wait for 10 ms;	

		-- Collego il canale di trasmissione con quello di ricezione
		ir_rx_select <= '0';

		tx_address  <= (others => '0');
		tx_command  <= "1000001";
		
		-- Resetto i due dispositivi
		tx_reset <= '1';
 		rx_reset  <= '1';
		wait for 1 ms;
		tx_reset <= '0';
		rx_reset  <= '0';

 		-- La trasmissione parte 10 ms dopo il reset del ricevitore
		wait for 10 ms;
		tx_send  <= '1';
 		
		-- Dopo 50 ms simulo un nuovo reset del ricevitore
		wait for 50 ms;
		rx_reset  <= '1';
		wait for 1 ms;
		rx_reset  <= '0';
		
		-- Dopo 70 ms circa arresto la trasmissione
		wait for 50 ms;
		tx_send  <= '0';
		wait for 10 ms;
		tx_send  <= '1';
		
		wait for 40 ms;
		rx_reset  <= '1';
		wait for 10 ms;
		rx_reset  <= '0';
		
		-- Dopo 220 ms simulo la caduta del segnale o comunque l'assenza di segnale
		wait for 80 ms;
		ir_rx_select <= '1';

		-- Dopo 270 ms ripristino il collegamento
		wait for 50 ms;
		ir_rx_select <= '0';
		
		-- Dopo 300 ms ricomincio il test
		wait for 100 ms;

   end process;

END;
