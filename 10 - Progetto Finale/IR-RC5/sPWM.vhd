-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar


-- 
-- Project Name: IR-RC5 Module
-- Create Date:  06/01/2015
-- Module Name:  sPWM.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Modulo per la generazione di un segnale PWM, a frequenza prefissata e 
--					  duty cycle fisso e prefissato in fase di istanziazione.
--
-- Dependencies: none
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity sPWM is
  generic(
      mode		: std_logic := '0';		 --Tipo di forma d'onda ..|''|(0) |''|..(1) 
		sys_clk  : integer := 50_000_000; --Clock di sistema in hz
      pwm_freq : integer := 36_000;     --Frequenza del PWM in hz
		duty     : integer := 30);			 --Duty cycle in % (ampiezza livello logico alto)
  port(
      clk      : in  std_logic;         --Clock di sistema
      reset    : in  std_logic;         --Reset del modulo
      pwm_out  : out std_logic);        --Outputs PWM
end sPWM;

architecture behavioral of sPWM is
	constant period   : integer := sys_clk/pwm_freq;         --Numero di clocks in un periodo
	constant reg_duty : integer := duty*period/100;          --Numero di clocks in un duty cycle
	signal reg_count  : integer range 0 to period - 1 := 0;  --Contatore di clocks
begin
	process(clk, reset)
	begin
		-- Reset asincrono
		if(reset = '1') then
			reg_count <= 0;
			pwm_out <= '0';
	   -- Ad ogni ciclo di clock
		elsif(clk'event and clk = '1') then                      
			-- Se il contatore ha raggiunto il numero di clocks per periodo
			-- allora resetto il contatore
			if(reg_count = (period-1)) then
				reg_count <= 0;
			else
				reg_count <= reg_count + 1;
			end if;
			-- Nel mode = '0' dove la forma d'onda � del tipo ....|''''|
			if	(mode = '0') then
				-- Se il contatore ha superato il numero di clock per duty cycle
				-- allora porto l'uscita al suo valore logico basso
				if(reg_count > (period-reg_duty-1)) then
					pwm_out <= '1';
				else
					pwm_out <= '0';
				end if;
			
			-- Altrimenti nel mode = '1' dove la forma d'onda � del tipo |''''|....
			else
				-- Se il contatore ha superato il numero di clock per duty cycle
				-- allora porto l'uscita al suo valore logico basso
				if(reg_count > (reg_duty-1)) then
					pwm_out <= '0';
				else
					pwm_out <= '1';
				end if;
			end if;
		end if;
	end process;
end behavioral;
