-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar


-- 
-- Project Name: IR-RC5 Module
-- Create Date:  02/01/2015
-- Module Name:  RC5Enc.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Modulo per la codifica Manchester, nota anche come codifica 
--					  bi-fase, di un dato ad N bit. Il modulo pone in uscita in 
--               modalit� sequenziale, il dato contenuto in data_bus.
--
-- Dependencies: none
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity RC5Enc is
	 generic ( N: natural := 14);
    port ( clk : in std_logic;									    -- Clock di trasmissione
			  reset : in std_logic;                             -- Reset e caricamento del dato
			  data_bus : in  std_logic_vector ((N-1) downto 0); -- Dato da trasmettere
			  end_tx : out std_logic;									 -- Attivo quando tutti gli N bit sono stati trasmessi
           out_data : out  std_logic);                       -- Dato serial codificato in uscita
end RC5Enc;

architecture behavioral of RC5Enc is
	signal ff_clk : std_logic := '0'; -- FlipFlop T utilizzato per dimezzare la frequenza di clock

	signal tx_enabled : std_logic := '0';
	signal bit_counter : integer range 0 to N := N; -- Contatore di bit interno (+1 bit di sync)
	signal reg_data : std_logic_vector ((N-1) downto 0) := (others => '0');
begin
	process(clk, reset, data_bus)
	begin
		-- Se � attivo il reset
		if (reset = '1') then
			ff_clk <= '0';
			
			-- Carico il dato da trasferire in un registro buffer interno
			reg_data <= data_bus;

			tx_enabled <= '1';
			bit_counter <= N;
			out_data <= '0';
		else
			-- Finch� non ho completato la trasmissione di tutti i bit
			if (tx_enabled = '1') then
				-- Ad ogni ciclo di clock
				if (clk'event and clk='1') then
					-- Togglo il flip-flop usato come riferimento per il posizionamento del dato
					ff_clk <= not ff_clk;
					-- Se l'FF-T � alto
					if (ff_clk = '1') then 
						-- Allora shifto il registro ad anello e decremento il contatore di bit
						reg_data <= reg_data((N-2) downto 0) & reg_data(N-1);
						bit_counter <= bit_counter - 1;
					end if;
					if (bit_counter = 0) then
						tx_enabled <= '0';
					end if;
				-- La codifica in uscita secondo manchester � dato dallo xor tra il bit piu 
				-- significativo da trasmettere e il clock del dato (negato a causa della natura 
				-- della forma d'onda del clock ..|''|)
					out_data <= reg_data(N-1) xor (not ff_clk);
				end if;
			else
			-- Se la trasmissione � disabilitata all'ora l'uscita � nulla
				out_data <= '0';
			end if;		
		end if;
	end process;
	
	end_tx <= not tx_enabled;
	
end behavioral;