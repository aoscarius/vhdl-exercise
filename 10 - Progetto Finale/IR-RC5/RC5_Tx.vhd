-------------------------------------------------------------------------------
-- Engineers: Castello Oscar
-- 
-- Project Name: IR-RC5 Module
-- Create Date:  06/01/2015
-- Module Name: RC5_Tx.vhd
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Modulo per la trasmissione seriale su infrarossi, secondo il 
--               protocollo RC5 della Philips.
--
-- Dependencies: sPWM.vhd, RC5Enc.vhd, CUEnc.vhd
-------------------------------------------------------------------------------
-- Il comando � composto da 14 bits:
--
-- | 1 | 1 | T | A | A | A | A | A | C | C | C | C | C | C |
-- |  ACG  |CHK|      ADDRESS      |        COMMAND        |
--
-- Il valore del singolo bit (0 o 1) � determinato dal tipo di fronte salita o 
-- discesa che veniamo a trovare:
-- 
-- rising_edge = 1 "01" nella cella bit (in trasmissione)
-- falling_edge = 0 "10" nella cella bit (in trasmissione)
-- 
-- ACG: Rappresenta la sequenza di start ed � composta dallo start bit sempre 1
--      e dal field bit utilizzato solo nella versione extended altrimenti 1.
-- CHK o TOGGLE: Questo bit cambia di stato logico ad ogni ritrasmissione. Ad 
--               esempio se premo il tasto 1 del mio telecomando fintanto che 
--               lo tengo premuto il bit TOGGLE rimarr� sempre uguale, mentre 
--               se rilascio il tasto e lo ripremo cambier� stato logico. 
--               Questo fornisce informazione al micropc in ricezione che ho 
--               appunto rilasciato e premuto un tasto.
-- ADDRESS: Qui troviamo in sequenza 5 bit utilizzati come indirizzo per poter 
--          comandare fino a 32 apparecchi diversi simultaneamente. Normalmente
--          troviamo "00000" come indirizzo per il comando del TV.
-- COMMAND: Qui troviamo in sequenza 6 bit utilizzati per la trasmissione del
--          comando vero e proprio. Che quindi nella versione base di RC5 
--          permette 2^6 = 64 comandi. Ad esempio "000000" corrisponde al tasto 
--          programma 0, mentre "000001" al tasto 1 e cos� via.
-- 
-- Sia per ADDRESS che per COMMAND la sequenza dei bit va dal pi� significatico
-- al meno significativo.
-- 
-- La larghezza di ciascun stato logico, che possiamo chiamare step, � di 889 usec.
-- Un bit � formato da 2 step.
-- Quindi la lunghezza totale � 889 x 2 x 14 = 24,9 msec
-- La pausa tra un treno impulsi e il successivo � di 105 msec

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity RC5_Tx is
	port (sys_clk : in std_logic;							 -- Clock di sistema
			reset   : in std_logic;							 -- Reset di sistema
			send    : in std_logic;							 -- Finch� � attivo invia la trasmissione
			address : in std_logic_vector(4 downto 0); -- Indirizzo del dispositivo 
			command : in std_logic_vector(6 downto 0); -- Comando per il dispositivo
			ir_led_out : out std_logic;					 -- Direttamente collegabile ad un led ad infrarossi
			ir_mod_out : out std_logic);               -- Direttamente collegabile in tx con filo (loop function)
end RC5_Tx;

architecture mixed of RC5_Tx is
	component sPWM is
		generic(
			mode		: std_logic := '1';		 --Tipo di forma d'onda ..|''|(0) |''|..(1) 
			sys_clk  : integer := 50_000_000; --Clock di sistema in hz
			pwm_freq : integer := 36_000;     --Frequenza del PWM in hz
			duty     : integer := 30);			 --Duty cycle in % (ampiezza livello logico alto)
		port(
			clk      : in  std_logic;         --Clock di sistema
			reset    : in  std_logic;         --Reset del modulo
			pwm_out  : out std_logic);        --Outputs PWM
	end component;

	component RC5Enc is
		 generic ( N: natural := 14);
		 port ( clk : in std_logic;									    -- Clock di trasmissione
				  reset : in std_logic;                             -- Reset e caricamento del dato
				  data_bus : in  std_logic_vector ((N-1) downto 0); -- Dato da trasmettere
				  end_tx : out  std_logic;                          -- Attivo a fine trasmissione
				  out_data : out  std_logic);                       -- Dato codificato
	end component;

	component CUEnc is
		port (sys_clk	        : in std_logic;	 -- Clock di sistema
				reset				  : in std_logic;  -- Resetta il sistema
				send		        : in std_logic;	 -- Avvia il sistema
				end_tx			  : in std_logic;  -- Segnale di fine trasmissione pacchetto
				toggle  	        : out std_logic; -- Segnale di toggle da codificare
				load_data        : out std_logic; -- Abilita la memorizzazione del dato da codificare
				all_reset        : out std_logic  -- Segnale di reset del codificatore
		);
	end component;

	signal carrier_clk : std_logic := '0';
	signal data_clk : std_logic := '0';

	signal reg_data : std_logic_vector(13 downto 0) := (others => '0');
	signal reg_out : std_logic := '0';
	
	signal cu_end_tx : std_logic := '0';

	signal cu_toggle : std_logic := '0';
	signal cu_load_data :std_logic := '0';
	signal cu_all_reset : std_logic := '0'; 
	
begin
	
	tx_start: process (sys_clk, cu_load_data, reg_data)
	begin
		if (sys_clk'event and sys_clk='1') then
			if (cu_load_data = '1') then
				-- Nella versione RC5-EX il bit C6 di command ha significato negato
				-- mentre nella RC5, questo bit � sempre 1
				reg_data <= '1' & (not command(6)) & cu_toggle & address & command(5 downto 0);
			end if;
		end if;
	end process;

	carrier_clk_gen : sPWM 
		generic map (
			pwm_freq => 36_000, -- Frequenza della portante
			duty => 30    
		)
		port map (
			clk => sys_clk,
			reset => cu_all_reset,
			pwm_out => carrier_clk
		);

	data_clk_gen : sPWM 
		generic map (
			pwm_freq => 1124, -- Frequenza doppia di bit-rate
			duty => 50    
		)
		port map (
			clk => sys_clk,
			reset => cu_all_reset,
			pwm_out => data_clk
		);
	
	rc5_encoder : RC5Enc
		port map (
			clk => data_clk,
			reset => cu_all_reset,
			data_bus => reg_data,
			end_tx => cu_end_tx,
			out_data => reg_out
		);

	rc5_control_unit_tx : CUEnc
		port map (
			sys_clk => sys_clk,
			reset => reset,
			send => send,
			end_tx => cu_end_tx,
			toggle => cu_toggle,
			load_data => cu_load_data,
			all_reset => cu_all_reset
		);
		
	ir_led_out <= reg_out and carrier_clk; -- Modulazione della portante con il segnale codificato
	ir_mod_out <= not reg_out; -- Segnale codificato negato (utile per le connessioni con filo)

end mixed;

