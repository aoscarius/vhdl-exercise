-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar


-- 
-- Project Name: IR-RC5 Module
-- Create Date:  9/01/2015
-- Module Name:  CUEnc.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Modulo che implementa la control unit, impiegata nel modulo di 
--               trasmissione IR-RC5. Questo modulo si occupa della generazione corretta
--               dei sincronismi e della supervisione sulla codifica del dato da trasmettere
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity CUEnc is
	generic ( sys_clock    : integer := 50_000_000 );
	port (sys_clk	        : in std_logic;	 -- Clock di sistema
			reset				  : in std_logic;  -- Resetta il sistema
			send		        : in std_logic;	 -- Avvia il sistema
			end_tx			  : in std_logic;  -- Segnale di fine trasmissione pacchetto
			toggle  	        : out std_logic; -- Segnale di toggle da codificare
			load_data        : out std_logic; -- Abilita la memorizzazione del dato da codificare
			all_reset        : out std_logic  -- Segnale di reset del codificatore
	);
end CUEnc;

architecture Behavioral of CUEnc is
	
	constant wdog_ticks : natural := (sys_clock/10); -- Numero di sys_ticks di pausa tra una trasmissione ed un altra

	type state_type is (s_reset, s_idle, s_toogle, s_load, s_start_reset, s_start_rx, s_wdog);
	signal state: state_type := s_idle;
	
	-- Segnali di controllo per il sync della decodifica
	signal wdog_counter : natural range 0 to wdog_ticks := 0; 
	signal wdog_reset : std_logic := '0';
	signal wdog_start : std_logic := '0';
	signal wdog_timeout : std_logic := '0';

	-- Segnale di memorizzazione del toogle bit
	signal reg_toggle : std_logic := '1';

begin

	-- Questo processo implementa un contatore, impiegato per 
	-- avviare la il reset automatico e il reinvio del pachetto,
	-- ogni wdog_ticks se il tasto send resta premuto
	watchdog_count: process(sys_clk, wdog_reset, wdog_start)
	begin
		if wdog_reset='1' then
			wdog_timeout <= '0';
			wdog_counter <= wdog_ticks;
		elsif (sys_clk'event and sys_clk='1') then
			if (wdog_start = '1') then
				wdog_counter <= wdog_counter - 1;
				if (wdog_counter = 0) then
					wdog_timeout <= '1';
				end if;
			end if;
		end if;
	end process;

	-- Macchina a stati finiti di controllo
	fsm_main: process (sys_clk, send, end_tx, wdog_timeout) 
	begin

		if reset='1' then
		
			state <= s_reset;
			
		elsif (sys_clk'event and sys_clk='1') then
			
			case state is
			
				-- Stato di reset dove, tutti i parametri vengoni inizializzati
				when s_reset => 
					load_data <= '0';
					all_reset <= '1';
					
					wdog_reset <= '1';
					wdog_start <= '0';
					
					state <= s_idle;
				
				-- Stato di idle, dove la macchina attende l'attivazione del segnale di send
				-- per iniziare il processo di memorizzazione, codifica ed invio del dato
				when s_idle => 	
					if (send='1') then
						state <= s_toogle;
					end if;
				
				-- Stato in cui viene togglato il bit di toggle da inserire nel dato da inviare
				when s_toogle =>
					reg_toggle <= not reg_toggle;
					state <= s_load;
					
				-- Stato in cui viene abilitata la memorizzazione del dato da inviare
				when s_load =>
					load_data <= '1';
					state <= s_start_reset;

				-- Stage 1 dello stato di start in cui vengono effettuate le operazioni di reset
				-- del contatore watchdog. In realt� questo stato � ridondante ma � stato inserito 
				-- per ritardare di un ciclo di clock la fase di memorizzazione del dato, prima di
				-- iniziare l'invio
				when s_start_reset =>
					wdog_reset <= '1';
					wdog_start <= '0';
					state <= s_start_rx;

				-- Stage 2 dello stato di start dove tutti i segnali di reset vengono abbassati 
				-- e viene iniziata effettivamente la codifica e trasmissione. Il sistema, resta
				-- in questo stato finch� non � terminata la trasmissione, dopodich�, se il segnale
				-- di send � ancora alto allora avvia il watchdog per inviare un nuovo pachetto,
				-- altrimenti ritorna in fase di reset
				when s_start_rx =>
					load_data <= '0';
					all_reset <= '0';
					wdog_reset <= '0';
					if (end_tx='1') then
						if (send='1') then
							wdog_start <= '1';
							state <= s_wdog;
						else
							state <= s_reset;
						end if;
					end if;
										
				-- Stato di controllo del watchdog. Lo stato resta in attesa del segnale di timeout
				-- del contatore watchdog per poter riavviare la codifica e trasmissione del dato
				-- gia memorizzato, purch� il segnale di send sia ancora alto, altrimenti resetta il sistema
				when s_wdog => 
					if (send = '1') then
						if (wdog_timeout = '1') then
							all_reset <= '1';
							state <= s_start_reset;
						end if;
					else
						state <= s_reset;
					end if;

				when others =>  
					state <= s_reset;
			
				end case;
			end if;
	end process;
	
	toggle <= reg_toggle;
	
end Behavioral;
