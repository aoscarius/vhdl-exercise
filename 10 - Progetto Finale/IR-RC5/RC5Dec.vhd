-------------------------------------------------------------------------------
-- University of Naples Federico II
-- Master of Architecture of Computer Systems
--
-- Academic Year: 2014-2015
--
-- Group Number: 14
-- Engineers: Castello Oscar


-- 
-- Project Name: IR-RC5 Module
-- Create Date:  05/01/2015
-- Module Name:  RC5Enc.vhd 
--
-- Target Board: Digilent Nexys 2
-- Target Devices: Spartan3E - XC3S1200E/FG320
-- Tool versions: Xilinx ISE 14.x WebPack Free
--
-- Description:  Modulo per la decodifica Manchester, nota anche come codifica 
--					  bi-fase, di un dato ad N bit. Il modulo necessita di essere 
--               allineato ad inizio trasmissione per poter effettuare una corretta
--               decodifica.
--
-- Dependencies: none
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity RC5Dec is
	 generic ( N: natural := 14 );
    port ( clk : in std_logic;									     -- Clock di campionamento
			  reset : in std_logic;                              -- Reset dei registri interni
           in_data : in std_logic;                            -- Dato seriale codificato in ingresso
			  end_rx : out std_logic;									  -- Attivo quando tutti gli N bit sono stati ricevuti
			  error_rx : out std_logic;								  -- Attivo quando si � presentato un errore nella decodifica
			  data_bus : out std_logic_vector ((N-1) downto 0)); -- Dato ricevuto
end RC5Dec;

architecture behavioral of RC5Dec is
	
	signal reg_sample : std_logic := '0';
	signal ff_sample : std_logic := '0';

	signal rx_enabled : std_logic := '0';
	signal bit_counter : integer range 0 to N := N; -- Contatore di bit interno
	signal reg_data : std_logic_vector ((N-1) downto 0) := (others => '0');
	
begin
	-- Il duty cycle del clock, determina il centraggio del campionamento
	-- mentre l'allineamento � dato dal rilascio del reset
	process(clk, reset)
	begin
		if (reset = '1') then
			reg_sample <= '0';
			ff_sample <= '0';
			
			error_rx <= '0';
			
			rx_enabled <= '1';
			reg_data <= (others => '0');
			bit_counter <= N;
			
			data_bus <= (others => '0');
		else
			if (rx_enabled='1') then
				if (clk'event and clk = '1') then 
				   -- Attraverso un FF-T
					ff_sample <= not ff_sample;
					-- Ogni due colpi di clock, campiono il bit in ingresso
					if (ff_sample = '0') then 
						reg_sample <= in_data;
					else
					-- Al successivo colpo di clock, confronto il bit campionato
					-- con l'attuale e quindi decodifico il dato
						if ((reg_sample = '0') and (in_data = '1')) then
							reg_data <= reg_data((N-2) downto 0) & '0';
							bit_counter <= bit_counter - 1;
						elsif ((reg_sample = '1') and (in_data = '0')) then
							reg_data <= reg_data((N-2) downto 0) & '1';
							bit_counter <= bit_counter - 1;
						elsif (reg_sample = in_data) then
							error_rx <= '1';
						end if;
					end if;
					-- Se il contatore di bit ha raggiunto lo 0 termino la ricezione
					if (bit_counter = 0) then
						rx_enabled <= '0';
						data_bus <= reg_data;
					else
						data_bus <= (others => '0');
					end if;
				end if;
			end if;
		end if;
	end process ;
	
	end_rx <= not rx_enabled;

end behavioral;

